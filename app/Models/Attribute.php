<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at','name_ar','name_en'];

    public function category(){
        return $this->belongsTo(Category::class , 'subcat_id');
    }

    public function  getIconPathAttribute()
    {
      return asset('uploads/attributes_images/'. $this->icon);
    }// end of get Image Path

    public function subattributes(){
        return $this->hasMany(Subattribute::class , 'attr_id');
    }
}
