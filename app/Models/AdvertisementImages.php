<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertisementImages extends Model
{
    protected $guarded = [];

    protected $hidden = ['ad_id','created_at','updated_at'];

    public function  getImagePathAttribute()
    {
      return asset('uploads/advertisements_images/'. $this->image);
    }// end of get Image Path
}
