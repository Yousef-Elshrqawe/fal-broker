<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialAdvertisement extends Model
{
    protected $table = 'special_advertisements';

    protected $guarded = [];

    public function category(){
        return $this->belongsTo(Category::class, 'cat_id');
    }


} // end of  SpecialAdvertisement
