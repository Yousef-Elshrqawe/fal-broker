<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    //
    protected $guarded = [];


    public function  getImageBannerAttribute($val)
    {
        return ($val !== null) ? asset('assets/img/banner_images/' . $val) : "";
    }

}
