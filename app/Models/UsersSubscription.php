<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersSubscription extends Model
{
    protected $guarded = [];


    public function  getBankTransferAttribute($val)
    {
        return ($val !== null) ? asset('assets/img/users/' . $val) : "";
    }

    public function subscriptionName(){
        return $this->belongsTo(Subscription::class , 'subscription_id');
    }
    
    public function bankName(){
        return $this->belongsTo(BankAccount::class , 'bank_id');
    }

}
