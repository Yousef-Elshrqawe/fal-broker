<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function  getImagePathAttribute()
    {
      return asset('uploads/categories/'. $this->image);
    }// end of get Image Path

    public function  getIconPathAttribute()
    {
      return asset('uploads/categories/'. $this->icon);
    }// end of get icon Path

    public function subcategories(){
      return $this->hasMany(Subcategory::class , 'cat_id');
    }

    public function SpecialAdvertisement(){
        return $this->hasMany(SpecialAdvertisement::class , 'cat_id');
    }

} // end of Category
