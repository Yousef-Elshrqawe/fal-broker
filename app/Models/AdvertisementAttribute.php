<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertisementAttribute extends Model
{
    protected $guarded = [];

    public function attribute(){
        return $this->belongsTo(Attribute::class , 'attr_id');
    }
    public function subAttribute(){
        return $this->belongsTo(Subattribute::class , 'subattr_id');
    }

}
