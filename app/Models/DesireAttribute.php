<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DesireAttribute extends Model
{
    protected $guarded = [];
}
