<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $guarded = [];
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImagePathAttribute()
    {
      return asset('uploads/customers_images/'. $this->image);
    }// end of get Image Path


    public function  getImageAttribute($val)
    {
        return ($val !== null) ? asset('uploads/customers_images/' . $val) : "";
    }

    public function  getImageIdentityAttribute($val)
    {
        return ($val !== null) ? asset('assets/img/users/' . $val) : "";
    }

    public function  getCommercialRegisterAttribute($val)
    {
        return ($val !== null) ? asset('assets/img/users/' . $val) : "";
    }


    public function scopeDistance($query, $latitude, $longitude, $distance, $unit = "km")
    {
        $constant = $unit == "km" ? 6371 : 3959;
        $haversine = "(
            $constant * acos(
                cos(radians(" .$latitude. "))
                * cos(radians(`latitude`))
                * cos(radians(`longitude`) - radians(" .$longitude. "))
                + sin(radians(" .$latitude. ")) * sin(radians(`latitude`))
            )
        )";

        return $query->selectRaw("$haversine AS distance")
            ->having("distance", "<=", $distance);
    } // end of scopeDistance

    //return user's liked ads
    public function wishlist(){
        return $this->hasMany(Wishlist::class , 'user_id');
    }

    public function advertisements(){
        return $this->hasMany(Advertisement::class , 'user_id')->orderby("id", "desc");
    }

    public function funds(){
        return $this->hasMany(Fund::class , 'user_id');
    }

    public function desires(){
        return $this->hasMany(Desire::class , 'user_id');
    }

    public function subscription(){
        return $this->hasOne(UsersSubscription::class , 'user_id');
    }


    public function users(){
        return $this->belongsToMany(User::class, 'user_follows','follow_id','user_id');
    }

    public function follows(){
        return $this->belongsToMany(User::class, 'user_follows','user_id','follow_id');
    }

    public function usersRate(){
        return $this->belongsToMany(User::class, 'user_rates','rate_user_id','user_id');
    }

    public function rateUsers(){
        return $this->belongsToMany(User::class, 'user_rates','user_id','rate_user_id');
    }

    
    public function governorate(){
        return $this->belongsTo(Governorate::class , 'governorate_id');
    }


} // end of user
