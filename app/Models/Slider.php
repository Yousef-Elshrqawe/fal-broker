<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $guarded = [];

    protected $hidden = ['image_en', 'image_ar' , 'ad_id'];

    public function category()
    {
        return $this->belongsTo(Category::class,'cat_id');
    }

    public function  getImagePathAttribute()
    {
      $image = 'image_'.app()->getLocale();
      return asset('uploads/sliders/'. $this->$image);
    }// end of get Image Path

    public function  getImageEnPathAttribute()
    {
      return asset('uploads/sliders/'. $this->image_en);
    }// end of get Image Path
    public function  getImageArPathAttribute()
    {
      return asset('uploads/sliders/'. $this->image_ar);
    }// end of get Image Path
}
