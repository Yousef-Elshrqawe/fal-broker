<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $guarded = [];

    public function users(){
        return $this->hasMany(User::class , 'country_id');
    }

    public function govers(){
        return $this->hasMany(Governorate::class,'country_id');
    }

}
