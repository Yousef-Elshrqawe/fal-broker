<?php

namespace App\Http\Resources\Main;

use App\Models\{AdvertisementAttribute, Advertisement};
use App\Models\AdvertisementImages;
use App\Models\AdvertisementType;
use App\Models\Attribute;
use App\Models\Country;
use App\Models\District;
use App\Models\Governorate;
use App\Models\Subattribute;
use App\Models\{User, ContactUs, SpecialAdvertisement, UserFollow};
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class AdvertisementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = 'name_'.app()->getLocale();

        //get state and city names
        $country = Country::select('id','name_'.app()->getLocale().' as name')->where('id',$this->country_id)->first();
        $govern = Governorate::select('id','name_'.app()->getLocale().' as name' , 'country_id as state_id')->where('id',$this->govern_id)->first();
        $district = District::select('id','name_'.app()->getLocale().' as name' , 'govern_id as city_id')->where('id',$this->district_id)->first();
        //get advertisement type name
        $ad_type = AdvertisementType::find($this->type);
        if($ad_type){
            $type = $ad_type->$name;
        }else{
            $type = '';
        }

        // check special add
        $special = SpecialAdvertisement::find($this->special_ad_id);
        if($special){
            $specialAdEnd = Carbon::parse($this->special_ad_start)->addDays($special->expiration);
            $specialAdEnd = $specialAdEnd->format('Y-m-d h:i:s');

            if($specialAdEnd < Carbon::now()){
                $advertisement = Advertisement::find($this->id);
                $advertisement->special_ad_id = null;
                $advertisement->save();
            }

        }else{
            $specialAdEnd = null;
        }

         //is Follow or not
        $userFollow = UserFollow::where('user_id',$request->user_id)->where('follow_id',$this->user_id)->first();
        if($userFollow)
            $is_followed = true;
        else
            $is_followed = false;

        //is liked or not
        $wishlist = Wishlist::where('user_id',$request->user_id)->where('ad_id',$this->id)->first();
        if($wishlist)
            $is_liked = true;
        else
            $is_liked = false;

        //Advertisement Images
        $advertisementImages = $this->images;//AdvertisementImages::select('id','image')->where('ad_id',$this->id)->get();
        foreach($advertisementImages as $advertisementImage){
            $advertisementImage->image = $advertisementImage->image_path;
        }

        //advertisement details
        $advertisementAttributes = AdvertisementAttribute::select('attr_type','attr_id','subattr_id','value')->where('ad_id',$this->id)->get();
        foreach($advertisementAttributes as $advertisementAttribute){
            $attribute = Attribute::find($advertisementAttribute->attr_id);
            $advertisementAttribute->attr_name = $attribute->$name;

            if($attribute->icon != null)
                $advertisementAttribute->icon = $attribute->icon_path;
            else
                $advertisementAttribute->icon = null;

            $subattribute = Subattribute::find($advertisementAttribute->subattr_id);
            if($subattribute)
                $advertisementAttribute->subattr_name = $subattribute->$name;
            else
                $advertisementAttribute->subattr_name = '';

            unset($advertisementAttribute->attr_id);
            unset($advertisementAttribute->subattr_id);
        }

        //Advertisement Rates
        $advertisementRates = ReviewResource::collection($this->rates);   // $advertisementRates = $this->rates;
        $advertisementAvgRates = $this->rates->avg('rate')?? 5;
        $advertisementCountRates = $this->rates->count();

        $setting = ContactUs::first(); // get link_banner and  image_banner

        // $user = User::find($this->user_id);

        return [
            'id'        =>$this->id,
            'user_id'   =>$this->user_id,
	        'user_name' =>$this->user->name?? '',
	        'user_type' =>$this->user->type?? '',
	        'user_dependence' =>$this->user->dependence?? '',
            'user_phone' => $this->user->phone?? '',
            'user_country_code'=>$this->user->country_code?? '',
            'user_image'=>$this->user->image?? '',
            'user_status'=>$this->user->active?? '',
            'user_view_rate'=>$this->user->view_rate?? 0,
            'user_avg_rate'=>number_format($this->user->avg_rate?? 0, 2, '.', ''),
            'user_count_rate'=>$this->user->count_rate?? 0,
            'user_address'=>$this->user->address?? '',
            'subcat_id' =>$this->subcat_id,
            'title'     =>$this->title,
            'desc'      =>$this->desc,
            'payment_way' =>$this->payment_way,
            'price'     =>$this->price,
            'type'      =>$type,
	        'type_id'   =>$this->type,
            'address'   =>$this->address,
            'state_name'=>$country?$country->name:'',
            'city_name' =>$govern?$govern->name:'',
            'district_name'=>$district?$district->name:'',
            'state'     =>$country,
            'city'      =>$govern,
            'district'  =>$district,
            'lat'       =>$this->lat,
            'lng'       =>$this->lng,
            'popular'  =>$this->popular,
            'finished'  =>$this->finished,
            'canceled'  =>$this->canceled,
            'reason_for_cancellation' => $this->reason_for_cancellation,
            'special_ad_id'=>$this->special_ad_id,
            'special_ad_start'=>$this->special_ad_start,
            'special_ad_end'  =>$specialAdEnd,
            'start_date'=>$this->start_date,
            'end_date'  =>$this->end_date,
            // 'created_at' => $this->created_at->diffForHumans(),
            'link_banner' => $setting->link_banner,
            'image_banner' => $setting->image_banner,
            'is_liked'  =>$is_liked,
            'is_followed' =>$is_followed,
            'images'    =>$advertisementImages,
            'attributes'=>$advertisementAttributes,
            'ratings' => $advertisementRates,
            'ad_avg_rate'  => number_format($advertisementAvgRates, 2, '.', ''),
            'ad_count_rate' => $advertisementCountRates,

        ];
    }
}
