<?php

namespace App\Http\Resources\Main;

use App\Models\{User, ContactUs, SpecialAdvertisement};
use App\Models\UserFollow;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $name = 'name_'.app()->getLocale();

        //get state and city names
        // $country = Country::select('id','name_'.app()->getLocale().' as name')->where('id',$this->country_id)->first();

        //is Follow or not
        $userFollow = UserFollow::where('user_id',$request->user_id)->where('follow_id',$this->id)->first();
        if($userFollow)
            $is_followed = true;
        else
            $is_followed = false;


        return [
            'id'            =>$this->id,
	        'name'          =>$this->name,
	        'type'          =>$this->type,
	        'email'         =>$this->email,
            'phone'         =>$this->phone,
            'country_code'  =>$this->country_code,
            'image'         =>$this->image,
            'governorate_id' =>$this->governorate_id,
            'address'      =>$this->address,
            'lat'          =>$this->lat,
            'lng'          =>$this->lng,
            'fcm_token'    =>$this->fcm_token,
            'mobile_id'    =>$this->mobile_id,
            'active'       =>$this->active,
            'dependence'   =>$this->dependence,
            'view_rate'    =>$this->view_rate,
            'avg_rate'     =>number_format($this->avg_rate, 2, '.', ''),
            'count_rate'   =>$this->count_rate,
            'number_identity' =>$this->number_identity,
            'image_identity'  =>$this->image_identity,
            'commercial_registration_no' =>$this->commercial_registration_no,
            'commercial_register'  =>$this->commercial_register,
            'count_followes'       =>$this->count_followes?? 0,
            'is_followed'          =>$is_followed,
	        'created_at'           =>$this->created_at,
            
        ];
    }
} // end of UserResource
