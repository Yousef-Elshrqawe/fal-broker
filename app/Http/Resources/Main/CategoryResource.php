<?php

namespace App\Http\Resources\Main;

use App\Models\AdvertisementFee;
use App\Models\Attribute;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($request->header('lang') == "ar"){
            $name   = $this->name_ar;
            $type_name = $this->type_name_ar;
	        $details_name = $this->details_name_ar;
        }else{
            $name   = $this->name_en;
            $type_name = $this->type_name_en;
            $details_name = $this->details_name_en;
        }

        // $attributes = Attribute::select('id','name_'.$request->header('lang').' AS name')
        // ->where('cat_id',$this->id)->with('subattributes')->get();
        // foreach($attributes as $attribute){
        //     foreach($attribute->subattributes as $subattribute){
        //         if($request->header('lang') == "ar"){
        //             $subattribute->name = $subattribute->name_ar;
        //         }else{
        //             $subattribute->name = $subattribute->name_en;
        //         }
        //     }
        // }
        

        return [
            'id'    => $this->id,
            'name'  => $name,
            'type_name'=>$type_name,
            'details_name'=>$details_name,
            'image' => $this->image!=null?$this->image_path:null,
            'icon' => $this->icon!=null?$this->icon_path:null,
            'ad_fees'=> AdvertisementFee::first()->fees ?? 0.0,
            //'attributes' => $attributes,
        ];
    }
}
