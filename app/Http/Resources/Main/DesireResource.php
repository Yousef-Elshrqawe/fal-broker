<?php

namespace App\Http\Resources\Main;

use App\Models\AdvertisementType;
use App\Models\Attribute;
use App\Models\Country;
use App\Models\DesireAttribute;
use App\Models\Governorate;
use App\Models\Subattribute;
use App\Models\Subcategory;
use App\Models\Category;
use App\Models\District;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class DesireResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = 'name_'.app()->getLocale();

        //get subcat and cat names
        $subcat = Subcategory::find($this->subcat_id);
	
        if($subcat){
            $subcatName = $subcat->$name;
	    $catName = Category::find($subcat->cat_id)->$name;
		
        }else{
            $subcatName = '';
	    $catName = '';
        }
        //get state, city and district names
        $country = Country::find($this->country_id);
        $govern = Governorate::find($this->govern_id);
        $district = District::find($this->district_id);

        $state_name = $country? $country->$name:'';
        $city_name = $govern? $govern->$name:'';
        $district_name = $district? $district->$name:'';

        //get desire type name
        $ad_type = AdvertisementType::find($this->type);
        if($ad_type){
            $type = $ad_type->$name;
        }else{
            $type = '';
        }

        //desire details
        $desireAttributes = DesireAttribute::select('attr_type','attr_id','subattr_id','value')->where('desire_id',$this->id)->get();
        foreach($desireAttributes as $desireAttribute){
            $attribute = Attribute::find($desireAttribute->attr_id);
            $desireAttribute->attr_name = $attribute->$name;

            if($attribute->icon != null)
                $desireAttribute->icon = $attribute->icon_path;
            else
                $desireAttribute->icon = null;

            $subattribute = Subattribute::find($desireAttribute->subattr_id);
            if($subattribute)
                $desireAttribute->subattr_name = $subattribute->$name;
            else
                $desireAttribute->subattr_name = '';
            
            unset($desireAttribute->attr_id);
            unset($desireAttribute->subattr_id);
        }

        //$user = User::find($this->user_id);

        return [
            'id'         =>$this->id,
            'user_id'    =>$this->user_id,
            'cat_name'=>$catName,
            'subcat_name'=>$subcatName,
            'from_price' =>$this->from_price,
            'to_price'   =>$this->to_price,
            'type'       =>$type,
            'state_name' =>$state_name,
            'city_name'  =>$city_name,
            'district_name'=>$district_name,
            'attributes' =>$desireAttributes,
        ];
    }
}
