<?php

namespace App\Http\Resources\Main;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'sender_id'     => $this->sender_id,
            'receiver_id'   => $this->receiver_id,
            'message'       => $this->message,
            'created_at'    => $this->created_at->diffForHumans(),
            'time'          => $this->created_at,
        ];  
    }
}
