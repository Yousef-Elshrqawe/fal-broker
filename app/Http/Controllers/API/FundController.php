<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Main\AdvertisementResource;
use App\Models\Advertisement;
use App\Models\{Fund, ContactUs,BankAccount};
use App\Models\UserCoupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\backendTraits;
use Tymon\JWTAuth\Facades\JWTAuth;

class FundController extends BaseController
{
    use backendTraits;

    public function addFundRequest(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $setting = ContactUs::first();
        if($setting->financing_request == 0){
            return $this->returnError(null , __("user.financing_request_close"));
        }

        $validator = Validator::make(
            $request->all(),
            [
                'user_id'    => 'required|exists:users,id',
                'fund_type'          => 'required|in:0,1,2',
                'birth_date'         => 'nullable|min:1',
                'nationlity'     => 'nullable|min:1',
                'monthly_income' => 'nullable|min:1',
                'sector'    => 'nullable|min:1',
                'salary_type'   => 'nullable|min:1',
                'financing_periad'   => 'nullable|min:1',
                'monthly_commit'   => 'nullable|min:1',
                'car_price'   => 'nullable|min:1',
                'downpayment'   => 'nullable|min:1',
                'property_value'   => 'nullable|min:1',
                'account_type'   => 'nullable|min:1',
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $data = $request->all();
        $fund = Fund::create($data);

        return $this->returnData('fund', $fund, __("user.registSuccess"));
    } // end of AddFundRequest



    public function getMyFunds(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'=>'required|exists:users,id',
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $funds = Fund::where('user_id',$request->user_id)->get();

        return $this->returnData('funds' , $funds);
    } // end of getMyFunds

} // end of FundController
