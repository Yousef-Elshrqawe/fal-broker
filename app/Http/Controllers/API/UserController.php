<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\NotificationController;
use App\Http\Resources\Main\AdvertisementResource;
use App\Http\Resources\Main\UserResource;
use App\Http\Resources\Main\ReviewResource;
use App\Http\Resources\Main\CategoryResource;
use App\Http\Resources\Main\SubcategoryResource;
use App\Models\Advertisement;
use App\Models\AdvertisementFee;
use App\Models\{Category, Country, Coupon, District, Feedback, Governorate, SentNotification};
use App\Models\{Subcategory, Subscription, Wishlist};
use App\Models\{User, BankAccount, UserRate, UserFollow, ContactUs};
use App\Models\UserCoupon;
use App\Models\UsersSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\backendTraits;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends BaseController
{
    use backendTraits;


    //return user Details by user_id
    public function userDetails(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'nullable|exists:users,id',
                'merchant_id' => 'required|exists:users,id'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::with(['advertisements', 'governorate'])->find($request->merchant_id);
        $userDetails = UserResource::make($user);
        return $this->returnData('user', $userDetails);
        // return $this->returnData('user', $user);

    } // end of userDetails

    //***********************************General Purposes APIs**********************************************/

    // All Categories
    public function getCats()
    {
        $categories = Category::orderBy('sorting')->get();
        $cats = CategoryResource::collection($categories);
        return $this->returnData('cats', $cats);
    }

    //return subcats for specific category
    public function getSubcats(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'cat_id' => 'required|exists:categories,id'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $subcats = Subcategory::select('id', 'name_' . app()->getLocale() . ' AS name', 'cat_id', 'image', 'sorting')->where('cat_id', $request->cat_id)->orderBy('sorting')->get();
        $subcats = SubcategoryResource::collection($subcats);
        return $this->returnData('subcats', $subcats);
    } // end of getSubcats

    //return all subscription bundles
    public function getSubscriptions()
    {
        $subscriptions = Subscription::select(
            'id',
            'name_' . app()->getLocale() . ' AS name',
            'user_type',
            'ads_number',
            'period',
            'price'
        )->orderBy('price')->get();
        return $this->returnData('subscriptions', $subscriptions);
    }

    //update user subscription
    public function updateSubscription(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'subscription_id' => 'required|exists:subscriptions,id',
                'payment_method'   => 'nullable|in:0,1',
                'bank_id'          => 'nullable|exists:bank_accounts,id',
                'bank_transfer'    => 'nullable|image',
                'coupon_id'=>'nullable'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if ($user) {
            $subscription = Subscription::find($request->subscription_id);
            if ($subscription) {
                $usersSubscription = UsersSubscription::where('user_id', $request->user_id)->first();
                if ($usersSubscription) {
                    $usersSubscription->subscription_id = $request->subscription_id;
                    $usersSubscription->price           = $subscription->price;
                    $usersSubscription->ads_number      = $subscription->ads_number;
                    $usersSubscription->remainder_ads  = $subscription->ads_number;
                    $usersSubscription->start_date     = now();
                    $usersSubscription->end_date       = now()->addDays($subscription->period);
                    $usersSubscription->payment_method = $request->payment_method?? null;
                    $usersSubscription->status = 0;
                    $usersSubscription->save();
                    // save image user subscription
                    if ($request->hasFile('bank_transfer')) {
                        $usersSubscription->bank_transfer = $this->upploadImage($request->File('bank_transfer'), 'assets/img/users/');
                        $usersSubscription->bank_id = $request->bank_id;
                        $usersSubscription->save();
                    }

                    //decrease coupon count_of_use if coupon sent
                    if($request->coupon_id && $request->coupon_id != null){
                        $coupon = Coupon::find($request->coupon_id);
                        if($coupon){
                            $coupon->count_of_use -= 1;
                            $coupon->save();

                            //to know how many times this user used this coupon
                            UserCoupon::create([
                                'user_id'=>     $user->id,
                                'coupon_id'=>   $coupon->id,
                                'discount_value'=> $coupon->percent * $subscription->price /100
                            ]);
                        }
                    }
                    return $this->returnSuccessMessage('Subscribed successfully');
                } else {
                    $userSubscription = UsersSubscription::create([
                        'user_id'          => $user->id,
                        'subscription_id'  => $request->subscription_id,
                        'price'            => $subscription->price,
                        'ads_number'       => $subscription->ads_number,
                        'remainder_ads'    => $subscription->ads_number,
                        'start_date'       => now(),
                        'end_date'         => now()->addDays($subscription->period),
                        'payment_method'   => $request->payment_method?? null,
                    ]);
                    if ($request->hasFile('bank_transfer')) {
                        $userSubscription->bank_transfer = $this->upploadImage($request->File('bank_transfer'), 'assets/img/users/');
                        $userSubscription->bank_id = $request->bank_id;
                        $userSubscription->save();
                    }
                    //decrease coupon count_of_use if coupon sent
                    if($request->coupon_id && $request->coupon_id != null){
                        $coupon = Coupon::find($request->coupon_id);
                        if($coupon){
                            $coupon->count_of_use -= 1;
                            $coupon->save();

                            //to know how many times this user used this coupon
                            UserCoupon::create([
                                'user_id'=>     $user->id,
                                'coupon_id'=>   $coupon->id,
                                'discount_value'=> $coupon->percent * $subscription->price /100
                            ]);
                        }
                    }
                    return $this->returnSuccessMessage('Subscribed successfully');
                }
            } else {
                return $this->returnError('success', 'subscription bundle not found');
            }
        } else {
            return $this->returnError('success', __("user.usernotexist"));
        }
    }

    //follow (add , delete from follow/unfollow )
    public function follow(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'follow_id' => 'required|exists:users,id'  // merchant_id
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        if($request->follow_id == $request->user_id){
            return $this->returnError('' ,'The same user cannot follow the same');
        }else{
            $merchant = User::find($request->follow_id);
            $userFollow = UserFollow::where('user_id', $request->user_id)
                ->where('follow_id', $request->follow_id)->first();
            if ($userFollow) {
                $userFollow->delete();
                $merchant->count_followes -= 1 ;
                $merchant->save();
                return $this->returnSuccessMessage('unfollow merchant');
            } else {
                UserFollow::create($request->all());
                $merchant->count_followes += 1 ;
                $merchant->save();
                // sand notification in merchant
                $user_name = User::where('id', $request->user_id)->first()->name;
                NotificationController::sendSingleNotification($request->follow_id,
                __("users.app_name"), __("user.follow_merchant :user_name", [ 'user_name' => $user_name, ]));


                return $this->returnSuccessMessage('follow merchant');
            }
        }

    } // end of follow


    //wishlist (add , delete from wishlist)
    public function wishlist(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
                'ad_id' => 'required|exists:advertisements,id'
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if ($user) {
            $wishlist = Wishlist::where('user_id', $request->user_id)
                ->where('ad_id', $request->ad_id)->first();

            if ($wishlist) {
                $wishlist->delete();
                return $this->returnSuccessMessage('Advertisement has been deleted from wishlist');
            } else {
                Wishlist::create($request->all());
                return $this->returnSuccessMessage('Advertisement has been added to wishlist');
            }
        } else {
            return $this->returnError('success', __("user.usernotexist"));
        }
    }

    //get user liked Advertisements
    public function getLikedAdvertisements(Request $request)
    {
        $validator = Validator::make(
            [
                'user_id' => 'required'
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if ($user) {
            $liked_advertisements = array();
            foreach ($user->wishlist as $liked_advertisement) {
                $liked_advertisements[] = Advertisement::find($liked_advertisement->ad_id);
            }
            $advertisements = AdvertisementResource::collection($liked_advertisements);
            return $this->returnData('advertisements', $advertisements);
        } else {
            return $this->returnError('success', __("user.usernotexist"));
        }
    }


    //get All countries
    public function getBankAccounts()
    {
        $BankAccounts = BankAccount::get();
        return $this->returnData('BankAccounts', $BankAccounts);
    }

    //get All countries
    public function getCountries()
    {
        $countries = Country::select("id", "name_" . app()->getLocale() . ' as name')->get();
        return $this->returnData('countries', $countries);
    }

    //get Governorates by Country
    public function getGovernorates()
    {
        // $validator = Validator::make(
        //     $request->all(),
        //     [
        //         'country_id' => 'required',
        //     ],
        //     [
        //         'country_id.required'     => __('user.reqCountry'),
        //     ]
        // );
        // if ($validator->fails()) {
        //     $code = $this->returnCodeAccordingToInput($validator);
        //     return $this->returnValidationError($code, $validator);
        // }

        $govers = Governorate::select("id", "name_" . app()->getLocale() . ' as name')
            ->where('country_id', 9)->get();
        return $this->returnData('governorates', $govers);
    }


    //get Districts by governorate
    public function getDistricts(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'govern_id' => 'required|exists:governorates,id',
            ],
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $districts = District::select("id", "name_" . app()->getLocale() . ' as name')
            ->where('govern_id', $request->govern_id)->get();
        return $this->returnData('districts', $districts);
    }


    //***********************************Login System ******************************************************/
    public function register(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $validator = Validator::make(
            $request->all(),
            [
                'name'          => ['required', 'string', 'min:3', 'max:70'],
                // 'type'          => 'nullable|in:0,1',
                'phone'         => 'required|unique:users',
                'country_code'  => 'required',
                'email'         => 'required|email|unique:users',
                'password'      => 'required|min:6',
                'subscription_id'   => 'nullable|exists:subscriptions,id',
                'governorate_id'    => 'required|exists:governorates,id',
                'address'           => 'nullable|string',
                'lat'               => 'nullable',
                'lng'               => 'nullable',
                'number_identity'   => 'nullable|min:3',
                'image_identity'    => 'nullable|image',
                'commercial_registration_no' => 'nullable|min:3',
                'commercial_register'  => 'nullable|image',
                'payment_method'   => 'nullable|in:0,1',
                'bank_id'          => 'nullable|exists:bank_accounts,id',
                'bank_transfer'    => 'nullable|image',
                'mobile_id'        => 'required|in:0,1',
                'fal_license_number' => 'nullable|min:3',
                // 'coupon_id'=>'nullable|exists:coupons,id',
            ],
            [
                'name.required'           => __('user.req_username'),
                'phone.required'          => __('user.reqPhone'),
                'phone.unique'            => __('user.uniquePhone'),
                'email.required'          => __('user.reqEmail'),
                'email.unique'            => __('user.unique_email'),
                'password.required'       => __("user.reqPassword"),
                'country_id.required'     => __('user.reqCountry'),
                'address.required'        =>  __('user.reqAddress'),
                'idImage.required'        =>  __('user.reqImageID'),
                'lat.required'            =>  __('user.latitude'),
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $data = $request->except(['password', 'subscription_id', 'image_identity', 'commercial_register', 'bank_transfer', 'bank_id', 'payment_method']);
        $data['password']  = bcrypt($request->password);

        $user = User::create($data);
        // save image user
        if ($request->hasFile('image_identity')) {
            $user->image_identity = $this->upploadImage($request->File('image_identity'), 'assets/img/users/');
            $user->save();
        }
        if ($request->hasFile('commercial_register')) {
            $user->commercial_register = $this->upploadImage($request->File('commercial_register'), 'assets/img/users/');
            $user->save();
        } // end of upload photo

        if ($user->type == 0) {
            $user->active = 1;
            $user->save();
        }

        // Subscription
        if($request->subscription_id){
            $subscription = Subscription::find($request->subscription_id);
            $userSubscription = UsersSubscription::create([
                'user_id'           => $user->id,
                'subscription_id'   => $subscription->id,
                'price'             => $subscription->price,
                'ads_number'        => $subscription->ads_number,
                'remainder_ads'     => $subscription->ads_number,
                'start_date'        => now(),
                'end_date'          => now()->addDays($subscription->period),
                'payment_method'    => $request->payment_method,
            ]);
            // $user->user_subscription =  $UsersSubscription;
            // save image user subscription
            if ($request->hasFile('bank_transfer')) {
                $userSubscription->bank_transfer = $this->upploadImage($request->File('bank_transfer'), 'assets/img/users/');
                $userSubscription->bank_id = $request->bank_id;
                $user->save();
            }
        } // end of Subscription

        $token = NULL;
        $credentials = request(['phone', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user->token =  $token;

        return $this->returnData('user', $user, __("user.registSuccess"));
    } // end of register

    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'phone'          => 'required',
                'password'       => 'required',
            ], [
                'phone.required'        => __("user.reqPhone"),
                'password.required'     => __("user.req_password"),
            ]);

            if ($validator->fails()) {
                $code = $this->returnCodeAccordingToInput($validator);
                return $this->returnValidationError($code, $validator);
            }

            //login
            $credentials = $request->only(['phone', 'password']);
            $token = auth('api')->attempt($credentials);  //generate token

            if (!$token)
                return $this->returnError('E001', __('user.false_info'));

            $user = auth('api')->user();
            //return subscription name and remainder ads with user profile
            $subscription = $user->subscription;
            $name = 'name_'.app()->getLocale();
            if($subscription){
                $user->subscription_name = Subscription::find($subscription->subscription_id)->$name?? '';
                $user->subscription_period = Subscription::find($subscription->subscription_id)->period?? 0;
                $user->subscription_ads_number = $subscription->ads_number?? 0;
                $user->remainder_ads = $subscription->remainder_ads;
                $user->end_date = $subscription->end_date;
                //$user->is_subscribed = !$subscription->status;
            }else{
                $user->subscription_name = '';
                $user->remainder_ads = 0;
                //$user->is_subscribed = false;
            }
            $user->governorate_name = Governorate::find($user->governorate_id)->$name?? '';

            $user->token = $token;
            unset($user->subscription);
            // $response = [
            //     'id'        => $user->id,
            //     'name'      => $user->name,
            //     'phone'     => $user->phone,
            //     'email'     => $user->email,
            //     'token'     => $token,
            // ];

            return $this->returnData('user', $user, __('user.login'));  //return json response
        } catch (\Exception $ex) {
            return $this->returnError($ex->getCode(), $ex->getMessage());
        }
    } // end of login

    /**
     * forgetpassword process By phone
     */
    public function forgetpassword(Request $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if (!$user) {
            $errormessage = __('user.wrang_phone');
            return $this->returnError('', $errormessage);
        } else {
            $randomcode        = substr(str_shuffle("0123456789"), 0, 4);
            $user->forgetcode  = $randomcode;
            $user->save();
            $successmessage = __('user.sent_phone');
            return $this->returnData('verify_code', $user->forgetcode, $successmessage);
        }
    }


    // Change Password user in profile
    public function changePassword(Request $request)
    {
        # Validation
        $validate = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            // 'new_password' => 'required|confirmed',
        ]);

        if ($validate->fails()) {
            $code = $this->returnCodeAccordingToInput($validate);
            return $this->returnValidationError($code, $validate);
        }

        #Match The Old Password
        if(!Hash::check($request->old_password, auth()->guard('api')->user()->password)){
            // return back()->with("error", "Old Password Doesn't match!");
            return $this->returnError('404', __('app/public.Old_Password_Doesnt_match'));
        }

        #Update the new Password // bcrypt($request->password);  // Hash::make($request->new_password)
        User::whereId(auth()->guard('api')->user()->id)->update([
            'password' => bcrypt($request->new_password)
        ]);

        return $this->returnSuccessMessage(__('app/public.Password_changed_successfully'));
    } // end of changePassword



    /**
     *  Chanage Password
     */
    public function rechangepass(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'new_password'    => 'required|min:6',
            ],
            [
                'new_password.required' => __('user.reqPassword'),
                'new_password.min'      => __('user.minPassword'),
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $member = User::where('phone', $request->phone)->first();
        if ($member) {
            $member->password = Hash::make($request->new_password);
            $member->save();
            $successmessage = __('user.new_pass');
            return $this->returnData('msg', $successmessage);
        } else {
            $errormessage = __('user.wrang_phone');
            return $this->returnError('error', $errormessage);
        }
    }

    // check phone if exist
    public function checkPhone(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'phone'    => 'required',
            ],
            [
                'phone.required'    =>  __('user.reqPhone'),
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $exists = User::where('phone', $request->phone)->exists();

        return $this->returnData('data', ['exists' => $exists]);
    } //


        // check Mail if exist
    public function checkEmail(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email'  => 'required|email',
                // 'email'  => 'required|email|unique:users',
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        // return $this->returnSuccessMessage(__('app/public.Password_changed_successfully'));

        $exists = User::where('email', $request->email)->exists();

        return $this->returnData('data', ['exists' => $exists]);
    } //end of checkEmail


    //user profile
    public function getProfile()
    {
        $user = auth()->user();
        // $user->image = $user->image_path;
        if ($user) {
            //return subscription name and remainder ads with user profile
            $subscription = $user->subscription;
            $name = 'name_'.app()->getLocale();
            if($subscription){
                $user->subscription_name = Subscription::find($subscription->subscription_id)->$name?? '';
                $user->subscription_period = Subscription::find($subscription->subscription_id)->period?? 0;
                $user->subscription_ads_number = $subscription->ads_number?? 0;
                $user->remainder_ads = $subscription->remainder_ads;
                $user->end_date = $subscription->end_date;
                //$user->is_subscribed = !$subscription->status;
            }else{
                $user->subscription_name = '';
                $user->remainder_ads = 0;
                //$user->is_subscribed = false;
            }
            $user->governorate_name = Governorate::find($user->governorate_id)->$name;
            $user->financing_request = ContactUs::first()->financing_request;
            unset($user->subscription);

            return $this->returnData('success', $user);
        } else {
            return $this->returnError('success', __("user.usernotexist"));
        }
    }

    //update profile
    public function updateProfile(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $upmember = User::where('id', $request->user_id)->first();
        if ($upmember) {
            $validator = Validator::make(
                $request->all(),
                [
                    'name'              => ['required', 'string', 'min:3', 'max:100'],
                    'email'             => 'required|unique:users,email,' . $upmember->id,
                    'phone'             => 'required|unique:users,phone,' . $upmember->id,
                    'country_code'      => 'required',
                    'governorate_id'    => 'required|exists:governorates,id',
                    'address'           => 'required|string',
                    'lat'               => 'required',
                    'lng'               => 'required',
                    'image'             => 'nullable|image',
                    'fal_license_number' => 'nullable|min:3',
                    //'country_id'    => 'required',
                ],
                [
                    'name.required'           => __('user.req_username'),
                    'email.required'          => __('user.reqEmail'),
                    'email.unique'            => __('user.unique_email'),
                    'country_id.required'     => __('user.reqCountry'),
                    'address.required'        => __('user.reqAddress'),
                ]
            );
            if ($validator->fails()) {
                $code = $this->returnCodeAccordingToInput($validator);
                return $this->returnValidationError($code, $validator);
            }

            $data = $request->except(['image', 'user_id']);

            // If image request
            $old_image = $upmember->image;
            if ($request->hasFile('image')) {
                if ($old_image != "default.png") {
                    $this->deleteFile( pathinfo($upmember->image)['basename'], 'uploads/customers_images/');
                    // Storage::disk('uploads')->delete('customers_images/' . $old_image);
                }
                $data['image'] = $this->upploadImage($request->image, 'uploads/customers_images/');
            } else {
                // $upmember->image  = $old_image;
            }
            $upmember->update($data);
            $successmessage = __("user.updated_successfully");
            return $this->returnData('msg', $successmessage);
        } else {
            $errormessage = __("user.usernotexist");
            return $this->returnError('E001', $errormessage);
        }
    }


    //logout users
    public function logout(Request $request)
    {
        $token = $request->header('token');
        if ($token) {
            try {
                // $user = User::find(auth()->guard('api')->user()->id);
                $user = auth()->guard('api')->user();
                $user->fcm_token = null;
                $user->save();

                JWTAuth::setToken($token)->invalidate(); //logout
            } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                return  $this->returnError('', __('user.wrangs'));
            }
            return $this->returnSuccessMessage(__('user.logout'));
        } else {
            return $this->returnError('', __('user.wrangs'));
        }
    } // end of logout


    public function sendOTP(Request $request){

      $validator = Validator::make($request->all(), [
          'phone'          => 'required',
          'country_code'   => 'required',
      ], [
          'phone.required' => __("user.reqPhone"),
      ]);
      if ($validator->fails()) {
          $code = $this->returnCodeAccordingToInput($validator);
          return $this->returnValidationError($code, $validator);
      }

    //   return "$request->country_code$request->phone";
      //header("Content-Type: text/html; charset=utf-8");
      $code = rand(1111,9999);
      $messageContent = __('user.your_verify_code')." $code";
      // $app_id = "api key";
      $app_id = "jEQAuWYrR6UndwAl7OVahUdVcaoGeIiTjS3KYZMe";
      // $app_sec = "api secret";
      $app_sec = "gRzEEXXfP5vDafGhIwUWArOTKKZmtrWt1KsB2McrxekeQJmwaj6TONlcHSeGWmDM2wlvZkv1A9yijb6ssIJbFOevsGVJMfBOH73N";
      $app_hash = base64_encode("{$app_id}:{$app_sec}");

// country_code
      $messages = [

          "messages" => [
              [
                  "text" => $messageContent,
                  "numbers" => ["$request->country_code$request->phone"],
                  "sender" => "Khiark"
              ]
          ]
      ];

      $url = "https://api-sms.4jawaly.com/api/v1/account/area/sms/send";
      $headers = [
          "Accept: application/json",
          "Content-Type: application/json",
          "Authorization: Basic {$app_hash}"
      ];

      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($messages));
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $response = curl_exec($curl);
      $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);

      $response_json = json_decode($response, true);

      if ($status_code == 200) {
          if (isset($response_json["messages"][0]["err_text"])) {
            return $this->returnError('error', $response_json["messages"][0]["err_text"]);
            //   return  $response_json["messages"][0]["err_text"];
          } else {
            return $this->returnData('code', $code, __('user.sent_phone'));
            //   return "تم الارسال بنجاح  " . " job id:" . $response_json["job_id"];
          }
      } elseif ($status_code == 400) {
        return $this->returnError('error', $response_json["message"]);
        //   return $response_json["message"];
      } elseif ($status_code == 422) {
        return $this->returnError('error', "نص الرسالة فارغ");
        //   return "نص الرسالة فارغ";
      } else {
        return $this->returnError('error', "محظور بواسطة كلاودفلير. Status code: {$status_code}");
        //   return "محظور بواسطة كلاودفلير. Status code: {$status_code}";
      }
    } // end of sendOTP


    //update user location
    public function updateLocation(Request $request)
    {
        date_default_timezone_set('Africa/Cairo');

        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required',
                'address'           => 'required|string',
                'lat'             => 'required|numeric',
                'lng'            => 'required|numeric',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
                'lat.required'         => __('user.reqLat'),
                'lng.required'        => __('user.reqLong'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);

        if ($user) {
            $user->address = $request->address;
            $user->lat = $request->lat;
            $user->lng = $request->lng;
            $user->save();
            return $this->returnData('msg', __("user.locationSuccess"));
        } else {
            return $this->returnError('error', __("user.usernotexist"));
        }
    }


    public function deleteUser()
    {
        $user = auth()->guard('api')->user();

        if($user){
            if ($user->delete()) {
                return $this->returnSuccessMessage(__('app/public.Account_deleted_successfully'));
            }
        }
        return $this->returnError('404', __('app/public.Something_went_wrong_please_try_again_later'));
    } // end of deleteUser

    //rate user and ad
    public function rateUser(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'   =>'required|exists:users,id',
                'rate_user_id'   =>'required|exists:users,id',
                'ad_id'   =>'nullable|exists:advertisements,id',
                'rate'      => 'required|numeric|between:0,5',
                'comment'   => 'nullable|string',
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }


        if($request->ad_id){
            $user_rate_ad = UserRate::where(['user_id'=> $request->user_id, 'ad_id' => $request->ad_id])->first();

            if($user_rate_ad){ // rate ad
                $user_rate_ad->update($request->all());

                $user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
                $user = User::find($request->rate_user_id);
                $user->avg_rate = $user_Rate;
                $user->save();

            }else{
                UserRate::create($request->all());

                $user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
                $user = User::find($request->rate_user_id);
                $user->avg_rate = $user_Rate;
                $user->count_rate += 1;
                $user->save();
            }

        }else{ // rate user

            $user_rate = UserRate::where(['user_id'=> $request->user_id, 'rate_user_id' => $request->rate_user_id])->first();

            if($user_rate){
                $user_rate->update($request->all());

                $avg_user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
                $user = User::find($request->rate_user_id);
                $user->avg_rate = $avg_user_Rate;
                $user->save();

            }else{
                UserRate::create($request->all());

                $avg_user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
                $user = User::find($request->rate_user_id);
                $user->avg_rate = $avg_user_Rate;
                $user->count_rate += 1;
                $user->save();
            }
        }


        return $this->returnSuccessMessage(__('users.review_added'));
    } // end of rateUser


    // //rate user and ad
    // public function rateUser(Request $request)
    // {
    //     $validator = Validator::make(
    //         $request->all(),
    //         [
    //             'user_id'   =>'required|exists:users,id',
    //             'rate_user_id'   =>'required|exists:users,id',
    //             'ad_id'   =>'nullable|exists:advertisements,id',
    //             'rate'      => 'required|numeric|between:0,5',
    //             'comment'   => 'nullable|string',
    //         ]
    //     );

    //     if ($validator->fails()) {
    //         $code = $this->returnCodeAccordingToInput($validator);
    //         return $this->returnValidationError($code, $validator);
    //     }

    //     if($request->ad_id){
    //         $user_rate_ad = UserRate::where(['user_id'=> $request->user_id, 'ad_id' => $request->ad_id])->first();

    //         if($user_rate_ad){
    //             $user_rate_ad->update($request->all());

    //             $user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
    //             $user = User::find($request->rate_user_id);
    //             $user->avg_rate = $user_Rate;
    //             $user->save();

    //         }else{
    //             UserRate::create($request->all());

    //             $user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
    //             $user = User::find($request->rate_user_id);
    //             $user->avg_rate = $user_Rate;
    //             $user->count_rate += 1;
    //             $user->save();
    //         }

    //     }else{
    //         UserRate::create($request->all());

    //         $user_Rate = UserRate::where('rate_user_id', $request->rate_user_id)->avg('rate');
    //         $user = User::find($request->rate_user_id);
    //         $user->avg_rate = $user_Rate;
    //         $user->count_rate += 1;
    //         $user->save();
    //     }
    //     return $this->returnSuccessMessage(__('users.review_added'));
    // } // end of rateUser


    //Review user
    public function ReviewUser(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'   =>'required|exists:users,id',
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $reviews = UserRate::with(['user'])->where('rate_user_id', $request->user_id)->get();
        $reviews = ReviewResource::collection($reviews);

        return $this->returnData('reviews' , $reviews);
    } // end of Review


    //change status of user (active or inactive)
    public function activateUser(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required|exists:users,id',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if($user){
            $user->active = 1;
            $user->save();

            return $this->returnSuccessMessage("user has been activated");
        }

    }
    public function deactivateUser(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required|exists:users,id',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if($user){
            $user->active = 0;
            $user->save();

            return $this->returnSuccessMessage("user has been deactivated");
        }

    }
    //*********************************************Notifications**************************** */
    public function updateFcmToken(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required',
                'fcm_token'   => 'nullable',
                'mobile_id'   => 'required|between:0,1'
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);

        if ($user) {
            $user->fcm_token = $request->fcm_token;
            $user->mobile_id = $request->mobile_id;
            $user->save();
            return $this->returnSuccessMessage("fcm token has been updated");
        } else {
            return $this->returnError('error', __("user.usernotexist"));
        }
    }

    public function getNotifications(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);

        if ($user) {
            $notifications = SentNotification::where('user_id', $request->user_id)->get();
            return $this->returnData('notifications', $notifications);
        } else {
            return $this->returnError('error', __("user.usernotexist"));
        }
    }

    //users feedback
    public function feedback(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required|exists:users,id',
                'message'     => 'required'
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        Feedback::create($request->all());
        return $this->returnSuccessMessage('Your feedback has been sent');
    }
}
