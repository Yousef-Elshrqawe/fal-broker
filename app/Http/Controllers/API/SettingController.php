<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Main\SliderResource;
use App\Models\AboutUs;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Terms;
use App\Models\User;
use App\Models\UserCoupon;
use Illuminate\Http\Request;
use App\Traits\backendTraits;
use Illuminate\Support\Facades\Validator;

class SettingController extends BaseController
{
    use backendTraits;

    public function getSliders(){
        $sliders =Slider::get();
        $sliders = SliderResource::collection($sliders);
        
        return $this->returnData('sliders', $sliders);
    }

    public function getTerms(){
        $term = Terms::select("term_" .app()->getLocale() . ' as term')->first();
        return $this->returnData('term', $term);
    }

    public function getAboutUs(){
        $about = AboutUs::select("about_" .app()->getLocale() . ' as about')->first();
        return $this->returnData('about', $about);
    }

    public function getContactUs(){
        $contact = ContactUs::first();
        return $this->returnData('contact', $contact);
    }

    public function checkCoupon(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                "user_id"=> 'required|exists:users,id',
                'code' => 'required',
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        $coupon = Coupon::where('code',$request->code)->first();
        if($coupon){
            $timesOfUses = UserCoupon::where(['user_id'=>$user->id, 'coupon_id'=>$coupon->id])->count();
            if($timesOfUses < $coupon->times_for_user){
                $coupon->is_valid = 1;
                return $this->returnData('coupon',$coupon);
            }else{
                $coupon->is_valid = 0;
                return $this->returnData('coupon',$coupon);
            }
        }else{
            return $this->returnError('',__('user.false_coupon'));
        }
    }
}
