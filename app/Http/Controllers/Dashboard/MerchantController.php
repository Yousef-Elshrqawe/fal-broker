<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Exports\MerchantsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\backendTraits;
// use Excel;


class MerchantController extends Controller
{
    use backendTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['merchant'] = User::where('type', 1)->when($request->search , function ($q) use ($request){
            return $q->where('phone' , 'like' , '%'. $request->search. '%')
            ->orWhere('name' , 'like' , '%'. $request->search. '%')
            ->orWhere('email' , 'like' , '%'. $request->search. '%');
        })->latest()->get();

      return view('dashboard.merchant.index')->with($data);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['merchant'] = User::with('subscription')->find($id);

        return view('dashboard.merchant.show')->with($data);
    }


    public function changeStatus( $id ) {
        $User            = User::findOrFail($id);
        $User['active']  = ! $User['active'];
        $User->save();
        return redirect()->back();
    } // end of changeStatus


    public function addComments(Request $request) {
        $User            = User::findOrFail($request->id);
        $User['comments']  = $request->comments;
        $User->save();
        session()->flash('success', __('user.updated_successfully'));
        return redirect()->back();
    } // end of addComments


    public function changeDependence( $id ) {
        $User            = User::findOrFail($id);
        $User['dependence']  = ! $User['dependence'];
        $User->save();
        return redirect()->back();
    } // end of changeStatus


    public function changeViewRate( $id ) {
        $User             = User::findOrFail($id);
        $User['view_rate'] = ! $User['view_rate'];
        $User->save();
        return redirect()->back();
    } // end of changeStatus



     //merchants Excel
    public function merchantsPrintExcel(Request $request)
    {
        $merchants = User::select("name", "email", "country_code", "phone", "address", "commercial_registration_no", "avg_rate", "count_rate", "count_followes")
            ->where('type', 1)->when($request->search , function ($q) use ($request){
            return $q->where('phone' , 'like' , '%'. $request->search. '%')
            ->orWhere('name' , 'like' , '%'. $request->search. '%')
            ->orWhere('email' , 'like' , '%'. $request->search. '%');
        })->latest()->get()->toArray();

        return Excel::download(new MerchantsExport($merchants) , 'MerchantsPrint.xlsx');
    }// end of merchantsPrintExcel



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->image != 'default.png') {
            $this->deleteFile( pathinfo($user->image)['basename'], 'uploads/customers_images/');
            // Storage::disk('uploads')->delete('/customers_images/' . $user->image);
        }

        if ($user->imageId != 'default2.png') {
            $this->deleteFile( pathinfo($user->idImage)['basename'], 'uploads/customers_images/');
            // Storage::disk('uploads')->delete('/customers_images/' . $user->idImage);
        }

        $deleted = $user->delete();
        if($deleted){
            session()->flash('success', __('user.deleted_successfully'));
        }
        return redirect()->back();
    } // end of destroy

} // end of MerchantController
