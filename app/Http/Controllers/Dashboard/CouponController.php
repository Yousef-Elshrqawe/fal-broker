<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['coupons'] = Coupon::get();

        $data['total'] = $data['coupons']->count();

        return view('dashboard.coupons.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code'      =>'required',
            'percent'   =>'required|numeric',
            'end_date'  =>'required|date',
            'count_of_use'=>'required|numeric',
            'times_for_user'=>'required|numeric'
        ],
        [

        ]);
        
        $data = $request->except('_token');
        Coupon::create($data);

        session()->flash('success', __('user.added_successfully'));

        return redirect()->route('admin.coupons.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::find($id);
        // $coupon->end_date = Carbon::parse($coupon->end_date)->format('yy-m-d');
        // return $coupon;
        return view('dashboard.coupons.edit',compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'code'      =>'required',
            'percent'   =>'required|numeric',
            'end_date'  =>'required|date',
            'count_of_use'=>'required|numeric',
            'times_for_user'=>'required|numeric'
        ],[

        ]);

        $coupon = Coupon::find($id);
        $data = $request->all();
        
        $coupon->update($data);

        session()->flash('success', __('user.updated_successfully'));

        return redirect()->route('admin.coupons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        session()->flash('success', __('user.deleted_successfully'));

        return redirect()->back();
    }
}
