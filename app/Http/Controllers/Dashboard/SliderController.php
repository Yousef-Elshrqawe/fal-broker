<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use LaravelLocalization;

use App\Traits\backendTraits;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    use backendTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['slider'] = Slider::select('id','cat_id','subcat_id',
        'image_en','image_ar','desc_'.app()->getLocale().' AS desc','link')->paginate(20);

        $data['total'] = Slider::count();
        $data['cats']  = Category::select('id', 'name_ar', 'name_en', 'name_' . app()->getLocale() . ' as name')->get();
        return view('dashboard.sliders.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['cats']  = Category::select('id', 'name_ar', 'name_en', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name')->get();
        //$data['products']  = Product::select('id', 'name_ar', 'name_en', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name')->get();
        return view('dashboard.sliders.create')->with($data);
    }

    public function ajaxLoad(Request $request)
    {

        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = Subcategory::select('id', 'name_ar', 'name_en', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name')
            ->where('cat_id', $value)->get();
        $output = '<option value="">' . __('user.select_subcat') . '</option>';
        foreach ($data as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        echo $output;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'image_ar'  => 'required',
                'image_en'   => 'required',
            ],
            [
                'image_en.required' => __('user.req_image_en'),
                'image_ar.required'  => __('user.req_image_ar'),

            ]
        );
        $data = $request->except('image_en', 'image_ar');
        $data['image_ar'] = $this->upploadImage($request->image_ar, 'uploads/sliders/');
        sleep(1); //to delay image saving beacuse the two images have the same name in ordinary case
        $data['image_en'] = $this->upploadImage($request->image_en, 'uploads/sliders/');


        Slider::create($data);

        session()->flash('success', __('user.added_successfully'));

        return redirect()->route('admin.slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['slider'] = Slider::find($id);
        $data['cats']  = Category::select('id', 'name_ar', 'name_en', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name')->get();
        $data['products']  = Category::select('id', 'name_ar', 'name_en', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name')->get();
        return view('dashboard.sliders.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        // $request->validate(
        //     [
        //         'image_ar'  => 'required',
        //         'image_en'   => 'required',
        //     ],
        //     [
        //         'image_en.required' => __('user.req_image_en'),
        //         'image_ar.required'  => __('user.req_image_ar'),

        //     ]
        // );


        $data = $request->except('image_en', 'image_ar');

        if ($slider) {
            if ($request->image_en && ($slider->image_en != $request->image_en)) {
                $this->deleteFile( pathinfo($slider->image_en)['basename'], 'uploads/sliders/');
                // Storage::disk('uploads')->delete('sliders/' . $slider->image_en);
                $data['image_en'] = $this->upploadImage($request->image_en, 'uploads/sliders/');
            }
            if ($request->image_ar && $slider->image_ar != $request->image_ar) {
                // sleep(1);
                $this->deleteFile( pathinfo($slider->image_ar)['basename'], 'uploads/sliders/');
                // Storage::disk('uploads')->delete('sliders/' . $slider->image_ar);
                $data['image_ar'] = $this->upploadImage($request->image_ar, 'uploads/sliders/');
            }
        }



        $slider->update($data);

        session()->flash('success', __('user.updated_successfully'));

        return redirect()->route('admin.slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        if ($slider->image_ar != 'default.png') {
            $this->deleteFile( pathinfo($slider->image_ar)['basename'], 'uploads/sliders/');
            // Storage::disk('uploads')->delete('sliders/' . $slider->image_ar);
        }
        if ($slider->image_en != 'default.png') {
            $this->deleteFile( pathinfo($slider->image_en)['basename'], 'uploads/sliders/');
            // Storage::disk('uploads')->delete('sliders/' . $slider->image_en);
        }
        $slider->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->route('admin.slider.index');
    }
}
