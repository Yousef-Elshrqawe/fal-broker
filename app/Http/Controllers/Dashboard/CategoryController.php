<?php

namespace App\Http\Controllers\Dashboard;

use LaravelLocalization;
use App\Http\Controllers\Controller;
use App\Models\{Category, Attribute};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Traits\backendTraits;

class CategoryController extends Controller
{
    use backendTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['cats'] = Category::select(
            'id',
            'name_ar',
            'name_en',
            'type_name_ar',
            'type_name_en',
            'details_name_ar',
            'details_name_en',
            'details_name_' . app()->getLocale() . ' as details_name',
            'type_name_' . app()->getLocale() . ' as type_name',
            'name_' . app()->getLocale() . ' as name',
            'image',
            'sorting',
            'icon'
        )->when($request->search, function ($q) use ($request) {
                return $q->where('name_ar', 'like', '%' . $request->search . '%')
                    ->orWhere('name_en', 'like', '%' . $request->search . '%');
            })->get();


        $data['total'] = Category::count();

        return view('dashboard.categories.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $total = Category::count();
        $data['sorting'] = $total + 1;

        return view('dashboard.categories.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name_ar' => 'required|unique:categories|max:255',
            'name_en' => 'required|unique:categories|max:255',
            'type_name_ar' => 'required|max:255',
            'type_name_en' => 'required|max:255',
            'details_name_ar' => 'required|max:255',
            'details_name_en' => 'required|max:255',
            'sorting' => 'required',
            'image'   => 'nullable',
            'icon'   => 'nullable',
        ], [
            'name_ar.required'  => __('user.reqCatAr'),
            'name_ar.unique'    => __('user.uniqueCatAr'),
            'name_en.required'  => __('user.reqCatEn'),
            'name_en.unique'    => __('user.uniqueCatEn'),
            'image.required'             => __('user.reqImage'),
        ]);

        $data = $request->except(['image','icon']);

        //save image if existed
        if ($request->image) {
            $data['image'] = $this->upploadImage($request->image, 'uploads/categories/');
            // sleep(1);
        } else {
            //$data['image'] = 'default.png';
        }

        //save icon if existed
        if ($request->icon) {
            $data['icon'] = $this->upploadImage($request->icon, 'uploads/categories/');
        } else {
            //$data['icon'] = 'default.png';
        }
        // $total = Category::count();
        // $data['sorting'] = $total + 1;

        Category::create($data);

        session()->flash('success', __('user.added_successfully'));

        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data['cat']    = Category::find($id);
        $data['attributes'] = $data['cat']->attributes;

        $data['total'] = Attribute::where('cat_id', $data['cat']->id)->count();

        return view('dashboard.attributes.index')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $cats = Category::get();
        return view('dashboard.categories.edit', compact('category', 'cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $id = $request->id;
        $this->validate($request, [
            'name_ar' => 'required|max:255|unique:categories,name_ar,' . $id,
            'name_en' => 'required|max:255|unique:categories,name_en,' . $id,
            'type_name_ar' => 'required|max:255',
            'type_name_en' => 'required|max:255',
            'details_name_ar' => 'required|max:255',
            'details_name_en' => 'required|max:255',
            'sorting' => 'required',
            'image'   => 'nullable',
            'icon'    => 'nullable',
        ], [

            'name_ar.required'  => __('user.reqCatAr'),
            'name_ar.unique'    => __('user.uniqueCatAr'),
            'name_en.required'  => __('user.reqCatEn'),
            'name_en.unique'    => __('user.uniqueCatEn'),
        ]);
        $category = Category::find($id);
        $data = $request->all();
        $old_img = $category->image ;
        if ($request->image) {
            if ($request->image != 'default.png')
            $this->deleteFile( pathinfo($category->image)['basename'], 'uploads/categories/');
            // Storage::disk('uploads')->delete('categories/' . $old_img);
            $data['image'] = $this->upploadImage($request->image, 'uploads/categories/');
            // sleep(1);
        } else {
            $data['image'] = $old_img;
        }

        //update icon if existed
        $oldIcon = $category->icon;
        if ($request->icon) {
            if ($request->icon != 'default.png')
            $this->deleteFile( pathinfo($category->icon)['basename'], 'uploads/categories/');
                // Storage::disk('uploads')->delete('categories/' . $oldIcon);
            $data['icon'] = $this->upploadImage($request->icon, 'uploads/categories/');
        } else {
            $data['icon'] = $oldIcon;
        }

        $category->update($data);

        session()->flash('success', __('user.updated_successfully'));

        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->image != 'default.png') {
            $this->deleteFile( pathinfo($category->image)['basename'], 'uploads/categories/');
            // Storage::disk('uploads')->delete('categories/' . $category->image);
        }

        if ($category->icon != 'default.png') {
            $this->deleteFile( pathinfo($category->icon)['basename'], 'uploads/categories/');
            // Storage::disk('uploads')->delete('categories/' . $category->icon);
        }

        $category->delete();

        session()->flash('success', __('user.deleted_successfully'));

        return redirect()->back();
    }
}
