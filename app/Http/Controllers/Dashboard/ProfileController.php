<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function profile()
    {
        return view('dashboard.profiles.profile');
    }

    public function editProfile(){
        $admin = Auth::guard('admin')->user();
        return view('dashboard.profiles.edit',compact('admin'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $request->validate([
            'first_name'  => 'required',
            'last_name'   => 'required',
            'username'    => ['required', Rule::unique('admins')->ignore($admin->id)],
            'email'       => ['required', Rule::unique('admins')->ignore($admin->id)],
            'image'       => 'nullable',
        ],[
            'first_name.required' => __('user.req_first_name'),
            'last_name.required'  => __('user.req_last_name'),
            'username.required'   => __('user.req_username'),
            'username.unique'     => __('user.unique_username'),
            'email.unique'        => __('user.unique_email'),
        ]
    );
        $data = $request->except(['image']);

        if ($request->image) {

            if ($admin->image != 'default.png') {
                Storage::disk('uploads')->delete('/users_images/' . $admin->image);
            }

            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })
                ->save(public_path('uploads/users_images/' . $request->image->hashName()));
            $data['image'] = $request->image->hashName();
        }

        $admin->update($data);

        session()->flash('success', __('user.updated_successfully'));

        return redirect()->back();
    }

    public function changePassword()
    {
        $admin = Auth::guard('admin')->user();
        return view('dashboard.profiles.change_password', compact('admin'));
    }

      /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function change_password_method(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $request->validate([
            'old_password' => 'required',
            'password'     => 'required|confirmed',
        ]);
       // dd(Hash::check($request->old_password, $user->password));
        if(Hash::check($request->old_password, $admin->password))
        {
            $admin->fill([
                'password' => Hash::make($request->password)
            ])->save();

            session()->flash('success', __('user.updated_successfully'));
            return redirect()->back();
        }
        else
        {
            return back()->withErrors(__('user.worng_old_password'));

        }
    }

}
