<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Advertisement;
use App\Models\Category;
use App\Models\Country;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $data['user']  = User::where('type', 0)->count();
        $data['merchant']  = User::where('type', 1)->count();
        $data['admin'] = Admin::count();
        $data['country']         = Country::count();
        $data['category']       = Category::count();
        $data['subscription']       = Subscription::count();
        $data['advertisements'] = Advertisement::count();

        return view('dashboard.index')->with($data);
    }// end of index
}
