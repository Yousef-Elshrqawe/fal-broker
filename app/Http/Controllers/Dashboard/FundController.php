<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\{Fund, ContactUs};
use Illuminate\Http\Request;

class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = ContactUs::first();
        $funds = Fund::with('user')->get();
        return view('dashboard.funds.index' , compact('funds', 'setting'));
    }

    public function financingRequest(){
        $setting = ContactUs::first();
        $setting['financing_request']  = ! $setting['financing_request'];
        $setting->save();
        return redirect()->back();
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fund = Fund::with('user')->find($id);
        return view('dashboard.funds.show' , compact('fund'));
    }



    public function edit($id)
    {
        //
    }



    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name_ar'    => 'required',
                'name_en'    => 'required',
                'ads_number' => 'required',
                'price'      => 'required',
                'user_type'  => 'required',
            ],
        );

        $sub = Subscription::find($id);
        $updated = $sub->update($request->all());

        if($updated)
            session()->flash('success', __('user.updated_successfully'));
        else
            session()->flash('success','something wentwrong!');

        return redirect()->route('admin.subscriptions.index');
    }


    public function destroy($id)
    {
        $fund = Fund::find($id);
        $deleted = $fund->delete();

        if($deleted)
            session()->flash('success', __('user.deleted_successfully'));
        else
            session()->flash('success','something wentwrong!');

        return redirect()->back();
    } // end of destroy

} // end of FundController
