<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SpecialAdvertisement;
use Illuminate\Http\Request;

class SpecialAdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['specialAds'] = SpecialAdvertisement::with('category')->get();

        $data['total'] = SpecialAdvertisement::count();
        $data['cats'] = Category::select('id','name_'.app()->getLocale().' as name')->get();

        return view('dashboard.special_advertisements.index')->with($data);
    } // end of index

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd('0');
        // $data['cats'] = Category::select('id','name_'.app()->getLocale().' as name')->get();
        // return view('dashboard.special-advertisements.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'expiration'  => 'required|numeric|min:0',
            'price'       => 'required|numeric|min:0',
            'cat_id'  => 'required|exists:categories,id'
        ]
        );
            $data = $request->all();

            SpecialAdvertisement::create($data);

            session()->flash('success', __('user.added_successfully'));

            return redirect()->route('admin.special-advertisements.index');
    } // end of store

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specialAd = SpecialAdvertisement::find($request->id);
        $request->validate([
            'expiration'  => 'required|numeric|min:0',
            'price'       => 'required|numeric|min:0',
            'cat_id'=>'required|exists:categories,id',
        ]);
        $data = $request->except('_token','_method');

        $specialAd->update($data);
        session()->flash('success', __('user.updated_successfully'));
        return redirect()->route('admin.special-advertisements.index');
    } // end of update

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $specialAd = SpecialAdvertisement::find($id);
        $specialAd->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->route('admin.special-advertisements.index');
    } // end of destroy

}
