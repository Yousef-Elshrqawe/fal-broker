<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{User, UserCoupon, Advertisement, Feedback, AdvertisementFee};
use App\Models\{ContactUs, AboutUs, Terms, FormContactUs, Subscription, UsersSubscription};
use App\Traits\backendTraits;
use Carbon\Carbon;
use App\Exports\ReportExport;
use Maatwebsite\Excel\Facades\Excel;


//use App\Models\DeliveryCost;

class SettingController extends Controller
{
    use backendTraits;

    public function contactUs(){
        $setting = ContactUs::first();
        return view('dashboard.setting.contact_us',compact('setting'));
    }

    public function editContactUs(Request $request,$id){
        $request->validate([
             'phone'    => 'nullable',
             'twitter'    => 'nullable',
             'snap_chat'    => 'nullable',
             'tik_tok'    => 'nullable',
             'instagram'    => 'nullable',
             'facebook'    => 'nullable',
             'website'  => 'required',
             'email'    => 'required',
             'complaint_email'    => 'required',
             'suggestions_email'   => 'required',
             'count_ads_latest'    => 'required',
             'financing_request'  => 'required|in:0,1',
         ]);

         $contact  = Contactus::find($id);
         $contact->phone  = $request->phone;
         $contact->website   = $request->website;
         $contact->email     = $request->email;
         $contact->complaint_email  = $request->complaint_email;
         $contact->suggestions_email  = $request->suggestions_email;
         $contact->twitter   = $request->twitter;
         $contact->snap_chat = $request->snap_chat;
         $contact->tik_tok   = $request->tik_tok;
         $contact->instagram = $request->instagram;
         $contact->facebook  = $request->facebook;
         $contact->financing_request  = $request->financing_request;
         $contact->count_ads_latest  = $request->count_ads_latest;
         $contact->save();


         session()->flash('success', __('user.updated_successfully'));
         return redirect()->back();
     } // end of editContactUs

    public function banner(){
        $setting = ContactUs::first();
        return view('dashboard.setting.banner',compact('setting'));
    }

    public function editBanner(Request $request,$id){
        $request->validate([
             'image_banner'    => 'nullable|image',
             'link_banner'    => 'nullable',
         ]);

         $contact  = Contactus::find($id);
         $contact->link_banner   = $request->link_banner;
         $contact->save();

         $photo_name = $contact->getAttributes()['image_banner'];

         if ($request->hasFile('image_banner')) {
            $this->deleteFile($photo_name, 'assets/img/banner_images/');
            $contact->image_banner  =$this->upploadImage($request->image_banner, 'assets/img/banner_images/');
            $contact->save();
         }
         session()->flash('success', __('user.updated_successfully'));

         return redirect()->back();
    } // end of editBanner

    public function deleteBanner($id){
         $contact  = Contactus::find($id);
         $photo_name = $contact->getAttributes()['image_banner'];
         $this->deleteFile($photo_name, 'assets/img/banner_images/');

         $contact->image_banner = null;
         $contact->save();

         session()->flash('success', __('user.deleted_successfully'));
         return redirect()->back();
    } // end of deleteBanner


     public function terms(){
        $setting = Terms::first();
        return view('dashboard.setting.terms',compact('setting'));
     }

     public function editTerms(Request $request,$id){
        $request->validate([
             'term_ar'    => 'required',
             'term_en'    => 'required',
         ]);

         $term  = Terms::find($id);
         $term->term_ar     = $request->term_ar;
         $term->term_en     = $request->term_en;
         $term->save();

         session()->flash('success', __('user.updated_successfully'));

         return redirect()->back();
     }



     public function about(){
        $setting = AboutUs::first();
        return view('dashboard.setting.about',compact('setting'));
     }

     public function editAbout(Request $request,$id){
        $request->validate([
             'about_ar'    => 'required',
             'about_en'    => 'required',
         ]);

         $term  = AboutUs::find($id);
         $term->about_ar     = $request->about_ar;
         $term->about_en     = $request->about_en;
         $term->save();

         session()->flash('success', __('user.updated_successfully'));

         return redirect()->back();
     }

    // public function deliveryCostAndTax(){
    //     $oneMonthCost = DeliveryCost::where('flag',0)->first()->cost;
    //     $threeMonthCost = DeliveryCost::where('flag',1)->first()->cost;
    //     $tax = DeliveryCost::where('flag',2)->first()->cost;

    //     return view('dashboard.delivery_cost_and_tax.index')->with(compact('oneMonthCost','threeMonthCost','tax'));
    // }

    // public function updateDeliveryCostAndTax(Request $request){

    //     $this->validate($request, [
    //         'one_month_cost' => 'required|numeric|min:0',
    //         'three_month_cost' => 'required|numeric|min:0',
    //         'tax' => 'required|numeric|min:0',
    //     ]);

    //     DeliveryCost::where('flag',0)->update(['cost' => $request->one_month_cost]);
    //     DeliveryCost::where('flag',1)->update(['cost' => $request->three_month_cost]);
    //     DeliveryCost::where('flag',2)->update(['cost' => $request->tax]);

    //     session()->flash('success', __('user.updated_successfully'));
    //     return redirect()->back();
    // }

    //users feedback
    public function feedback(Request $request){
        $feedbacks = Feedback::with('user')->get();
        $total = $feedbacks->count();
        // $feedbacks = Feedback::get();
        // foreach($feedbacks as $feedback){
        //     $feedback->user_name = User::find($feedback->user_id)->name;
        // }
        return view('dashboard.setting.feedback',compact('feedbacks','total'));
    }

    public function formContactUs(){
        $formContactUs = FormContactUs::get();
        $total = $formContactUs->count();
        return view('dashboard.setting.formContactUs',compact('formContactUs','total'));
    }

    //report
    public function report(Request $request){
        $from = Carbon::now()->subYear();
        $to = Carbon::now();
        if($request->to && $request->to != null){
            $to = $request->to;
        }
        if($request->from && $request->from != null){
            $from = $request->from;
        }

        $data['usedCoupons'] = UserCoupon::whereBetween('updated_at',[$from, $to])->select('coupon_id')->groupBy('coupon_id')->get();
        $data['totalCouponsDiscount'] = UserCoupon::whereBetween('updated_at',[$from, $to])->sum('discount_value');
        $data['advertisementsActivated'] = Advertisement::where('finished',0)->whereBetween('updated_at',[$from, $to])->count();
        $data['advertisementsFinished'] = Advertisement::where('finished',1)->whereBetween('updated_at',[$from, $to])->count();
        $data['advertisementsIsSubscribed'] = Advertisement::where(['is_subscribed'=>1])->whereBetween('updated_at',[$from, $to])->count();
        $data['advertisementsPopular'] = Advertisement::where(['popular'=>1])->whereBetween('updated_at',[$from, $to])->count();
        $data['users'] = User::where(['type'=> 0])->whereBetween('updated_at',[$from, $to])->count();
        $data['merchants'] = User::where(['type'=> 1])->whereBetween('updated_at',[$from, $to])->count();
        $usersSubscriptions = UsersSubscription::whereBetween('updated_at',[$from, $to])->get();
        $data['totalUsersSubscriptions'] = 0;
        foreach($usersSubscriptions as $userSubscription){
            $subscription = Subscription::find($userSubscription->subscription_id);
            if($subscription){
                $data['totalUsersSubscriptions'] += $subscription->price;
            }
        }

        $fees = 0;
        $ad_fees = AdvertisementFee::first();
        if($ad_fees){
            $fees = $ad_fees->fees;
        }
        $data['totalAdsEarnings'] = Advertisement::whereBetween('updated_at',[$from, $to])
        ->where('is_subscribed',0)->count() * $fees;

        $data['finalTotalEarnings'] = $data['totalUsersSubscriptions'] + $data['totalAdsEarnings'] - $data['totalCouponsDiscount'];

        return view('dashboard.report.index')->with($data);
    } //

     //reportPrint
    public function reportPrintExcel(Request $request){
        $from = Carbon::now()->subYear();
        $to = Carbon::now();
        if($request->to && $request->to != null){
            $to = $request->to;
        }
        if($request->from && $request->from != null){
            $from = $request->from;
        }
        // $data= ['' => , ];
        // return AdvertisementFee::get();

        $row =(object)[];
        $row->advertisementsActivated = Advertisement::where('finished',0)->whereBetween('updated_at',[$from, $to])->count();
        $row->advertisementsFinished = Advertisement::where('finished',1)->whereBetween('updated_at',[$from, $to])->count();
        $row->advertisementsIsSubscribed = Advertisement::where(['is_subscribed'=>1])->whereBetween('updated_at',[$from, $to])->count();
        $row->advertisementsPopular = Advertisement::where(['popular'=>1])->whereBetween('updated_at',[$from, $to])->count();
        $row->users = User::where(['type'=> 0])->whereBetween('updated_at',[$from, $to])->count();
        $row->merchants = User::where(['type'=> 1])->whereBetween('updated_at',[$from, $to])->count();
        $row->totalUsersSubscriptions = 0;
        $usersSubscriptions = UsersSubscription::whereBetween('updated_at',[$from, $to])->get();
        foreach($usersSubscriptions as $userSubscription){
            $subscription = Subscription::find($userSubscription->subscription_id);
            if($subscription){
                $row->totalUsersSubscriptions += $subscription->price;
            }
        }
        $fees = 0;
        $ad_fees = AdvertisementFee::first();
        if($ad_fees){
            $fees = $ad_fees->fees;
        }
        $row->totalAdsEarnings = Advertisement::whereBetween('updated_at',[$from, $to])
        ->where('is_subscribed',0)->count() * $fees;
        $usedCoupons = UserCoupon::whereBetween('updated_at',[$from, $to])->select('coupon_id')->groupBy('coupon_id')->get();
        $row->usedCoupons = $usedCoupons->count();
        $row->totalCouponsDiscount = UserCoupon::whereBetween('updated_at',[$from, $to])->sum('discount_value');
        $row->finalTotalEarnings = $row->totalUsersSubscriptions + $row->totalAdsEarnings - $row->totalCouponsDiscount;
        $data[]= $row;
        // return $data;
        return Excel::download(new ReportExport($data) , 'reportPrint.xlsx');

    }// end of reportPrintExcel

}
