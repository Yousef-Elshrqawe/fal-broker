<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\District;
use App\Models\Governorate;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function index(Country $country,  $govern){
        $govern = Governorate::find($govern);
        return view('dashboard.country.governorates.districts.index',compact('country','govern'));
    }

    public function store(Request $request, $country, $govern)
    {
      
        $request->validate([
            'name_ar'  => 'required|string',
            'name_en'  => 'required|string',
            'govern_id'=>'required|exists:governorates,id'
        ],[
            
        ]
        );
            $data = $request->except('_token');

            $district = District::create($data);

            session()->flash('success', __('user.added_successfully'));

            return redirect()->route('admin.districts.index',[$country,$govern]);

    }

    public function destroy($country, $govern, $district){
        $district = District::find($district);
        $district->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->route('admin.districts.index',[$country,$govern]);
    }
}
