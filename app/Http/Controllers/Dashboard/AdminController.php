<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Traits\backendTraits;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    use backendTraits;
    // public function __construct()
    // {
    //   $this->middleware(['permission:read_users'])->only('index');

    //   $this->middleware(['permission:create_users'])->only('create');

    //   $this->middleware(['permission:update_users'])->only('edit');

    //   $this->middleware(['permission:delete_users'])->only('destroy');
    // }//end of construct


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
           $data['users'] = Admin::when($request->search , function ($q) use ($request){
              return $q->where('username' , 'like' , '%'. $request->search. '%')
              ->orWhere('first_name' , 'like' , '%'. $request->search. '%')
              ->orWhere('last_name' , 'like' , '%'. $request->search. '%')
              ->orWhere('email' , 'like' , '%'. $request->search. '%');
          })->latest()->get();

          $data['total'] = Admin::count();

        return view('dashboard.users.index')->with($data);
    }


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.create');
    }


     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
        $request->validate([
            'first_name'  => 'required|string|min:3|max:20',
            'last_name'   => 'required|string|min:3|max:20',
            'username'    => 'required|string|min:3|max:20|unique:admins',
            'email'       => 'nullable|email|unique:admins',
            'image'       => 'nullable',
            'password'    => 'required|confirmed',
            'permissions' => 'required|min:1',
        ],[
            'first_name.required' => __('user.req_first_name'),
            'last_name.required'  => __('user.req_last_name'),
            'username.required'   => __('user.req_username'),
            'username.unique'     => __('user.unique_username'),
            'email.unique'        => __('user.unique_email'),
            'password.required'   => __('user.req_password'),
            'password.confirmed'  => __('user.password_confirmed'),
        ]
        );
            $data = $request->except(['password', 'password_confirmation', 'permissions', 'image']);
            $data['password'] = bcrypt($request->password);

            if ($request->image) {
                $data['image'] = $this->upploadImage($request->image, 'uploads/users_images');
             }else{
                $data['image'] = 'default.png';
             }
            $user = Admin::create($data);

            $user->attachRole('admin');

            $user->syncPermissions($request->permissions);

            session()->flash('success', __('user.added_successfully'));

            return redirect()->route('admin.users.index');
    }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::find($id);
        return view('dashboard.users.edit' , compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user = Admin::find($id);
        $request->validate([
            'first_name'  => 'required|string|min:3|max:20',
            'last_name'   => 'required|string|min:3|max:20',
            'username'    => ['required','string','min:3','max:20', Rule::unique('admins')->ignore($user->id)],
            'email'       => ['nullable', Rule::unique('admins')->ignore($user->id)],
            'image'       => 'nullable',
            'permissions' => 'required|min:1',
        ],
        [
            'first_name.required' => __('user.req_first_name'),
            'last_name.required'  => __('user.req_last_name'),
            'username.required'   => __('user.req_username'),
            'username.unique'     => __('user.unique_username'),
            'email.unique'        => __('user.unique_email'),
        ]);
        $data = $request->except(['permissions', 'image']);

        if ($request->image) {
            if ($user->image != 'default.png') {
                $this->deleteFile( pathinfo($user->image)['basename'], 'uploads/users_images/');
                // Storage::disk('uploads')->delete('/users_images/' . $user->image);
            }

            $data['image'] = $this->upploadImage($request->image, 'uploads/users_images');
        }

        $user->update($data);
        $user->syncPermissions($request->permissions);
        session()->flash('success', __('user.updated_successfully'));
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $admin = Admin::find($id);
        if ($admin->image != 'default.png') {
            $this->deleteFile( pathinfo($admin->image)['basename'], 'uploads/users_images/');
            // Storage::disk('uploads')->delete('/users_images/' . $admin->image);
        }

        $admin->delete();

        session()->flash('success',  __('user.deleted_successfully'));

        return redirect()->route('admin.users.index');
    }
}
