<?php

namespace App\Http\Controllers\Dashboard;
use LaravelLocalization;
use App\Http\Controllers\Controller;
use App\Models\Attribute ;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Traits\backendTraits;

class SubcategoryController extends Controller
{
    use backendTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , $cat)
    {
        $data['subcats'] = Subcategory::where('cat_id',$cat)->select('id','cat_id','name_ar','name_en','name_' . app()->getLocale() . ' as name','image', 'sorting')->
            when($request->search , function ($q) use ($request){
                return $q->where('name_ar' , 'like' , '%'. $request->search. '%')
                        ->orWhere('name_en' , 'like' , '%'. $request->search. '%');
            })->get();

        $data['cat_id'] = $cat;

       $data['total'] = $data['subcats']->count();

       return view('dashboard.subcategories.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cat)
    {
        $cat_id = $cat;
        $total = Subcategory::where('cat_id',$cat)->count();
        $sorting = $total + 1;

        return view('dashboard.subcategories.create' , compact('cat_id', 'sorting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $cat)
    {
        $validatedData = $request->validate([
            // 'name_ar' => 'required|unique:subcategories|max:255',
            'name_ar' => 'required|max:255',
            'name_en' => 'required|max:255',
            'sorting' => 'required',
            'image'   => 'nullable',
        ],[
            'name_ar.required'  => __('user.reqCatAr'),
            'name_ar.unique'    => __('user.uniqueCatAr'),
            'name_en.required'  => __('user.reqCatEn'),
            'name_en.unique'    => __('user.uniqueCatEn'),
            'image.required'             => __('user.reqImage'),
        ]);

        $data = $request->except(['image']);

        $data['cat_id'] = $cat;

        $uniqueName = Subcategory::where(['cat_id' => $cat, 'name_ar' => $request->name_ar])->first();
        if($uniqueName){
            session()->flash('error', 'هذا القسم موجود بالفعل ');
            return redirect()->back();
        }

        if ($request->image) {
            $data['image'] = $this->upploadImage($request->image, 'uploads/categories/');
         }else{
            $data['image'] = 'default.png';
         }

        Subcategory::create($data);

        session()->flash('success', __('user.added_successfully'));

        return redirect()->route('admin.subcategories.index' , $cat);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $cat , $id)
    {
        $data['cat'] = $cat;
        $data['subcat']    = Subcategory::find($id);
        $data['attributes'] = $data['subcat']->attributes;

        $data['total'] = $data['subcat']->attributes->count();

        return view('dashboard.attributes.index')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cat,$subcategory)
    {
        $subcat = Subcategory::find($subcategory);
        $cats = Category::select('id','name_'.app()->getLocale().' AS name')->get();
        return view('dashboard.subcategories.edit' , compact('subcat','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cat , $id)
    {

        $id = $request->id;
        $this->validate($request, [
            // 'name_ar' => 'required|max:255|unique:subcategories,name_ar,'.$id,
            'name_ar' => 'required|max:255',
            'name_en' => 'required|max:255',
            'sorting' => 'required',
            'image'   => 'nullable',
        ],[

            'name_ar.required'  => __('user.reqCatAr'),
            'name_ar.unique'    => __('user.uniqueCatAr'),
            'name_en.required'  => __('user.reqCatEn'),
            'name_en.unique'    => __('user.uniqueCatEn'),
        ]);
        $subcategory = Subcategory::find($id);
        $data = $request->all();
        $old_img = $subcategory->image;
        if ($request->image) {
            if($request->image != 'default.png')
                $this->deleteFile( pathinfo($subcategory->image)['basename'], 'uploads/categories/');

                // Storage::disk('uploads')->delete('categories/' . $old_img);
            $data['image'] = $this->upploadImage($request->image, 'uploads/categories/');
        }else{
            $data['image'] = $old_img;
        }

        $subcategory->update($data);

        session()->flash('success', __('user.updated_successfully'));

        return redirect()->route('admin.subcategories.index' , $cat);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cat , Subcategory $subcategory)
    {

        if($subcategory->image != 'default.png'){
            $this->deleteFile( pathinfo($subcategory->image)['basename'], 'uploads/categories/');
            // Storage::disk('uploads')->delete('categories/' . $subcategory->image);
        }

        $subcategory->delete();

        session()->flash('success', __('user.deleted_successfully'));

        return redirect()->back();
    }
}
