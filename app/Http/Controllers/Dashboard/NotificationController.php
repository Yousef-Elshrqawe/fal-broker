<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\SentNotification;
use App\Models\User;
use GuzzleHttp\Client;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    private static $sourceKey = 'AAAAIXCeN20:APA91bHpLAwz5UDnMjJI3sFBa8WAcxx_4KsovHdJF9u0BJS6JETGkjnniYgC-AcNweFk_NcVH-c7BZoRqrudsBqqzaLlDjCoHgq0cufPeqztbnGdE6AbqX3O9EE7qMgkGqnsSlO2myIE';
    // private static $sourceKey = '22AAAAqkyQ7G8:APA91bHJwH6u_o6BOJdrN1ePFxuvymTyNFHrPkIYSRMUdfz6XwT1hYXXM11dLYtNPjsmeoCUlj-Q0SDiyh_V5iLH-Rmk3WEGUzYZW0Lm3npmVljxNyeKt4qiLqTAEfoDgBG6PqWGtsPd';
    public function index()
    {
        return view('dashboard.notifications.index');
    }

    public function send(Request $request)
    {
        $users = User::get();
        $ios_tokens = [];
        $android_tokens = [];
        foreach($users as $user)
        {
            if($user->fcm_token != null && $user->mobile_id == 0)
                array_push($android_tokens,$user->fcm_token);
            else if($user->fcm_token != null && $user->mobile_id == 1)
                array_push($ios_tokens,$user->fcm_token);
        }
        //return $ios_tokens;
        $sourceKey = self::$sourceKey;

        $header = [
            'Authorization'=> 'key=' . $sourceKey,
            'Content-Type' => 'application/json'
        ];

        $data = [
            'title' => $request->title,
            'body'  => $request->body,
            'content_available'=>1,
            'sound' => 'default'
        ];

        // if (request()->has('image')) {
        //     $image = Image::make(request()->image)->resize(300, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     })
        //         ->save(public_path('uploads/notification_images/' . request()->image->hashName()));
        //     $data['image'] = request()->image->hashName();
        // }
        //return ($image);

        // if(isset($data['image'])){
        //             $data['image'] = asset('uploads/notification_images/'.$data['image']);
        // }

        $client = new Client(['headers' => $header]);

        //send notifications for android devices
        if(count($android_tokens)>0){
            $body = [
                'data'=> $data ,
                //'notification' => $data,
                //'to' => $tokens[3]
                'registration_ids' => $android_tokens,
                "priority" => "high",
                "delay_while_idle" => false
            ];

            $res = $client->post('https://fcm.googleapis.com/fcm/send',[
                'body' => json_encode($body),
            ]);
            //return $res;
        }

        //send notifications for ios devices
        if(count($ios_tokens)>0){
            $body = [
                'data'=> $data ,
                'notification' => $data,
                //'to' => $tokens[3]
                'registration_ids' => $ios_tokens,
            ];

            $res = $client->post('https://fcm.googleapis.com/fcm/send',[
                'body' => json_encode($body),
            ]);
            //return $res;
        }

        if ($request->save){
            $not = Notification::create([
                'title' => $request->title,
                'body' => $request->body
            ]);
            foreach($users as $user){
                if($user->fcm_token != null){
                    SentNotification::create([
                        'user_id' => $user->id,
                        'not_id' => $not->id,
                        'title' => $not->title,
                        'body'  => $not->body
                    ]);
                }
            }
        }

        session()->flash('success', __('user.sended_successfully'));
        return view('dashboard.notifications.index');
    }

    public static function sendSingleNotification($user_id, $title, $content, $ad_id = null)
    {
        $user = User::find($user_id);

        $sourceKey = self::$sourceKey;

        $header = [
            'Authorization'=> 'key=' . $sourceKey,
            'Content-Type' => 'application/json'
        ];
        $data = [
                'title' => $title,
                'body'  => $content,
                'ad_id' => $ad_id,
                'content_available'=>1,
                'sound' => 'default'
            ];

        //send notifications for android device
        if($user->mobile_id == 0){
            $body = [
                'data'=> $data ,
                //'notification' => $data,
                'to' => $user->fcm_token,
                //'registration_ids' => $tokens
                "priority" => "high",
                "delay_while_idle" => false
            ];
        }

        //send notifications for ios device
        if($user->mobile_id == 1){
            $body = [
                'data'=> $data ,
                'notification' => $data,
                'to' => $user->fcm_token
                //'registration_ids' => $tokens
            ];
        }

        if($user->fcm_token != null){
            $client = new Client(['headers' => $header]);
            $res = $client->post('https://fcm.googleapis.com/fcm/send',[
                'body' => json_encode($body),
            ]);

            SentNotification::create([
                'user_id' => $user->id,
                'title' => $title,
                'body'  => $content,
                'ad_id' => $ad_id?? null
            ]);
        }
    }

    public static function sendMessageNotification($user_id, $title, $content, $sender_id)
    {
        $user = User::find($user_id);

        $sender = User::find($sender_id);

        $sourceKey = self::$sourceKey;

        $header = [
            'Authorization'=> 'key=' . $sourceKey,
            'Content-Type' => 'application/json'
        ];
        $data = [
                'title' => $title,
                'body'  => $content,
                'sender_id' => $sender->id,
                'image'=>$sender->image_path,
                'content_available'=>1,
                'sound' => 'default'
            ];

        //send notifications for android device
        if($user->mobile_id == 0){
            $body = [
                'data'=> $data ,
                //'notification' => $data,
                'to' => $user->fcm_token,
                //'registration_ids' => $tokens
                "priority" => "high",
                "delay_while_idle" => false
            ];
        }

        //send notifications for ios device
        if($user->mobile_id == 1){
            $body = [
                'data'=> $data ,
                'notification' => $data,
                'to' => $user->fcm_token
                //'registration_ids' => $tokens
            ];
        }

        if($user->fcm_token != null){
            $client = new Client(['headers' => $header]);
            $res = $client->post('https://fcm.googleapis.com/fcm/send',[
                'body' => json_encode($body),
            ]);

            // SentNotification::create([
            //     'user_id' => $user->id,
            //     'title' => $title,
            //     'body'  => $content
            // ]);
        }
    }
}
