<?php

namespace App\Exports;

// use App\Models\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MerchantsExport implements  FromCollection , WithHeadings
{
    // $data = null ;

    public function __construct($data)
    {
        $this->data = $data;

    }

    public function headings():array {
        // $data = [];
        // foreach ($this->log_columns_names as $log_columns_name){
        //     array_push($data, $log_columns_name->name);
        // }
        return [
            'اسم التاجر',
            'الايميل',
            'كود الدولة',
            'التليفون',
            'العنوان',
            'رقم السجل التجاري',
            'متوسط التقييمات',
            'عدد التقييمات',
            'عدد متابعين التاجر ',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect( $this->data );

    } // end of collection

} // end of MerchantsExport
