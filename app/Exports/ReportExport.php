<?php

namespace App\Exports;

// use App\Models\Advertisement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportExport implements  FromCollection , WithHeadings
{
    // $data = null ;

    public function __construct($data)
    {
        $this->data = $data;
        
    }

    public function headings():array {
        // $data = [];
        // foreach ($this->log_columns_names as $log_columns_name){
        //     array_push($data, $log_columns_name->name);
        // }
        return [
            'عدد الاعلانات المفعلة', 
            'عدد الاعلانات المنتهيه', 
            'عددالاعلانات المميزة',
            'عدد الاعلانات الاكثر شهره', 
            'عدد المستخدمين', 
            'عدد التجار المعتمدين',
            'إجمالي أرباح اشتراكات المستخدمين',
            'إجمالي أرباح الاعلانات', 
            'إجمالي عدد الكوبونات المستخدمة', 
            'إجمالي قيمة الخصم من الكوبونات', 
            'إجمالي الأرباح النهائية',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect( $this->data );

    } // end of collection

} // end of ReportExport
