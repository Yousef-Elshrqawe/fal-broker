<?php

namespace App\Console\Commands;

use App\Http\Controllers\Dashboard\NotificationController;
use App\Models\Advertisement;
use App\Models\Desire;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendNotificationsOnDesireArrive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'desire:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notification to the user when finding advertisement match his desire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $desires = Desire::with('attributes')->get();
        foreach($desires as $desire){
            if($desire->subcat_id){
                $advertisements = Advertisement::with('attributes')
                ->where(['subcat_id'=>$desire->subcat_id ,'finished'=>0])
                ->whereBetween('price' , [$desire->from_price , $desire->to_price?? random_int(9999999999,99999999999)])->get();
            }else{
                $advertisements = Advertisement::with('attributes')
                ->where(['finished'=>0])
                ->whereBetween('price' , [$desire->from_price , $desire->to_price?? random_int(9999999999,99999999999)])->get();
            }
            
            if($desire->type){
                $advertisements = $advertisements->where('type',$desire->type);
            }

            if($desire->country_id){
                $advertisements = $advertisements->where('country_id',$desire->country_id);
            }
            if($desire->govern_id){
                $advertisements = $advertisements->where('govern_id',$desire->govern_id);
            }
            if($desire->district_id){
                $advertisements = $advertisements->where('district_id',$desire->district_id);
            }

            //extract desire attributes,subattributes ids and values
            $attrsIds = [];
            $subattrsIds = [];
            $values = [];

            if($desire->attributes){
                if(sizeof($desire->attributes)>0){
                    foreach($desire->attributes as $attribute){
                        $attrsIds[] = $attribute->attr_id;
                        if($attribute->aatr_type == 0){
                            $subattrsIds[] = $attribute->subattr_id;
                            $values[] = -1; 
                        }
                        elseif($attribute->attr_type == 1 || $attribute->attr_type == 2){
                            $subattrsIds[] = 0;
                            $values[] = $attribute->value;
                        }
                    }
                }else{
                    //sent notifications to user with each ad id
                    if(sizeof($advertisements)>0){
                        foreach($advertisements as $advertisement){
                            if($advertisement->user_id != $desire->user_id){
                                NotificationController::sendSingleNotification($desire->user_id,
                                __('user.desire_matching_title'), __('user.desire_matching_body'), $advertisement->id);
                            }
                        }
                    }
                }
            }

            //loop on ads attributes with desire attributes to get matched ads
            $selectedAds = [];
            $matching_points = 0;
            foreach($advertisements as $advertisement){
                $matching_points = 0;
                foreach($advertisement->attributes as $attribute){
                    for($i=0; $i<count($attrsIds); $i++){
                        if($values[$i] == -1){
                            if($attribute->attr_id==$attrsIds[$i] && $attribute->subattr_id==$subattrsIds[$i]){
                                $matching_points++;
                            }
                        }else{
                            if($attribute->attr_id==$attrsIds[$i] && $attribute->value==$values[$i]){
                                $matching_points++;
                            }
                        }
                    }
                }
                if($matching_points == count($values)){//equality here means that ad exactly match
                    $selectedAds[] = $advertisement;
                }
            }

            //sent notifications to user with each ad id
            if(sizeof($selectedAds)>0){
                foreach($selectedAds as $selectedAd){
                    if($advertisement->user_id != $desire->user_id){
                        NotificationController::sendSingleNotification($desire->user_id,
                        __('user.desire_matching_title'), __('user.desire_matching_body'), $selectedAd->id);
                    }
                }
            } 
        }
    }
}
