<?php

namespace App\Console\Commands;

use App\Models\Advertisement;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireAdvertisement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:advertisement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire advertisement after end date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $advertisements = Advertisement::where('finished' , 0)->get();
        foreach($advertisements as $advertisement){
            if($advertisement->end_date <= Carbon::now()){
                $advertisement->finished = 1;
                $advertisement->save();
            }
        }
    }
}
