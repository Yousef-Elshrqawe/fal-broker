<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Advertisement;
use App\Models\AdvertisementAttribute;
use App\Models\AdvertisementImages;
use App\Models\AdvertisementType;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Governorate;
use App\Models\Subcategory;
use App\Models\User;
use App\Models\UserRate;
use App\Models\Attribute;
use App\Models\Fund;
use App\Models\SpecialAdvertisement;
use App\Models\Subattribute;
use App\Models\UsersSubscription;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\backendTraits;
use SubattributeSeeder;

class SiteController extends Controller
{
    use backendTraits;
    public function index()
    {
        $data['whoUs'] = AboutUs::select('about_' . app()->getLocale() . ' as about')->first()->about;
        $data['contactUs'] = ContactUs::first();
        $data['cats'] = Category::select('name_' . app()->getLocale() . ' as name', 'image')->get();
        $data['govern'] = Governorate::all();
        $data['latest_ads'] = Advertisement::latest()->take(5)->with('images')->get();
        // dd(($data['latest_ads']->first())->images);
        $data['ad_images'] = AdvertisementImages::all();
        $data['ads'] = Advertisement::all();
        return view('site.index')->with($data);
    }
    public function about()
    {
        return view('site.about-us');
    }
    public function profile()
    {
        $data['profile'] = User::with('governorate')->with('subscription.subscriptionName')->with('usersRate')->with('rateUsers')->find(Auth::user()->id);
        $data['ratings'] = UserRate::where('rate_user_id', Auth::user()->id)->get();
        return view('site.profile')->with($data);
    }
    public function filter(Request $request)
    {
        $data['ad_types'] = AdvertisementType::all();
        $data['subcats'] = Subcategory::all();
        $data['attributes'] = Attribute::all();
        $data['ads'] = Advertisement::join('subcategories', 'subcategories.id', 'advertisements.subcat_id')->where('subcategories.cat_id', 1)->get(['*', 'advertisements.id as ad_id']);
        $data['subatts'] = Subattribute::where('attr_id', 5)->get();
        return view('site.filter')->with($data);
    }
    public function filter_data(Request $request)
    {
        $data['ad_types'] = AdvertisementType::all();
        $data['subcats'] = Subcategory::all();
        $data['attributes'] = Attribute::all();
        $data['subatts'] = Subattribute::where('attr_id', 5)->get();

        $query = Advertisement::join('subcategories', 'subcategories.id', 'advertisements.subcat_id')->where('subcategories.cat_id', 1)->where('type', '!=', NULL);
        if (isset($request->min_price) && isset($request->max_price)) {
            $query->whereBetween('price', [$request->min_price, $request->max_price]);
        }
        if ($request->subcat) {
            $query->where('subcat_id', '=', $request->input('subcat'));
        }

        if (isset($request->att[0]) || isset($request->att[1])) {
            $query->join('advertisement_attributes', 'advertisement_attributes.ad_id', '=', 'advertisements.id')
                ->where(function ($q) use ($request) {
                    return $q->whereIn('advertisement_attributes.value', $request->input('att'));
                });

            if (isset($request->att[2])) {
                $query->where(function ($q) use ($request) {
                    return $q->where('advertisement_attributes.subattr_id', '=', $request->input('att[2]'));
                });
            }
        }
        if (isset($request->att[2])) {
            $query->join('advertisement_attributes', 'advertisement_attributes.ad_id', '=', 'advertisements.id')
                ->where(function ($q) use ($request) {
                    return $q->where('advertisement_attributes.subattr_id', '=', $request->att[2]);
                });
        }
        if (isset($request->type)) {
            $query->where('type', '=', $request->type);
        }
        if (isset($request->payment_way)) {
            $query->where('payment_way', '=', $request->payment_way);
        }


        // $query->whereHas('roles', function ($q) use ($request) {
        //     return $q->whereIn('id', $request->input('roles'));
        // })
        //     ->whereHas('clients', function ($q) use ($request) {
        //         return $q->whereHas('industry_id', $request->input('industry'));
        //     });
        $data['ads'] = $query->get(['*', 'advertisements.id as ad_id']);
        return view('site.filter')->with($data);
    }
    public function addAdPage()
    {
        if (!Auth::user())
            return redirect('/ar/site/')->with('error', "Please login first");

        $data['ads'] = Advertisement::with('getType')->with('attributes')->with('images')->with('user')->get();
        $data['categories'] = Category::all();
        $data['types'] = AdvertisementType::all();
        $data['images'] = AdvertisementImages::all();
        $data['ad_attributes'] = AdvertisementAttribute::all();
        $data['attributes'] = Attribute::all();
        $data['sub_cat'] = Subcategory::all();
        $data['sub_att'] = Subattribute::all();
        $data['special'] = SpecialAdvertisement::all();
        $data['user'] = User::with('subscription')->where('id', Auth::user()->id)->first();

        return view('site.add-ad')->with($data);
    }
    public function adInfo($id)
    {
        $data['ad'] = Advertisement::with('images')->with('attributes')->find($id);

        return view('site.ad-info')->with($data);
    }
    public function addFundPage()
    {
        if (!Auth::user())
            return redirect('/ar/site/')->with('error', "Please login first");

        $data['ads'] = Advertisement::with('getType')->with('attributes')->with('images')->with('user')->get();
        $data['categories'] = Category::all();
        $data['types'] = AdvertisementType::all();
        $data['images'] = AdvertisementImages::all();
        $data['ad_attributes'] = AdvertisementAttribute::all();
        $data['attributes'] = Attribute::all();
        $data['sub_cat'] = Subcategory::all();
        $data['sub_att'] = Subattribute::all();
        return view('site.funding-request')->with($data);
    }
    public function traders()
    {
        $data['traders'] = User::whereHas('advertisements')->where('type', 1)->get();
        return view('site.traders')->with($data);
    }
    public function traderRatings($id)
    {
        $rating = UserRate::where('user_id', $id)->get();
        return $rating;
    }
    public function getWishlist()
    {
        $data['user'] = User::with('wishlist')->where('id', auth()->user()->id)->first();
        $data['adverts'] = Advertisement::with('attributes')->get();
        $data['subattrs'] = Subattribute::all();
        return view('site.wishlist')->with($data);
    }
    public function addFund(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'birth_date'  => 'required',
                'sector'  => 'required',
                'fund_type' => 'required',
                'monthly_income' => 'required',
                'salary_type' => 'required',
                'monthly_commit' => 'required',
                'financing_periad' => 'required',
            ],
            []
        );

        $data = $request->all();
        $fund_request = Fund::create($data);

        return redirect()->back();
    }

    public function addAd(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'cat'  => 'required',
                'ad_type'  => 'required',
                'type' => 'required',
                'ad_images' => 'array',
                'ad_details' => 'array',
                'attr_type' => 'required',
                'price' => 'required',
                'payment_method' => 'required',
                'address' => 'required',
                'lat' => 'required',
                'lng' => 'required',
                'desc' => 'required',
                'special_ad_id' => 'required',
                'duration' => 'required',
                'bank' => 'required',
                'attr_id' => 'required|array',
            ],
            []
        );
 $pay = paypage::sendPaymentCode('all')
            ->sendTransaction('sale')
            ->sendCart(10, 1000, 'test')
            ->sendCustomerDetails('Name', 'email@email.com', '0501111111', 'test', 'Riyadh', 'Riyadh', 'SA', '1234', '10.0.0.10')
            ->sendShippingDetails('Name', 'email@email.com', '0501111111', 'test', 'Riyadh', 'Riyadh', 'SA', '1234', '10.0.0.10')
            ->sendURLs('/', '')
            ->sendLanguage('en')
            ->create_pay_page();
        return $pay;
        $data = $request->except('ad_images', 'ad_details', 'attr_type', 'attr_id', 'duration', 'bank');
        // dd($data);


        $advertisement = Advertisement::create($data);
        $days_to_add = SpecialAdvertisement::where('id', $advertisement->special_ad_id)->first();
        $advertisement->user_id = Auth::user()->id;
        $advertisement->special_ad_start = Carbon::now();
        $advertisement->start_date = Carbon::now();
        $advertisement->end_date = Carbon::now()->addDay($days_to_add->expiration + 30);
        $advertisement->save();
        if ($request->hasFile('ad_images')) {
            foreach ($request->ad_images as $image) {
                // $name = $image->getClientOriginalName();
                $image_name = $this->upploadImage($image, 'site/assets/images/');
                // sleep(1); //to delay image saving beacuse the two images have the same name in ordinary case
                AdvertisementImages::create([
                    'ad_id'     => $advertisement->id,
                    'image'     => $image_name,
                ]);
            }
        }
        if (sizeof($request->ad_details) > 0) {
            foreach ($request->ad_details as $index => $value) {
                $subattribute = Subattribute::find($value);
                if ($subattribute) {
                    AdvertisementAttribute::create([
                        'ad_id'         => $advertisement->id,
                        'attr_id'       => $request->attr_id[$index],
                        'subattr_id'    => null,
                        'attr_type'     => $request->attr_type[$index],
                    ]);
                }
            }
        }

        $user_sub = UsersSubscription::where('user_id', Auth::user()->id)->first();
        if ($user_sub->remainder_ads > 0) {
            $user_sub->remainder_ads -= 1;
            $user_sub->save();
            $advertisement->is_subscribed = 1;
            $advertisement->save();
        }

        return redirect()->back();
    }
}
