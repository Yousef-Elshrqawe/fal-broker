<?php

use App\Http\Controllers\Dashboard\NotificationController;
use App\Http\Controllers\Site\SiteController;
use App\Http\Controllers\Site\UserController;
use App\Models\Advertisement;
use App\Models\Attribute;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Chat;
use App\Models\Desire;
use App\Models\Order;
use App\Models\Subattribute;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function () {

  //Site Routes
  Route::group(['prefix' => '/', 'namespace' => 'site', 'middleware' => ['web']], function () {

    // Route::get('/', [SiteController::class, 'index']);
    // Route::post('/login-user', [UserController::class, 'login'])->name('login-user');
    // Route::post('/forget-pw', [UserController::class, 'forgetPassword'])->name('forget-pw');
    // Route::post('/register', [UserController::class, 'register'])->name('register');
    // Route::get('/logout-user', [UserController::class, 'logout'])->name('logout-user');
    // Route::get('/about', [SiteController::class, 'about'])->name('about');
    // Route::get('/profile/{id}', [SiteController::class, 'profile'])->name('profile');
    // Route::post('/change_password', [UserController::class, 'changePassword'])->name('changePassword');
    // Route::post('/update_data', [UserController::class, 'updateData'])->name('updateData');
    // Route::post('/addad', [SiteController::class, 'addAd'])->name('addAd');
    // Route::post('/add-fund', [SiteController::class, 'addFund'])->name('addFund');
    // Route::get('/add_ad', [SiteController::class, 'addAdPage'])->name('addAdPage');
    // Route::get('/fund', [SiteController::class, 'addFundPage'])->name('addFundPage');
    // Route::get('/ad_info/{id}', [SiteController::class, 'adInfo'])->name('adInfo');
    // Route::get('/filter', [SiteController::class, 'filter'])->name('filter');
    // Route::post('/filter_data', [SiteController::class, 'filter_data'])->name('filter_data');
    // Route::get('/traders', [SiteController::class, 'traders'])->name('traders');
    // Route::get('/trader_rating/{id}', [SiteController::class, 'traderRatings'])->name('trader_rating');
    // Route::get('/get_name/{id}', [SiteController::class, 'getName'])->name('get_name');
    // Route::get('/wishlist', [SiteController::class, 'getWishlist'])->name('wishlist');
    // Route::get('/delete_wishlist/{id}', [SiteController::class, 'deleteWish'])->name('delete_wishlist');
    // Route::get('/getAttributes/{id}', [SiteController::class, 'getAttributes'])->name('getAttributes');
    // Route::get('/getAdvAtts/{attr_id}/{ad_id}', [SiteController::class, 'getAdvAtts'])->name('getAdvAtts');
    // Route::get('/getAdvAttsForWishes/{attr_id}/{desire_id}', [SiteController::class, 'getAdvAttsForWishes'])->name('getAdvAttsForWishes');
    // Route::get('/getSubAtts/{id}', [SiteController::class, 'getSubAtts'])->name('getSubAtts');
    // Route::get('/getSubcats/{id}', [SiteController::class, 'getSubcats'])->name('getSubcats');
    // Route::post('/addwish', [SiteController::class, 'addDesire'])->name('addwish');
    // Route::post('/addcontactus', [SiteController::class, 'sendContactUs'])->name('addcontactus');
    // Route::post('/add_rating', [SiteController::class, 'addRating'])->name('add_rating');
    // Route::get('/cat/{id}', [SiteController::class, 'categoryAds'])->name('cat');
    // Route::get('/subcat/{id}', [SiteController::class, 'subcategoryAds'])->name('subcat');
    // Route::get('/ads/{id}', [SiteController::class, 'ads'])->name('ads');
    // Route::get('/favorite', [SiteController::class, 'getFavorites'])->name('favorite');
    // Route::get('/wish_ad/{ad_id}/{user_id}', [SiteController::class, 'wish_ad'])->name('wish_ad');
    // Route::get('/unwish_ad/{ad_id}/{user_id}', [SiteController::class, 'unwish_ad'])->name('unwish_ad');



    // Route::get('/ads', function () {
    //   return view('site.ads');
    // })->name('ads');
    // Route::get('/favorite', function () {
    //   return view('site.favorite');
    // })->name('favorite');

    //  Route::get('/privacy', function () {
    //   return view('site.privacy');
    // })->name('privacy');
    // Route::get('/notifications', function () {
    //   return view('site.notifications');
    // })->name('notifications');
    // Route::get('/terms-and-conditions', function () {
    //   return view('site.terms-and-conditions');
    // })->name('terms-and-conditions');
    // Route::get('/about-us', function () {
    //   return view('site.about-us');
    // })->name('about-us');


    Route::fallback(function () {
        return response()->json([
            'result' => false,
            'errNum' => 404,
            'message' => 'Invalid Route'
        ]);
    });

    // Route::get('/ad_info', function () {
    //   return view('site.ad-info');
    // })->name('pagee');
    // 
  });
});
