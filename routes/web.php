<?php

use App\Http\Controllers\Dashboard\NotificationController;
use App\Models\Advertisement;
use App\Models\Attribute;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Chat;
use App\Models\Desire;
use App\Models\Order;
use App\Models\Subattribute;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Login to Dashboard
// Route::get('/', 'Dashboard\AuthController@index')->name('home');


Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function () {

    //IF Admin Not Auth Return to login
    // Route::get('/dashboard', function () {
    //     return view('dashboard.index');
    // })->name('pagee')->middleware('adminauth:admin');

    // Website routes
    // Route::group(['namespace' => 'Site'], function () {
    //     Route::get('/', 'SiteController@index');
    // });



    // Clear application cache:
    Route::get('/clear-cache', function() {
        \Artisan::call('optimize:clear');
            // \Artisan::call('cache:clear');
        return 'Application cache has been cleared';
    });

    //site route
    //Route::get('/', 'Site\SiteController@index');
    Route::get('/',function(){
        return '404 Not Found!';
    });

    // Dashboard routes
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard'], function () {
        Route::get('/', 'DashboardController@index')->name('pagee')->middleware('adminauth:admin');
        //Login process
        Route::get('/login', 'AuthController@login')->name('admin.login');
        Route::post('/do-login', 'AuthController@doLogin')->name('admin.doLogin');

        Route::name('admin.')->middleware('adminauth:admin')->group(function () {
            //Logout proccess
            Route::get('/logout', 'AuthController@logout')->name('logout');

            //Admin Routes
            Route::resource('users', 'AdminController')->except(['show']);

            // Profile
            Route::get('profile', 'ProfileController@profile')->name('profiles');
            Route::get('profile/edit', 'ProfileController@editProfile')->name('profiles.edit');
            Route::put('profile/update', 'ProfileController@updateProfile')->name('profiles.update');
            Route::get('profile/change_password', 'ProfileController@changePassword')->name('profiles.change_password');
            Route::put('profile/change_password', 'ProfileController@change_password_method')->name('profiles.change_password_method');

            //Customers routes
            Route::resource('customer', 'CustomersController');
            Route::get('customer-excel', 'CustomersController@customersPrintExcel')->name('customer.index.excel');


            //Merchant routes
            Route::resource('merchant', 'MerchantController');
            // Route::get('merchant/active/{id}', 'MerchantController@changeStatus')->name('merchant.active');addComments
            Route::post('merchant/addComments', 'MerchantController@addComments')->name('merchant.addComments');
            Route::get('merchant/active/{id}', 'MerchantController@changeStatus')->name('merchant.active');
            Route::get('merchant/dependence/{id}', 'MerchantController@changeDependence')->name('merchant.dependence');
            Route::get('merchant/changeViewRate/{id}', 'MerchantController@changeViewRate')->name('merchant.changeViewRate');
            Route::get('merchant-excel', 'MerchantController@merchantsPrintExcel')->name('merchant.index.excel');


            // Country routes
            Route::resource('countries', CountryController::class);

            // Governorates routes
            Route::resource('countries/{country}/governorates', GovernorateController::class);

            // districts routes
            Route::resource('countries/{country}/governorates/{governorate}/districts', DistrictController::class);

            // Categories routes
            Route::resource('categories', CategoryController::class);

            //Subcategories routes
            Route::resource('categories/{cat}/subcategories', SubcategoryController::class);

            // Attributes routes
            Route::resource('categories/{cat}/subcategories/{subcat}/attributes', AttributeController::class);
            Route::post('categories/{cat}/subcategories/{subcat}/attributes/save', 'AttributeController@createSub')->name('subattributes.save');
            Route::post('categories/{cat}/subcategories/{subcat}/attributes/delete/{id}', 'AttributeController@deleteSub')->name('subattributes.delete');

            //Advertisements
            Route::resource('advertisements', AdvertisementController::class);
            Route::get('fees-expiration' , 'AdvertisementController@getFeesExpiration')->name('fees-expiration.index');
            Route::get('advertisements/popular/{id}', 'AdvertisementController@changeStatusPopular')->name('advertisements.popular');

            Route::post('advertisements/fees-expiration' , 'AdvertisementController@updateFeesExpiration')->name('fees-expiration.update');
            Route::resource('advertisement-types' , AdvertisementTypeController::class);

            //special advertisements
            Route::resource('special-advertisements' , SpecialAdvertisementController::class);

            //Subscriptions
            Route::resource('subscriptions', SubscriptionController::class);

            //Funds
            Route::get('financing_request', 'FundController@financingRequest')->name('financing_request');
            Route::resource('funds', FundController::class);


            Route::get('report', 'SettingController@report')->name('report.index');
            Route::get('report-excel', 'SettingController@reportPrintExcel')->name('report.index.excel');

            // Setting
            Route::resource('slider', 'SliderController');
            Route::post('ajaxLoad', 'SliderController@ajaxLoad')->name('ajaxLoad');

            Route::get('feedbacks','SettingController@feedback')->name('setting.feedback');
            Route::get('form_contact_us','SettingController@formContactUs')->name('setting.form_contact_us');

            Route::get('setting/banner', 'SettingController@banner')->name('setting.banner');
            Route::put('setting/banner/{id}/update', 'SettingController@editBanner')->name('setting.banner.update');

            Route::get('setting/banner/{id}/delete', 'SettingController@deleteBanner')->name('setting.banner.delete');

            Route::get('setting/contact-us', 'SettingController@contactUs')->name('setting.contact-us');
            Route::put('setting/contact-us/{id}/update', 'SettingController@editContactUs')->name('setting.contact-us.update');

            Route::get('setting/terms', 'SettingController@terms')->name('setting.terms');
            Route::put('setting/terms/{id}/update', 'SettingController@editTerms')->name('setting.terms.update');

            Route::get('setting/about', 'SettingController@about')->name('setting.about');
            Route::put('setting/about/{id}/update', 'SettingController@editAbout')->name('setting.about.update');

            Route::get('setting/delivery-tax', 'SettingController@deliveryCostAndTax')->name('setting.delivery-tax');
            Route::post('setting/delivery-tax', 'SettingController@updateDeliveryCostAndTax')->name('setting.delivery-tax.update');

            //Notifications
            Route::get('notifications','NotificationController@index')->name('notifications.index');
            Route::post('notifications','NotificationController@send')->name('notifications.send');

            //coupons
            Route::resource('coupons','CouponController');
        });
    });
});
Route::get('test', function () {

});
