<?php

use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['api','App\Http\Middleware\ChangeLanguage'], 'namespace' => 'API'], function () {

    // Common
    // User
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::post('forgetpassword', 'UserController@forgetpassword');
    //Route::post('activcode', 'UserController@activcode');
    Route::post('rechangepass', 'UserController@rechangepass');

    //check phone if exist
    Route::post('check-phone','UserController@checkPhone');

    //check email if exist checkEmail
    Route::post('check-email','UserController@checkEmail');


    //OTP
    Route::post('send-otp', 'UserController@sendOTP');

    // Category
    Route::get('/categories', 'UserController@getCats');
    Route::post('/subcategories', 'UserController@getSubcats');

    //special ad
    Route::get('special-ads','AdvertisementController@getSpecialAds');
    // Route::get('special-ads/{cat_id}','AdvertisementController@getSpecialAds');

    //Merchants
    Route::post('merchants','MerchantController@getMerchants');
    Route::post('advertisements-by-merchant' , 'MerchantController@getAdsByMerchant');
    Route::post('merchant-details' , 'UserController@userDetails');

    //Advertisements canceled
    Route::post('all-advertisements','AdvertisementController@getAds');
    Route::post('advertisement-latest','AdvertisementController@getAdsLatest');
    Route::post('advertisement-popular','AdvertisementController@getAdsPopular');
    Route::get('Count-Images-Advertisement','AdvertisementController@getCountImageAdvertisement');
    Route::get('advertisement-types','AdvertisementController@getAdvertisementTypes');
    Route::post('advertisements-by-subcat' , 'AdvertisementController@getAdsBySubcat');
    Route::post('advertisements-by-cat' , 'AdvertisementController@getAdsByCat');
    Route::post('advertisement-by-id' , 'AdvertisementController@getAdsById');
    Route::post('filter-ads' , 'AdvertisementController@filterAds');
    Route::get('advertisement-special','AdvertisementController@getSpecialAdvertisements');


    //Subscriptions
    Route::get('subscriptions','UserController@getSubscriptions');

    // bank_accounts
    Route::get('/bank_accounts', 'UserController@getBankAccounts');

    // Country
    Route::get('/countries', 'UserController@getCountries');

    // Governorates
    Route::get('/governorates', 'UserController@getGovernorates');

    // Districts
    Route::post('/districts', 'UserController@getDistricts');

    // reasons_for_refusal
    Route::get('/reasons_for_refusal', 'AdvertisementController@ReasonsForRefusal');

    //App Setting
    Route::get('sliders', 'SettingController@getSliders');
    Route::get('setting/terms','SettingController@getTerms');
    Route::get('setting/about-us','SettingController@getAboutUs');
    Route::get('setting/contact-us', 'SettingController@getContactUs');
    //Route::get('setting/privacy-policy','SettingController@getPrivacy');

     //check coupon
     Route::post('check-coupon' , 'SettingController@checkCoupon');

    //search advertisements
    Route::post('search','AdvertisementController@search');

    Route::group(['middleware' => ['auth.guard:api'],], function () {
        //logout
        Route::get('logout', 'UserController@logout');

        //profile
        Route::get('profile', 'UserController@getProfile');

        // Update profile
        Route::post('update-profile', 'UserController@updateProfile');
        //store user location
        Route::post('update-location', 'UserController@updateLocation');

        //changePassword
        Route::post('change_password', 'UserController@changePassword');

        //delete user
        Route::get('user/delete', 'UserController@deleteUser');
        // Route::delete('user/delete', 'UserController@deleteUser');


        //activate and deactivate user
        Route::post('activate-user','UserController@activateUser');
        Route::post('deactivate-user','UserController@deactivateUser');

        //follow  (add, delete from follow/unfollow )
        Route::post('follow ' , 'UserController@follow');

        //wishlist (add, delete from wishlist)
        Route::post('wishlist' , 'UserController@wishlist');

        //liked advertisements
        Route::post('liked-advertisements' , 'UserController@getLikedAdvertisements');

        // rate User
        Route::post('/rate_user', 'UserController@rateUser');
        Route::post('/reviews_user_by_id', 'UserController@ReviewUser');

        //Funds
        Route::post('add-fund-request' , 'FundController@addFundRequest');
        Route::post('my-funds' , 'FundController@getMyFunds');

        //desires
        Route::post('add-desire','DesireController@addDesire');
        Route::post('delete-desire','DesireController@deleteDesire');
        Route::post('my-desires','DesireController@myDesires');

        //Notifications
        Route::post('update-fcm-token' , 'UserController@updateFcmToken');
        Route::post('notifications', 'UserController@getNotifications');

        //update subscriptions
        Route::post('update-subscription','UserController@updateSubscription');

        //Chats
        Route::post('send-message' , 'ChatController@sendMessage');
        Route::post('my-chats' , 'ChatController@myChats');
        Route::get('get-messages-by-chat' , 'ChatController@getMessagesByChat');
        Route::post('get-messages-by-sender-and-receiver' , 'ChatController@getMessagesBySenderAndReceiver');

        //advertisements
        Route::post('create-advertisement','AdvertisementController@createAdvertisement');
        Route::post('update-advertisement','AdvertisementController@updateAdvertisement');
        Route::post('cancel-advertisement','AdvertisementController@cancelAdvertisement');
        Route::post('delete-advertisement','AdvertisementController@deleteAdvertisement');
        Route::post('my-advertisements','AdvertisementController@myAdvertisements');

        //feedback
        Route::post('feedback','UserController@feedback');

    });
});
