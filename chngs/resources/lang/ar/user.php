<?php

return [

    //Login Page
    'access'            => 'قم بالتسجيل للدخول الي لوحه التحكم',
    'login'             => 'تسجيل الدخول',
    'username'          => 'اسم المستخدم',
    'password'          => 'كلمه المرور',
    'rep_password'      => 'تاكيد كلمه المرور',
    'forg_pass'         => 'نسيت كلمه المرور؟',
    'logout'            => 'تسجيل الخروج',
    'error_credentials' => 'جرب باستخدام البريد الإلكتروني وكلمة المرور الصحيحين !!',
    'succes_login'      => 'تم تسجيل الدخول بنجاح',
    'registSuccess'     => 'تم التسجيل بنجاح',
    'false_info'        => 'معلومات خاطئه يرجي المحاوله من جديد',
    'new_pass'          => 'تم تغير كلمه المرور بنجاح',
    'usernotexist'      => 'مستخدم غير موجود',
    'rateSuccess'       => 'تم التقيم بنجاح',
    'userlocationexist' => 'موقع مزود الخدمه موجود من قبل',
    'reqLat'            => 'خطوط الطول مطلوبه',
    'reqLong'           => 'خطوط العرض مطلوبه',
    'locationSuccess'   => 'تم اضافه الموقع بنجاح',
    'latitude'           => 'أدخل بيانات موقعك',
    'longitude'         => 'أدخل بيانات موقعك',

    //Common
    'incorrect_info' => 'معلومات غير صحيحة',
    'no_data_found'  => 'للأسف لا يوجد اي سجلات',
    'no_records'     => 'للاسف لا يوجد اي سجلات',
    'menu'           => 'القائمة',
    'search'         => 'بحث',
    'operations'     => 'العمليات',
    'edit'           => 'تعديل',
    'delete'         => 'حذف',
    'add'            => 'اضافه',
    'show'           => 'عرض',
    'permission'     => 'الصلاحيات',
    'confirm_delete' => 'تأكيد الحذف',
    'yes'            => 'نعم',
    'no'             => 'لا',
    'submit'         => 'تاكيد',
    'name'           => 'الاسم',
    'stauts'         => 'الحاله',
    'Not_dependence' => 'الغاءاعتمادالتاجر',
    'dependence'     => 'اعتمادالتاجر',
    'inactive'       => 'الغاء تفعيل التاجر',
    'active'         => 'تفعيل التاجر',
    'commercial_register' => 'صورة السجل التجارى',
    'user_type' => 'المستهدف',

    'reqEmail'           => 'البريد الالكتروني مطلوب',
    'unique_username'    => 'اسم المستخدم مقرر يرجي استخدام اسم اخر',
    'unique_email'       => 'هذا البريد مستخدم من قبل',
    'req_password'       => 'كلمت المرور مطلوبه',
    'password_confirmed' => 'كلمت المرور غير متطابقه',

    'reqCountry'   => 'الدوله مطلوبه',
    'reqAddress'   => 'العنوان مطلوب',
    'reqImageID'   => 'صوره اثبات الشخصيه مطلوبه',
    'reqCats'      => 'اختيار القسم ملطوب',
    'reqSubs'      => 'يجب اختيار نوع الاشتراك',

    'reqPhone'      => 'الهاتف المحمول مطلوب',
    'uniquePhone'   => 'الهاتف المحمول مسجل لدينا بالفعل',

    'wrang_phone'   => 'رقم الهاتف غير صحيح',
    'sent_phone'    => 'تم ارسال الرساله بنجاح',


    'create' => 'اضافه',
    'update' => 'تعديل',
    'read'   => 'عرض',
    'users'  => 'المشرفين',

    'added_successfully'   => 'تم إضافه البيانات بنجاح',
    'updated_successfully' => 'تم تعديل البيانات بنجاح',
    'deleted_successfully' => 'تم حذف البيانات بنجاح',


    'special_ads'			=>'الاعلانات المميزة',
    'special_advertisements'=>'الاعلانات المميزة',

    //NaveBar
    'main' => 'الرئيسيه',
    'user' => 'المشرفين',

    //Admins Page
    'username'           => 'اسم المستخدم',
    'image'              => 'الصوره',
    'email'              => 'البريد الالكتروني',
    'add_new_user'       => 'اضافه مشرف جديد',
    'edit_user'          => 'تعديل المشرف',
    'first_name'         => 'الاسم الاول',
    'last_name'          => 'الاسم الاخير',
    'req_first_name'     => 'الاسم الاول مطلوب',
    'req_last_name'      => 'الاسم الاخير مطلوب',
    'req_username'       => 'اسم المستخدم مطلوب',
    'unique_username'    => 'اسم المستخدم مقرر يرجي استخدام اسم اخر',
    'unique_email'       => 'هذا البريد مستخدم من قبل',
    'req_password'       => 'كلمت المرور مطلوبه',
    'password_confirmed' => 'كلمت المرور غير متطابقه',

    //Profile
    'profile'                => 'الملف الشخصي',
    'editProfile'            => 'تعديل الملف الشخصي',
    'setting'                => 'الاعدادات',
    'changPass'              => 'تغير كلمه المرور',
    'oldPass'                => 'كلمه المرور القديمه',
    'newPass'                => 'كلمه المرور الجديده',
    'password_confirmation'  => 'تاكيد كلمه المرور الجديده',

    // Setting
    'technical-support'   => 'الدعم الفني',
    'instegram'           => 'انتسجرام',
    'facebook'            => 'فيسبوك',
    'paying'              => 'وسائل الدفع',
    'terms'               => 'الشروط و الاحكام',
    'term_ar'             => 'الشروط بالعربيه',
    'term_en'             => 'الشروط بالانجليزيه',
    'slider'              => 'سلايدر',
    'sliders'             => 'سلايدر',
    'contactUs'           => 'تواصل معانا',
    'aboutAs'             => 'من نحن',
    'about'               => 'من نحن',


    // Categories

    'cats'         => 'الاقسام',
    'cat_name'     => 'اسم القسم',
    'subcats'      => 'الأقسام الفرعية',
    'addNewCat'    => 'اضف قسم جديد',
    'catAr'        => 'القسم بالعربيه',
    'catEn'        => 'القسم بالانجليزيه',
    'chooseCat'    => 'اختر القسم (قسم اساسي)',
    'reqCatAr'     => 'يرجي ادخال اسم القسم بالعربيه',
    'uniqueCatAr'  => 'اسم القسم بالعربيه مسجل مسبقا',
    'reqCatEn'     => 'يرجي ادخال اسم القسم بالانجليزيه',
    'uniqueCatEn'  => 'اسم القسم بالانجليزيه مسجل مسبقا',
    'reqImage'     => 'يرجي ادخال الصوره',
    'attrType'     =>'نوع الصفة',
    'textAttr'     =>'صفة نصية',
    'numericAttr'   =>'صفة رقمية',
    'multichoiceAttr'=>'صفة متعددة الاختيارات',
    'type_name'     => 'اسم متغير النوع',
    'type_name_ar'  => 'اسم متغير النوع بالعربية',
    'type_name_en'  => 'اسم متغير النوع بالانجليزية',
    'details_name'  => 'اسم متغير التفاصيل',
    'details_name_ar'  => 'اسم متغير التفاصيل بالعربية',
    'details_name_en'  => 'اسم متغير التفاصيل بالانجليزية',
    'icon'          => 'ايقونة',

    //coupons
    'coupons'       => 'كوبونات',
    'coupon_code'   => 'كود الكوبون',
    'discount_percent'=> 'نسبة الخصم',
    'count_of_use'  =>'عدد مرات الاستخدام',
    'times_for_user'=>'عدد المرات للمستخدم',
    'false_coupon'  =>'الكوبون غير صحيح',

    //feedback
    'feedback'      => 'تعليقات المستخدمين',
    'content'       => 'محتوى الرسالة',
    'user_name'     => 'اسم المستخدم',


    // Country

    'country'            => 'الولايات',
    'state'              => 'ولاية',
    'countryAr'          => 'الولاية بالعربيه',
    'countryEn'          => 'الولاية بالانجليزيه',
    'reqStatNameAr'      => 'الولاية بالعربيه مطلوبه',
    'reqStatNameEn'      => 'الولاية بالانجليزيه مطلوبه',
    'uniqueStatNameEn'   => 'الاسم بالانجليزيه مقرر',
    'uniqueStatNameAr'   => 'الاسم بالعربيه مقرر',
    'govern'             => 'المدن',
    'governAr'           => 'المدينة بالعربيه',
    'governEn'           => 'المدينة بالانجليزيه',
    'countryEn'          => 'الولاية بالانجليزيه',
    'reqGovernNameAr'    => 'المدينة بالعربيه مطلوبه',
    'reqGovernNameEn'    => 'المدينة بالانجليزيه مطلوبه',
    'uniqueGovernNameEn' => 'المدينة بالانجليزيه مقرر',
    'uniqueGovernNameAr' => 'المدينة بالعربيه مقرر',

    'districts'          =>'الأحياء',
    'district_ar'        =>'الحى بالعربية',
    'district_en'        =>'الحى بالانجليزية',

    // Providers
    'provider'   => 'مقدمين الخدمات',
    'job'        => 'المهنه',
    'phone'      => 'رقم الهاتف',
    'address'    => 'العنوان',
    'idImage'    => 'صوره بطاقه الهويه',
    'info'       => 'تفاصيل',

    // customers
    'customers'  => 'الزبائن',
    'location'   => 'الموقع',
    'lat'        => 'خط العرض',
    'long'       => 'خط الطول',

    'substra'    => 'اشتراك',
    'substras'   => 'الاشتراكات',
    'price'      => 'السعر',
    'subDesc'    => 'تفاصيل الاشتراك',
    'substraAr'  => 'الاشتراك بالعربيه',
    'substraEn'  => 'الاشتراك بالانجليزيه',
    'subsDescAr' => 'تفاصيل الاشتراك بالعربيه',
    'subsDescEn' => 'تفاصيل الاشتراك بالانجليزيه',
    'reqInputAr' => 'المدخل بالعربيه مطلوب',
    'reqInputEn' => 'المدخل بالانجليزيه مطلوب',
    'reqDescAr'  => 'الوصف بالعربيه مطلوب',
    'reqDescEn'  => 'الوصف بالانجليزيه مطلوب',
    'uniInputAr' => 'المدخل بالعربيه بالعربيه موجود من قبل',
    'uniInputEn' => 'المدخل بالانجليزيه موجود من قبل',
    'rate'       => 'التقيم',
    'reqPrice'   => 'السعر مطلوب',
    'website'    => 'الموقع الالكتروني',


    // Api
    'reqCustomer'  => 'العميل ملطوب',
    'reqProvider'  => 'مزود الخدمه مطلوب',
    'reqRate'      => 'التقيم ملطوب',
    'addedCart'    => 'تم إضافة المنتج إلى سلة التسوق',


    // Subscriptions
    'subscriptions'         =>'الاشتراكات',
    'ads_number'            =>'عدد الاعلانات',
    'subscription_name_ar'  =>'اسم الاشتراك بالعربية',
    'subscription_name_en'  =>'اسم الاشتراك بالانجليزية',
    'ad_fees'               =>'رسوم الاعلان الواحد',
    'ad_expiration'         =>' مدة الاعلان بالايام',
    'advertisement_fees_expiration'=>'مدة الاعلان والرسوم',
    'expiration'			=>'المدة',

    'merchant'          => 'التاجر',
    'merchants'         => 'التجار',
    'remainder_ads'     => 'الإعلانات المتبقية',
    'country_code'      => 'رمز الدولة',
    'dependence'        => 'الاعتماد',
    'count_rate'        => 'عدد التقييمات',
    'avg_rate'          => 'متوسط التقييمات',
    'commercial_registration_no' => 'رقم السجل التجاري',


     //advertisements
     'advertisement_types'   =>'أنواع الاعلانات',
     'advertisements'        =>'الاعلانات',
     'nameAr'                =>'الاسم بالعربية',
     'nameEn'                =>'الاسم بالانجليزية',
     'start_date'            =>'تاريخ البداية',
     'end_date'              =>'تاريخ الانتهاء',
     'type'                  =>'نوع الاعلان',
     'status'                =>'حالة الاعلان',
     'advertisement_details' =>'تفاصيل الاعلان',
     'current'               =>'حالى',
     'finished'              =>'منتهى',

    //verify to deelte
    'yes'  =>'نعم',
    'no'   =>'لا',
    'confirm_delete'=>'هل أنت متأكد من الحذف',

    //dashboard index
    'show'      => 'عرض',

    'subscription'=>'الاشتراكات',
    'admin' => 'المشرفين',

    //notifications
    'notifications'     =>'الاشعارات',
    'title'             =>'العنوان',
    'body'              =>'المحتوى',
    'all'               =>'ارسال الى الكل',
    'send'              =>'ارسال',
    'add_notify'        =>'اضافة اشعار',
    'desire_matching_title'=>'الأمنيات',
    'desire_matching_body' =>'تم العثور على اعلان موافق لأمنياتك',
    'chat_notify_title'    =>'رسالة من: ',
    'chat_notify_body'     =>'',
    'saveToDatabase'       =>'اظهرها فى اشعارات التطبيق',

    //sliders
    'slider_image_en'   =>'صورة السلايدر بالانجليزية',
    'slider_image_ar'   =>'صورة السلايدر بالعربية',
    'slider_link'       =>'رابط السلايدر',
    'select_category'   =>'اختر قسم رئيسى',
    'select_subcat'=>'اختر قسم فرعى',
    'subcat_name'       =>'اسم القسم الفرعى',

    //subscriptions
    'subscribe_status'  =>'حالة الاشتراك',
    'subscribe_status_valid'  =>'جارى',
    'subscribe_status_expired'  =>'منتهى',
    'subscribe_name'    =>'اسم الاشتراك',
    'subscribe_start_date'=>'تاريخ البدء',
    'subscribe_end_date'=>'تاريخ الانتهاء',
    'provider_subs'     =>'اشتراكات مقدمين الخدمة',

    //pharmacy, delivery
    'pharmacy'          =>'الصيدليات',
    'delivery'          =>'التوصيل',
    'customer'          =>'الزبائن',

    //products
    'products'          =>'المنتجات',
    'desc'              =>'الوصف',
    'productAr'         =>'اسم المنتج بالعربية',
    'productEn'         =>'اسم المنتج بالانجليزية',
    'descAr'            =>'الوصف بالعربية',
    'descEn'            =>'الوصف بالانجليزية',
    'addNewProduct'     =>'أضف منتج جديد',
    'product_name'      =>'اسم المنتج',
    'select_product'    =>'اختر منتج',

    //attributes
    'attributes'        =>'المواصفات',
    'subattr'           =>'المواصفات الفرعية',
    'attr_name'         =>'اسم الصفة',
    'attrAr'            => 'اسم الصفة بالعربية',
    'attrEn'            => 'اسم الصفة بالانجليزية',
    'subattrAr'         => 'اسم الصفة الفرعية بالعربية',
    'subattrEn'         => 'اسم الصفة الفرعية بالانجليزية',
    'value'             =>'القيمة',
    'attrIcon'          => 'أيقونة الصفة',

    //orders
    'order'             =>'الطلبات',
    'customerName'      =>'اسم العميل',
    'pharmacyName'      =>'الصيدلية',
    'pharmacyPhone'     =>'هاتف الصيدلية',
    'deliveryName'      =>'اسم الدليفرى',
    'deliveryPhone'     =>'هاتف الدليفرى',
    'plainOrder'        =>'طلب عادى',
    'fastOrder'         =>'طلب سريع',
    'subscribe'         =>'الاشترك',
    'paymentWay'        =>'طريقة الدفع',
    'deliveryCost'      =>'تكلفة التوصيل',
    'taxValue'          =>'قيمة الضريبة',
    'totalPrice'        =>'السعر الاجمالى',
    'productName'       =>'اسم المنتج',
    'quantity'          =>'الكمية',
    'date'              =>'التاريخ',
    'orderDetails'      =>'تفاصيل الطلب',
    'orderCart'         =>'محتويات الطلب',
    'orderDates'        =>'تواريخ توصيل الطلب',
    'oneMonth'          =>'شهر واحد',
    'threeMonth'        =>'ثلاثة أشهر',
    'currentOrder'      =>'جارى التنفيذ',
    'waitingOrder'      =>'فى انتظار الصيدلية',
    'finishedOrder'     =>'طلب منتهى',

    //about us
    'about_ar'          =>'من نحن بالعربية',
    'about_en'          =>'من نحن بالانجليزية',

    //delivery cost and tax
    'delivery_tax'      =>'مصاريف الشحن والضريبة',
    'one_month_cost'    =>'سعر التوصيل لباقة الشهر الواحد',
    'three_month_cost'  =>'سعر التوصيل لباقة الثلاثة أشهر',
    'tax'               =>'النسبة المئوية للضريبة',

    //report
    'report'            =>'التقرير',
    'total_users_subscriptions' =>'إجمالي أرباح اشتراكات المستخدمين',
    'total_ads_earnings'=>'إجمالي أرباح الاعلانات',
    'total_used_coupons'=>'إجمالي عدد الكوبونات المستخدمة',
    'total_coupons_discount'=>'إجمالي قيمة الخصم من الكوبونات',
    'final_total_earnings'=>'إجمالي الأرباح النهائية',
    'from'              =>'من',
    'to'                =>'الى',
];


?>
