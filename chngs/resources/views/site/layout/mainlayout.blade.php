<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('site.layout.partials.head')
</head>
    @if(Route::is(['login','register']))
        <body class="account-page">
    @endif
    @if(Route::is(['error-404','error-500']))
        <body class="error-page">
    @endif
    @if(!Route::is(['login','register','error-404','error-500']))
        @include('site.layout.partials.header')
    @endif
    @yield('content')
    @include('partials._session')
    @include('site.layout.partials.footer-scripts')
  </body>
</html>
