<footer id="site-footer">
  <div class="cols">
      <div class="container">
          <div class="row">
              <div class="col-6 col-md-3 p-4">
                  <a href="#"><i class="fa fa-arrow-up-long"></i></a>
                  <img src="/site/assets/images/Logo-f.png" alt="">
                  <ul class="list-group list-group-flush mt-3">
                      <li class="list-group-item bg-transparent"><a href="about-us.html">من نحن</a></li>
                      <li class="list-group-item bg-transparent"><a href="#">الفئات</a></li>
                      <li class="list-group-item bg-transparent"><a href="favorite.html">المفضلة</a></li>
                      <li class="list-group-item bg-transparent"><a href="wishlist.html">أمنياتي</a></li>
                      <li class="list-group-item bg-transparent"><a href="terms-and-conditions.html">الشروط والأحكام</a></li>
                      <li class="list-group-item bg-transparent"><a href="#">سياسة الخصوصية</a></li>
                  </ul>
              </div>
              <div class="col-6 col-md-3 p-4">
                  <h4 class="mb-4 mt-4">تواصل معنا</h4>
                  <ul class="list-group list-group-flush mt-3">
                      <li class="list-group-item bg-transparent">
                          <h6>عبر الهاتف</h6>
                          <a href="tel:96612346789" class="tel">+96612346789</a>
                      </li>
                      <li class="list-group-item bg-transparent">
                          <h6>عبر البريد الإلكتروني</h6>
                          <a href="mailto:info@khyark.com" class="email">info@khyark.com</a>
                      </li>
                  </ul>
              </div>
              <div class="col-md-6 p-4">
                  <a href="#">
                      <div class="bg-3">
                          <h3 class="text-white fw-bold">التجار المعتمدين</h3>
                      </div>
                  </a>
                  <h4 class="mb-4 mt-5">كن دائما على تواصل معنا</h4>
                  <form action="#" class="position-relative w-75">
                      <div class="mb-3">
                          <input type="email" class="form-control" placeholder="البريد الالكترونى">
                      </div>
                      <div class="mb-3">
                          <textarea class="form-control" rows="4" placeholder="رسالتك"></textarea>
                      </div>
                      <button type="submit" class="btn btn-contact"><i class="fa fa-arrow-left-long"></i></button>
                  </form>
              </div>
          </div>
      </div>
  </div>
  <div class="copy">
      <small>جميع الحقوق محفوظة <span>©</span> 2023</small>
  </div>
  @if(Auth::user())
  <div class="container">
      <div class="bottom-btn">
          <a href="/site/add_ad" class="btn btn1">انشاء اعلان</a>
          <a href="/site/fund" class="btn btn2">طلب تمويل</a>
      </div>
  </div>
  @endif
</footer>

<!-- JavaScript -->
<script src="/site/assets/js/jquery.min.js"></script>
<script src="/site/assets/js/wow.min.js"></script>
<script src="/site/assets/js/owl.carousel.min.js"></script>
<script src="/site/assets/js/bootstrap.bundle.min.js"></script>
<script src="/site/assets/js/custom.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>