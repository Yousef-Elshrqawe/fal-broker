<header id="site-header">
  <nav class="navbar navbar-expand-lg navbar-dark">
      <div class="container">
          <a class="navbar-brand" href="#"><img src="/site/assets/images/Logo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#siteNavbar" aria-controls="siteNavbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="siteNavbar">
              <ul class="navbar-nav">
                  <li class="nav-item">
                      <a class="nav-link" href="about-us">من نحن</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">الفئات</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="favorite">المفضلة</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="wishlist">أمنياتى</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">تواصل معنا</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="profile">الملف الشخصى</a>
                  </li>
                  <li class="nav-item lang">
                      <a class="nav-link" href="#">EN</a>
                  </li>
              </ul>
          </div>
      </div>
  </nav>
</header>