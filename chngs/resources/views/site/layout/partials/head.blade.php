<!doctype html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>خيارك</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800;900;1000&display=swap" rel="stylesheet">
    <link href="/site/assets/css/all.min.css" rel="stylesheet">
    <link href="/site/assets/css/animate.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site/assets/css/bootstrap.rtl.min.css" rel="stylesheet">
    <link href="/site/assets/css/style.css" rel="stylesheet">
</head>