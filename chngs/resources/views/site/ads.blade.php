<!doctype html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>خيارك</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800;900;1000&display=swap" rel="stylesheet">
    <link href="/site/assets/css/all.min.css" rel="stylesheet">
    <link href="/site/assets/css/animate.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site/assets/css/bootstrap.rtl.min.css" rel="stylesheet">
    <link href="/site/assets/css/style.css" rel="stylesheet">
</head>

<body>

<header id="site-header">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="/site/assets/images/Logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#siteNavbar" aria-controls="siteNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="siteNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="about-us.html">من نحن</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">الفئات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="favorite.html">المفضلة</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wishlist.html">أمنياتى</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">تواصل معنا</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="profile.html">الملف الشخصى</a>
                    </li>
                    <li class="nav-item lang">
                        <a class="nav-link" href="#">EN</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item">التجار المعتمدين</li>
    </ol>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="nav flex-column nav-pills me-3 bg-light p-4" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link bg-white rounded-3 p-4 mb-3 active" id="v-pills-currently-tab" data-bs-toggle="pill" data-bs-target="#v-pills-currently" type="button" role="tab" aria-controls="v-pills-currently" aria-selected="true">
                    <i class="fa fa-rectangle-list"></i>الإعلانات الحالية
                </button>
                <button class="nav-link bg-white rounded-3 p-4" id="v-pills-finished-tab" data-bs-toggle="pill" data-bs-target="#v-pills-finished" type="button" role="tab" aria-controls="v-pills-finished" aria-selected="false">
                    <i class="fa fa-rectangle-list"></i>الإعلانات المنتهية
                </button>
            </div>
        </div>
        <div class="col-md-8">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-currently" role="tabpanel" aria-labelledby="v-pills-currently-tab" tabindex="0">
                    <h5 class="fw-bold mb-4">جميع الإعلانات</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1 active">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1 active">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1 active">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2">
                                    <small>متبقي يومين</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="tab-pane fade" id="v-pills-finished" role="tabpanel" aria-labelledby="v-pills-finished-tab" tabindex="0">
                    <h5 class="fw-bold mb-4">جميع الإعلانات</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1 active">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1 active">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1 active">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                                    <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="posts-box">
                                <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                                <div class="right1">
                                    <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                                    <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                                </div>
                                <div class="right2 bg-primary">
                                    <small>انتهي</small>
                                </div>
                                <div class="left1">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="bottom-box">
                                    <small class="fw-light">22/11/2022</small>
                                    <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                                    <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                                    <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                                    <div class="phone">
                                        <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                                    </div>
                                    <div class="whatsapp">
                                        <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<footer id="site-footer">
    <div class="cols">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-3 p-4">
                    <a href="#"><i class="fa fa-arrow-up-long"></i></a>
                    <img src="/site/assets/images/Logo-f.png" alt="">
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent"><a href="about-us.html">من نحن</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">الفئات</a></li>
                        <li class="list-group-item bg-transparent"><a href="favorite.html">المفضلة</a></li>
                        <li class="list-group-item bg-transparent"><a href="wishlist.html">أمنياتي</a></li>
                        <li class="list-group-item bg-transparent"><a href="terms-and-conditions.html">الشروط والأحكام</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">سياسة الخصوصية</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 p-4">
                    <h4 class="mb-4 mt-4">تواصل معنا</h4>
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent">
                            <h6>عبر الهاتف</h6>
                            <a href="tel:96612346789" class="tel">+96612346789</a>
                        </li>
                        <li class="list-group-item bg-transparent">
                            <h6>عبر البريد الإلكتروني</h6>
                            <a href="mailto:info@khyark.com" class="email">info@khyark.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 p-4">
                    <a href="#">
                        <div class="bg-3">
                            <h3 class="text-white fw-bold">التجار المعتمدين</h3>
                        </div>
                    </a>
                    <h4 class="mb-4 mt-5">كن دائما على تواصل معنا</h4>
                    <form action="#" class="position-relative w-75">
                        <div class="mb-3">
                            <input type="email" class="form-control" placeholder="البريد الالكترونى">
                        </div>
                        <div class="mb-3">
                            <textarea class="form-control" rows="4" placeholder="رسالتك"></textarea>
                        </div>
                        <button type="submit" class="btn btn-contact"><i class="fa fa-arrow-left-long"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="copy">
        <small>جميع الحقوق محفوظة <span>©</span> 2023</small>
    </div>
    <div class="container">
        <div class="bottom-btn">
            <a href="add-ad.html" class="btn btn1">انشاء اعلان</a>
            <a href="#" class="btn btn2">طلب تمويل</a>
        </div>
    </div>
</footer>

<!-- JavaScript -->
<script src="/site/assets/js/jquery.min.js"></script>
<script src="/site/assets/js/wow.min.js"></script>
<script src="/site/assets/js/owl.carousel.min.js"></script>
<script src="/site/assets/js/bootstrap.bundle.min.js"></script>
<script src="/site/assets/js/custom.js"></script>

</body>

</html>