@extends('site.layout.mainlayout')
@section('content')

<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item">التجار المعتمدين</li>
    </ol>
</nav>

<div class="container">
    <div class="bg-finance">
        <h2 class="fw-bold">طلب تمويل</h2>
    </div>
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="nav flex-column nav-pills me-3 bg-light p-4" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link bg-white rounded-3 p-4 mb-3 active" id="v-pills-mortgage-tab" data-bs-toggle="pill" data-bs-target="#v-pills-mortgage" type="button" role="tab" aria-controls="v-pills-mortgage" aria-selected="true">
                    <i class="fa fa-home"></i>عقارات
                </button>
                <button class="nav-link bg-white rounded-3 p-4 mb-3" id="v-pills-personal-tab" data-bs-toggle="pill" data-bs-target="#v-pills-personal" type="button" role="tab" aria-controls="v-pills-personal" aria-selected="false">
                    <i class="fa fa-user"></i>شخصي
                </button>
                <button class="nav-link bg-white rounded-3 p-4 mb-3" id="v-pills-cars-tab" data-bs-toggle="pill" data-bs-target="#v-pills-cars" type="button" role="tab" aria-controls="v-pills-cars" aria-selected="false">
                    <i class="fa fa-car"></i>سيارات
                </button>
            </div>
        </div>
        <div class="col-md-8">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane bg-light p-4 fade show active" id="v-pills-mortgage" role="tabpanel" aria-labelledby="v-pills-mortgage-tab" tabindex="0">
                    <div class="row p-3">
                        <div class="col-md-8">
                            <form action="/site/add-fund" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <label class="form-label fw-bold mb-5">البيانات الشخصية</label>

                                    <div class="input-group has-validation mb-3">
                                        <input type="date" name="birth_date" class="form-control" placeholder="تاريخ الميلاد dd/mm/yy">
                                        <span class="m-2 mt-0 mb-0"><img src="/site/assets/images/birthday.svg" alt=""></span>
                                    </div>
                                    {{-- <select class="form-select" name="gender">
                                        <option selected>الجنسية</option>
                                        <option value="1">ذكر</option>
                                        <option value="2">انثي</option>
                                    </select> --}}

                                <label class="form-label fw-bold mt-5 mb-5">التفاصيل المالية والمهنية</label>

                                <input type="text" name="sector" class="form-control" placeholder="القطاع">
                                <input type="hidden" value="0" name="fund_type">
                                <input type="number" name="monthly_income" class="form-control" placeholder="الدخل الشهري (ريال سعودي)">

                                <input type="text" name="salary_type" class="form-control" placeholder="نوع الراتب">

                                <input type="number" name="monthly_commit" class="form-control" placeholder="الإلتزامات الشهرية الحالية (ريال سعودي)">


                                <label class="form-label fw-bold mt-5 mb-5">تفاصيل التمويل</label>

                                <input type="number" min="1" max="12" name="financing_periad" class="form-control" placeholder="فترة التمويل (عدد الشهور)">


                                <button type="submit" class="btn btn-primary w-100 pt-3 pb-3 mt-4">ارسال</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane bg-light p-4 fade" id="v-pills-personal" role="tabpanel" aria-labelledby="v-pills-personal-tab" tabindex="0">
                    <div class="row p-3">
                        <div class="col-md-8">
                            <form action="/site/add-fund" method="post" enctype="multipart/form-data">

                                {{ csrf_field() }}

                                <label class="form-label fw-bold mb-5">البيانات الشخصية</label>

                                <div class="input-group has-validation mb-3">
                                    <input type="date" class="form-control" placeholder="تاريخ الميلاد dd/mm/yy">
                                    <span class="m-2 mt-0 mb-0"><img src="/site/assets/images/birthday.svg" alt=""></span>
                                </div>
                                {{-- <select class="form-select" name="gender">
                                    <option selected>الجنسية</option>
                                    <option value="1">ذكر</option>
                                    <option value="2">انثي</option>
                                </select> --}}

                            <label class="form-label fw-bold mt-5 mb-5">التفاصيل المالية والمهنية</label>

                            <input type="text" name="sector" class="form-control" placeholder="القطاع">
                            <input type="hidden" value="1" name="fund_type">

                            <input type="number" name="monthly_income" class="form-control" placeholder="الدخل الشهري (ريال سعودي)">

                            <input type="text" name="salary_type" class="form-control" placeholder="نوع الراتب">

                            <input type="number" name="monthly_commit" class="form-control" placeholder="الإلتزامات الشهرية الحالية (ريال سعودي)">


                                <label class="form-label fw-bold mt-5 mb-5">تفاصيل التمويل</label>

                                <input type="number" min="1" max="12" name="financing_periad" class="form-control" placeholder="فترة التمويل (عدد الشهور)">




                            <button type="submit" class="btn btn-primary w-100 pt-3 pb-3 mt-4">ارسال</button>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane bg-light p-4 fade" id="v-pills-cars" role="tabpanel" aria-labelledby="v-pills-cars-tab" tabindex="0">
                    <div class="row p-3">
                        <div class="col-md-8">
                            <form action="/site/add-fund" method="post" enctype="multipart/form-data">

                                {{ csrf_field() }}

                                <label class="form-label fw-bold mb-5">البيانات الشخصية</label>

                                <div class="input-group has-validation mb-3">
                                    <input type="date" class="form-control" placeholder="تاريخ الميلاد dd/mm/yy">
                                    <span class="m-2 mt-0 mb-0"><img src="/site/assets/images/birthday.svg" alt=""></span>
                                </div>
                                {{-- <select class="form-select" name="gender">
                                    <option selected>الجنسية</option>
                                    <option value="1">ذكر</option>
                                    <option value="2">انثي</option>
                                </select> --}}

                            <label class="form-label fw-bold mt-5 mb-5">التفاصيل المالية والمهنية</label>

                            <input type="text" name="sector" class="form-control" placeholder="القطاع">
                            <input type="hidden" value="2" name="fund_type">

                            <input type="number" name="monthly_income" class="form-control" placeholder="الدخل الشهري (ريال سعودي)">

                            <input type="text" name="salary_type" class="form-control" placeholder="نوع الراتب">

                            <input type="number" name="monthly_commit" class="form-control" placeholder="الإلتزامات الشهرية الحالية (ريال سعودي)">


                                <label class="form-label fw-bold mt-5 mb-5">تفاصيل التمويل</label>

                                <input type="number" min="1" max="12" name="financing_periad" class="form-control" placeholder="فترة التمويل (عدد الشهور)">




                            <button type="submit" class="btn btn-primary w-100 pt-3 pb-3 mt-4">ارسال</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection