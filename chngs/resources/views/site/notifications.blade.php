<!doctype html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>خيارك</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800;900;1000&display=swap" rel="stylesheet">
    <link href="/site/assets/css/all.min.css" rel="stylesheet">
    <link href="/site/assets/css/animate.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site/assets/css/bootstrap.rtl.min.css" rel="stylesheet">
    <link href="/site/assets/css/style.css" rel="stylesheet">
</head>

<body>

<header id="site-header">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="/site/assets/images/Logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#siteNavbar" aria-controls="siteNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="siteNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="about-us.html">من نحن</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">الفئات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="favorite.html">المفضلة</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wishlist.html">أمنياتى</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">تواصل معنا</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="profile.html">الملف الشخصى</a>
                    </li>
                    <li class="nav-item lang">
                        <a class="nav-link" href="#">EN</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item">الاشعارات</li>
    </ol>
</nav>
<main id="page">
    <div class="container">
        <h5 class="fw-bold mb-5">الاشعارات</h5>
        <div id="pageContent">

            <div class="row">
                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-n">
                        <div class="float-start">
                            <i class="fa fa-bell"></i>
                        </div>
                        <div class="text-start">
                            <h6><a href="#">لقد تم نشر اعلانك بنجاح</a></h6>
                            <small>12/11/2022</small>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

<footer id="site-footer">
    <div class="cols">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-3 p-4">
                    <a href="#"><i class="fa fa-arrow-up-long"></i></a>
                    <img src="/site/assets/images/Logo-f.png" alt="">
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent"><a href="about-us.html">من نحن</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">الفئات</a></li>
                        <li class="list-group-item bg-transparent"><a href="favorite.html">المفضلة</a></li>
                        <li class="list-group-item bg-transparent"><a href="wishlist.html">أمنياتي</a></li>
                        <li class="list-group-item bg-transparent"><a href="terms-and-conditions.html">الشروط والأحكام</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">سياسة الخصوصية</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 p-4">
                    <h4 class="mb-4 mt-4">تواصل معنا</h4>
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent">
                            <h6>عبر الهاتف</h6>
                            <a href="tel:96612346789" class="tel">+96612346789</a>
                        </li>
                        <li class="list-group-item bg-transparent">
                            <h6>عبر البريد الإلكتروني</h6>
                            <a href="mailto:info@khyark.com" class="email">info@khyark.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 p-4">
                    <a href="#">
                        <div class="bg-3">
                            <h3 class="text-white fw-bold">التجار المعتمدين</h3>
                        </div>
                    </a>
                    <h4 class="mb-4 mt-5">كن دائما على تواصل معنا</h4>
                    <form action="#" class="position-relative w-75">
                        <div class="mb-3">
                            <input type="email" class="form-control" placeholder="البريد الالكترونى">
                        </div>
                        <div class="mb-3">
                            <textarea class="form-control" rows="4" placeholder="رسالتك"></textarea>
                        </div>
                        <button type="submit" class="btn btn-contact"><i class="fa fa-arrow-left-long"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="copy">
        <small>جميع الحقوق محفوظة <span>©</span> 2023</small>
    </div>
    <div class="container">
        <div class="bottom-btn">
            <a href="add-ad.html" class="btn btn1">انشاء اعلان</a>
            <a href="#" class="btn btn2">طلب تمويل</a>
        </div>
    </div>
</footer>

<!-- JavaScript -->
<script src="/site/assets/js/jquery.min.js"></script>
<script src="/site/assets/js/wow.min.js"></script>
<script src="/site/assets/js/owl.carousel.min.js"></script>
<script src="/site/assets/js/bootstrap.bundle.min.js"></script>
<script src="/site/assets/js/custom.js"></script>

</body>

</html>