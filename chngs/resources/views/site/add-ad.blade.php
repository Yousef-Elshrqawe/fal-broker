@extends('site.layout.mainlayout')
@section('content')

<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item"><a href="#">عقارات</a></li>
        <li class="breadcrumb-item">إضافة اعلان</li>
    </ol>
</nav>

<main id="page">
    <div class="container">
        <h5 class="fw-bold mb-5">إضافة اعلان جديد</h5>

        <form id="addForm" action="/site/addad" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="text-end mb-3">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
            </div>

            <div class="tab bg-white rounded-3 p-3 p-md-5">
                <h6 class="fw-bold mt-3 mb-2 pb-3 border-bottom border-light">نوع فئة الاعلان</h6>
                <div class="w-50">
                    {!! Form::select('cat', $categories->where('id',1)->pluck('name_ar','id')->toArray(),'', 
                    ['class' => 'form-select','disabled' => 'disabled' ])  !!}
                </div>
                <h6 class="fw-bold mt-5 mb-2 pb-3 border-bottom border-light">نوع الاعلان</h6>
                <div class="w-50">
                    {!! Form::select('subcat_id', $sub_cat->where('cat_id',1)->pluck('name_ar','id')->toArray(),'', 
                                                    ['class' => 'form-select'])  !!}
                                                    
                </div>
                <h6 class="fw-bold mt-5 mb-2 pb-3 border-bottom border-light">غرض الاعلان</h6>
                <div class="w-50">
                    {!! Form::select('type', $types->pluck('name_ar','id')->toArray(),'', 
                                                    ['class' => 'form-select'])  !!}
                </div>
                <h6 class="fw-bold mt-5 mb-2 pb-3 border-bottom border-light">مواصفات اخرى للعقار</h6>
                <div class="row">
                    @foreach($attributes->where('subcat_id',1) as $att)
                    
                    <div class="col-md-6 mt-3 pb-3">
                        <h6 class="fw-bold pb-2">{{ $att->name_ar }}</h6>
                            @switch($att->type)
                                @case(0)
                                <select name="ad_details[{{$loop->index}}]" id="" class="form-select">
                                    @foreach($sub_att->where('attr_id',$att->id) as $sub)
                                    <option value="{{ $sub->id }}">{{ $sub->name_ar }}</option>
                                    @endforeach
                                </select>
                                {{-- {!! Form::select('ad_details[{{$loop->index}}]', $sub_att->where('attr_id',$att->id)->pluck('name_ar','id')->toArray(),'', 
                                ['class' => 'form-select'])  !!} --}}
                        <input type="hidden" name="attr_type[{{$loop->index}}]" class="form-control" placeholder="" value="0">
                        <input type="hidden" name="attr_type[{{$loop->index}}]" class="form-control" placeholder="" value="0">
                        <input type="hidden" name="attr_id[{{$loop->index}}]" class="form-control" placeholder="" value="{{ $att->id }}">

                                    @break
                                @case(1)
                        <input type="number" min="0" name="ad_details[{{$loop->index}}]" class="form-control" placeholder="">
                        <input type="hidden" name="attr_type[{{$loop->index}}]" class="form-control" placeholder="" value="1">
                        <input type="hidden" name="attr_id[{{$loop->index}}]" class="form-control" placeholder="" value="{{ $att->id }}">
                                    
                                    @break
                                @case(2)  
                        <input type="text" name="ad_details[{{$loop->index}}]" class="form-control" placeholder="">
                        <input type="hidden" name="attr_type[{{$loop->index}}]" class="form-control" placeholder="" value="2">
                        <input type="hidden" name="attr_id[{{$loop->index}}]" class="form-control" placeholder="" value="{{ $att->id }}">
                                    
                                @break
                                
                                    
                            @endswitch
                        

                    </div>
                    @endforeach
                    
                </div>
                <h6 class="fw-bold mt-5 mb-2 pb-3 border-bottom border-light">مواصفات اخرى</h6>
                <div class="row">
                    <div class="col-md-6 mt-3 pb-3">
                        <h6 class="fw-bold pb-2">السعر</h6>
                        <input type="number" name="price" class="form-control" placeholder="">
                        <h6 class="fw-bold mt-3 pb-2">طريقة الدفع</h6>
                        <select class="form-select" name="payment_method">
                            <option value="0" selected>Bank Transfer</option>
                            <option value="1">Visa</option>
                        </select>
                    </div>
                    <div class="col-md-6 mt-3 pb-3">
                        <h6 class="fw-bold pb-2">الموقع</h6>
                        <div class="input-group has-validation mb-2">
                            <input type="text" class="form-control" placeholder="الموقع" name="address">
                            <input type="hidden" value="111" class="form-control" placeholder="الموقع" name="lat">
                            <input type="hidden" value="222" class="form-control" placeholder="الموقع" name="lng">
                            <span class="m-2 mt-0 mb-0"><a href="https://maps.google.com" target="_blank"><img src="/site/assets/images/map.svg" alt=""></a></span>
                        </div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d927758.0365435549!2d47.48202997738883!3d24.72499781589947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f03890d489399%3A0xba974d1c98e79fd5!2z2KfZhNix2YrYp9i2INin2YTYs9i52YjYr9mK2Kk!5e0!3m2!1sar!2seg!4v1683747145167!5m2!1sar!2seg" width="100%" height="100" class="rounded-5" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
                <h6 class="fw-bold mt-5 mb-2 pb-3 border-bottom border-light">مواصفات الاعلان</h6>
                <div class="row">
                    <div class="col-md-6 mt-3 pb-3">
                        <h6 class="fw-bold pb-2">عنوان الاعلان</h6>
                        <input type="text" class="form-control" placeholder="عنوان الاعلان" name="title">

                        <h6 class="fw-bold mt-3 pb-2">تفاصيل الاعلان</h6>
                        <textarea class="form-control" rows="4" placeholder="تفاصيل الاعلان" name="desc"></textarea>
                    </div>
                </div>
                <h6 class="fw-bold mt-3 pb-2">صور الاعلان</h6>

                <span class="float-end text-warning">يمكنك إضافة حتى 20 صورة</span>
                <div class="upload__box">
                    <div class="upload__btn-box">
                        <label class="upload__btn">
                            <p>إضغط للرفع</p>
                            <input type="file" multiple="multiple" data-max_length="20" class="upload__inputfile" name="ad_images[]">
                        </label>
                    </div>
                    <div class="bg-light p-2 p-md-4 upload__img-wrap"></div>
                </div>

            </div>

            <div class="tab">
                <h4 class="fw-bold mb-2">تمويل الاعلان</h4>
                <small>يظهر الاعلان فى المقدمة لفترة محدودة</small>


                <div class="row">
                    <div class="col-md-6">
                        @foreach($special->where('cat_id',1) as $spec)
                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3 text-center">
                                    <img src="/site/assets/images/banker.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <h5 class="text-start text-primary">لمدة {{$spec->expiration}} ايام</h5>
                                    <h6 class="text-end text-warning small">{{$spec->price}} ر.س</h6>
                                    <input class="form-check-input float-end" type="radio" name="special_ad_id" id="exampleRadios1" value="{{$spec->id}}" checked>
                                </div>
                            </div>
                        </div>
@endforeach
                        {{-- <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3 text-center">
                                    <img src="/site/assets/images/banker.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <h5 class="text-start text-primary">لمدة 14 يوم</h5>
                                    <h6 class="text-end text-warning small">400 ر.س</h6>
                                    <input class="form-check-input float-end" type="radio" name="special_ad_id" id="exampleRadios2" value="option2">
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3 text-center">
                                    <img src="/site/assets/images/banker.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <h5 class="text-start text-primary">لمدة 30 يوم</h5>
                                    <h6 class="text-end text-warning small">600 ر.س</h6>
                                    <input class="form-check-input float-end" type="radio" name="special_ad_id" id="exampleRadios3" value="option3">
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>

            @if($user->subscription->remainder_ads <= 0)
            <div class="tab">
                <h4 class="fw-bold mb-2">الباقات المتاحة</h4>
                <small>قم بإختيار الباقة الأنسب لك</small>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3">
                                    <img src="/site/assets/images/Logo-f.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <h5 class="text-start text-primary">باقة 7 ايام</h5>
                                    <small>لك 5 اعلانات</small>
                                    <h6 class="text-end text-warning small">مجاناً</h6>
                                    <input class="form-check-input float-end" type="radio" name="duration" id="exampleRadios4" value="option4">
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3">
                                    <img src="/site/assets/images/Logo-f.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <h5 class="text-start text-primary">باقة 14 يوم</h5>
                                    <small>لك 7 اعلانات</small>
                                    <h6 class="text-end text-warning small">200 ر.س</h6>
                                    <input class="form-check-input float-end" type="radio" name="duration" id="exampleRadios5" value="option5">
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3">
                                    <img src="/site/assets/images/Logo-f.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <h5 class="text-start text-primary">باقة 30 يوم</h5>
                                    <small>لك 10 اعلانات</small>
                                    <h6 class="text-end text-warning small">500 ر.س</h6>
                                    <input class="form-check-input float-end" type="radio" name="duration" id="exampleRadios6" value="option6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endif
            <div class="tab">
                <h4 class="fw-bold mb-2">وسيلة الدفع</h4>
                <small>إختر وسيلة الدفع الأنسب لك</small>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3">
                                    <img src="/site/assets/images/Anb_bank.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <small>الاسم : خيارك</small><br>
                                    <small>رقم الحساب : 1234567890</small><br>
                                    <small>الايبان : SA1234567890</small>
                                    <input class="form-check-input float-end" type="radio" name="bank" id="exampleRadios7" value="option7">
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3">
                                    <img src="/site/assets/images/rb.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <small>الاسم : خيارك</small><br>
                                    <small>رقم الحساب : 1234567890</small><br>
                                    <small>الايبان : SA1234567890</small>
                                    <input class="form-check-input float-end" type="radio" name="bank" id="exampleRadios8" value="option8">
                                </div>
                            </div>
                        </div>

                        <div class="form-check">
                            <div class="row align-items-center border border-light rounded-4 m-3 shadow">
                                <div class="col-3 p-3">
                                    <img src="/site/assets/images/Alinma_Bank.png" alt="">
                                </div>
                                <div class="col-9 bg-white rounded-4 p-3">
                                    <small>الاسم : خيارك</small><br>
                                    <small>رقم الحساب : 1234567890</small><br>
                                    <small>الايبان : SA1234567890</small>
                                    <input class="form-check-input float-end" type="radio" name="bank" id="exampleRadios9" value="option9">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    {{-- <div class="col-md-4">
                        <h6 class="fw-bold mt-3 mb-3 pb-3 border-bottom">الفاتورة</h6>

                        <div class="bg-white border rounded-4 p-4">
                            <small>سعر الباقة : 200 ر.س</small><br><br>
                            <small>رسوم التمويل : 200 ر.س</small><br><br>
                            <small class="text-primary fw-bold">الاجمالي : 400 ر.س</small>
                        </div>

                    </div> --}}
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-4">
                    <button type="button" class="btn btn-light border-primary w-100 p-2 mb-3" id="prevBtn" onclick="nextPrev(-1)">السابق</button>
                    <button type="button" class="btn btn-primary w-100 p-2" id="nextBtn" onclick="nextPrev(1)">التالي</button>
                </div>
            </div>

        </form>

    </div>
</main>


@endsection