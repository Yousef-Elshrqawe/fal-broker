@extends('site.layout.mainlayout')
@section('content')


<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item"><a href="index.html">عقارات</a></li>
        <li class="breadcrumb-item">تفاصيل العقار</li>
    </ol>
</nav>

<div class="container">

    <div class="post-box">
        <div class="right">
            <small class="bg-warning text-white rounded-3 p-3 pt-0 pb-0">ممول</small>
        </div>
        <div class="left">
            <a href="#" class="btn btn-light text-danger">إضافة إلى المفضلة <i class="fa fa-heart"></i></a>
        </div>
        <div class="bottom bg-white rounded-4 shadow">
            <small class="fw-light">{{ date('Y-m-d',strtotime($ad->start_date)) }}</small>
            <h5 class="fw-bold text-primary pt-2 pb-2">{{$ad->price}} ر.س</h5>
            <h6 class="fw-bold"><a href="#">{{$ad->title}}</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
            <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> {{$ad->address}}</small>
            <div class="id-post">#{{ $ad->id }}</div>
        </div>
    </div>

    <div class="owl-carousel owl-theme carousel-g">
        <div class="item" data-hash="zero">
            <img src="/site/assets/images/{{ isset($ad->images[0]) ?? $ad->images[0]->image :: default.jpg }}" class="rounded-4" alt="">
        </div>
        {{-- <div class="item" data-hash="one">
            <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
        </div>
        <div class="item" data-hash="two">
            <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
        </div>
        <div class="item" data-hash="three">
            <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
        </div>
        <div class="item" data-hash="four">
            <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
        </div> --}}
    </div>
    <div class="owl-carousel owl-theme carousel-g-th mt-4 mb-4">
        @foreach($ad->images as $image)
        <div class="item">
            <a href="#">
                <img src="/site/assets/images/{{ isset($image) ?? $image->image :: default.jpg }}" class="rounded-4" alt="">
            </a>
        </div>
        @endforeach
        {{-- <div class="item">
            <a href="#one">
                <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
            </a>
        </div>
        <div class="item">
            <a href="#two">
                <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
            </a>
        </div>
        <div class="item">
            <a href="#three">
                <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
            </a>
        </div>
        <div class="item">
            <a href="#four">
                <img src="/site/assets/images/bg-s.jpg" class="rounded-4" alt="">
            </a>
        </div> --}}
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="bg-light rounded-4 rounded-bottom-0 p-4 mt-4 mb-4">
                <div class="row">
                    <div class="col-md-3">
                        <img src="/site/assets/images/User-c.png" class="bg-white rounded-circle p-2" alt="">
                    </div>
                    <div class="col-md-9">
                        <h5><a href="#">مؤسسة عقار</a><img src="/site/assets/images/true-s.png" alt=""></h5>
                        <small class="fw-light text-muted p-4">(99) 4.9 <i class="fa fa-star"></i></small>
                    </div>
                </div>
                <hr>
                <div class="text-end">
                    <a href="#" target="_blank" class="m-2"><img src="/site/assets/images/chat-c.svg" alt=""></a>
                    <a href="#" target="_blank" class="m-2"><img src="/site/assets/images/whatsapp-c.svg" alt=""></a>
                    <a href="#" target="_blank" class="m-2"><img src="/site/assets/images/call-c.svg" alt=""></a>
                </div>
            </div>
            <div class="bg-light p-3 mt-4 mb-4">
                <h5 class="mt-3 fw-bold">اعلانات مشابهه</h5>
                <hr>
                <div class="scroll-r">
                    <div class="posts-box">
                        <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                        <div class="right1">
                            <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>350م</small></span>
                            <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>15</small></span>
                            <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                        </div>
                        <div class="left1">
                            <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="bottom-box">
                            <small class="fw-light">22/11/2022</small>
                            <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                            <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                            <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                            <div class="phone">
                                <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                            </div>
                            <div class="whatsapp">
                                <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="posts-box">
                        <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                        <div class="right1">
                            <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>350م</small></span>
                            <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>15</small></span>
                            <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                        </div>
                        <div class="left1 active">
                            <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="bottom-box">
                            <small class="fw-light">22/11/2022</small>
                            <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                            <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                            <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                            <div class="phone">
                                <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                            </div>
                            <div class="whatsapp">
                                <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="posts-box">
                        <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                        <div class="right1">
                            <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                            <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                            <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                        </div>
                        <div class="left1 active">
                            <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="bottom-box">
                            <small class="fw-light">22/11/2022</small>
                            <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                            <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                            <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                            <div class="phone">
                                <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                            </div>
                            <div class="whatsapp">
                                <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="posts-box">
                        <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                        <div class="right1">
                            <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                            <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                            <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                        </div>
                        <div class="left1">
                            <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="bottom-box">
                            <small class="fw-light">22/11/2022</small>
                            <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                            <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                            <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                            <div class="phone">
                                <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                            </div>
                            <div class="whatsapp">
                                <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="posts-box">
                        <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                        <div class="right1">
                            <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                            <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                            <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                        </div>
                        <div class="left1">
                            <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="bottom-box">
                            <small class="fw-light">22/11/2022</small>
                            <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                            <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                            <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                            <div class="phone">
                                <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                            </div>
                            <div class="whatsapp">
                                <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-light rounded-4 rounded-top-0 p-3 mt-4 mb-4">
                <h5 class="mt-3 fw-bold">تقييمات المستخدمين</h5>
                <hr>
                <div class="text-center">
                    <img src="/site/assets/images/Ratings.png" alt="">
                </div>
                <div class="scroll-r">
                    <div class="bg-white p-3 position-relative border border-light">
                        <div class="ratings text-end small">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <p class="small">عقار جيد كما بالوصف</p>
                        <small class="fw-bold">محمد محمود</small>
                        <small class="fw-light m-2">12/11/2022</small>
                    </div>
                    <div class="bg-white p-3 position-relative border border-light">
                        <div class="ratings text-end small">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <p class="small">عقار جيد كما بالوصف</p>
                        <small class="fw-bold">محمد محمود</small>
                        <small class="fw-light m-2">12/11/2022</small>
                    </div>
                    <div class="bg-white p-3 position-relative border border-light">
                        <div class="ratings text-end small">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <p class="small">عقار جيد كما بالوصف</p>
                        <small class="fw-bold">محمد محمود</small>
                        <small class="fw-light m-2">12/11/2022</small>
                    </div>
                    <div class="bg-white p-3 position-relative border border-light">
                        <div class="ratings text-end small">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <p class="small">عقار جيد كما بالوصف</p>
                        <small class="fw-bold">محمد محمود</small>
                        <small class="fw-light m-2">12/11/2022</small>
                    </div>
                    <div class="bg-white p-3 position-relative border border-light">
                        <div class="ratings text-end small">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                        </div>
                        <p class="small">عقار جيد كما بالوصف</p>
                        <small class="fw-bold">محمد محمود</small>
                        <small class="fw-light m-2">12/11/2022</small>
                    </div>
                </div>

                <form>
                    <div class="rating mt-5 mb-3">
                        <label>
                            <input type="radio" name="stars" value="1" />
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="2" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="3" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="4" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                        <label>
                            <input type="radio" name="stars" value="5" />
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                            <span class="icon">★</span>
                        </label>
                    </div>

                    <textarea class="form-control mb-3" rows="4" placeholder="ملاحظات"></textarea>

                    <button type="submit" class="btn btn-primary w-100 pt-3 pb-3 mt-4">ارسال</button>
                </form>



            </div>
        </div>
        <div class="col-md-8">
            <div class="bg-light rounded-4 rounded-bottom-0 p-4 mt-4 mb-4">
                <div class="row">
                    <div class="col-md">
                        <h5 class="fw-bold">مواصفات العقار</h5>
                    </div>
                    <div class="col-md text-end">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>350م</small></span>
                            <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>15</small></span>
                            <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                </div>
            </div>
            <div class="bg-light p-5 mt-4 mb-4">
                <h5 class="mt-3 fw-bold">وصف العقار</h5>
                <hr>
                <p>
                    {{$ad->desc}}
                </p>
            </div>
            <a href="#"><img src="/site/assets/images/banner2.jpg" class="rounded-4" alt=""></a>
            <div class="bg-light rounded-4 rounded-top-0 p-5 mt-4 mb-4">
                <h5 class="mt-3 fw-bold">مواصفات اخرى</h5>
                <hr>
                <div class="row">
                    <div class="col-md-6 pb-3"><span class="w-title">الطابق : </span> 2</div>
                    <div class="col-md-6 pb-3"><span class="w-title">سنة البناء : </span> 2014</div>
                    <div class="col-md-6 pb-3"><span class="w-title">التشطيب : </span> سوبر لوكس</div>
                    <div class="col-md-6 pb-3"><span class="w-title">الاطلالة : </span> على النيل</div>
                    <div class="col-md-6 pb-3"><span class="w-title">البائع : </span> المالك</div>
                    <div class="col-md-6 pb-3"><span class="w-title">طريقة الدفع : </span> كاش</div>
                    <div class="col-md-6 pb-3"><span class="w-title">عدد الغرف : </span> 6</div>
                    <div class="col-md-6 pb-3"><span class="w-title">عدد الحمامات : </span> 4</div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection