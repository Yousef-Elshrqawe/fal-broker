<!doctype html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>خيارك</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800;900;1000&display=swap" rel="stylesheet">
    <link href="/site/assets/css/all.min.css" rel="stylesheet">
    <link href="/site/assets/css/animate.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site/assets/css/bootstrap.rtl.min.css" rel="stylesheet">
    <link href="/site/assets/css/style.css" rel="stylesheet">

</head>


<body>
    {{-- @dd(auth()->user()) --}}
<header id="site-header">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="/site/assets/images/Logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#siteNavbar" aria-controls="siteNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="siteNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="about-us">@lang('site.who_us')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">@lang('site.cats')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="favorite">@lang('site.favorite')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wishlist">@lang('site.wish')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactus">@lang('site.contact_us')</a>
                    </li>
                    @if(auth()->user())
                    <li class="nav-item">
                        <a class="nav-link" href="/site/logout-user">@lang('site.logout')</a>
                    </li>
                    @endif
                    
                    @if(!auth()->user())
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#regModal">@lang('site.register')</a>
                        <div class="modal fade z-3" id="regModal" tabindex="-1" aria-labelledby="regModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="regForm" action="/site/register" method="post" enctype="multipart/form-data">
                                        {{{ csrf_field() }}}
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <div class="tabReg p-3 p-md-5">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="fw-bold mb-2">نوع الحساب</h5>
                                                    <p class="small">من فضلك قم بإختيار نوع الحساب</p>
                                                    <select class="form-select" name="type">
                                                        <option selected>نوع الحساب</option>
                                                        <option value="0">فرد</option>
                                                        <option value="1">مؤسسة</option>
                                                    </select>
                                                </div>
                                                <div class="col-3"><img src="/site/assets/images/li1.png" alt=""></div>
                                            </div>
                                        </div>
                                        <div class="tabReg p-3 p-md-5">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="fw-bold mb-2">مستخدم جديد</h5>
                                                    <p class="small">مرحباً بك</p>
                                                    <input type="text" name="name" class="form-control mb-2" placeholder="الاسم">
                                                    <div class="row">
                                                    <input type="text" style="width:80%" name="phone" class="form-control mb-2" placeholder="رقم الجوال">
                                                    <input type="text" style="width:20%" name="country_code" class="form-control mb-2" placeholder="+966">
                                                </div>
                                                    <input type="email" name="email" class="form-control mb-2" placeholder="البريد الإلكتروني">
                                                    @if(app()->getLocale() == 'ar')
                                                    {!! Form::select('governorate_id', $govern->pluck('name_ar','id')->toArray(),'', 
                                                    ['class' => 'form-select mb-2'])  !!}
                                                    @elseif(app()->getLocale() == 'en')
                                                    {!! Form::select('governorate_id', $govern->pluck('name_en','id')->toArray(),'', 
                                                    ['class' => 'form-select mb-2'])  !!}
                                                    @endif
                                                    {{-- <select class="form-select mb-2" name="governorate_id">
                                                        <option selected>المدينة</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                    </select> --}}
                                                    <div class="input-group has-validation mb-2">
                                                        <input name="address" type="text" class="form-control" placeholder="الموقع">
                                                        <input name="lat" type="text" style="display: none" value="5555555" class="form-control" placeholder="الموقع">
                                                        <input name="lng" type="text" style="display: none" value="123123123" class="form-control" placeholder="الموقع">
                                                        <span class="m-2 mt-0 mb-0"><a href="https://maps.google.com" target="_blank"><img src="/site/assets/images/map.svg" alt=""></a></span>

                                                        
                                                    </div>
                                                    <div class="mb-3">
                                                        <input class="form-control" type="password" name="password" placeholder="كلمة المرور">
                                                    </div>
                                                    <div class="mb-3">
                                                        <input class="form-control" type="password" name="password" placeholder="تأكيد كلمة المرور">
                                                    </div>
                                                </div>
                                                <div class="col-3"><img src="/site/assets/images/li2.png" alt=""></div>
                                            </div>
                                        </div>


                                        <div class="tabReg p-3 p-md-5">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="fw-bold mb-2">مستخدم جديد</h5>
                                                    <p class="small">من فضلك قم بإرفاق السجل التجاري وصورة الهوية للمراجعة وتوثيق الحساب</p>
                                                    <input type="text" class="form-control mb-2" name="number_identity" placeholder="رقم الهوية">
                                                    <div class="row">
                                                        <div class="col-6 pt-3">
                                                            <h6>صورة الهوية</h6>
                                                            <img id="blah1" src="/site/assets/images/up-img.jpg" class="pb-3" alt="">
                                                            <input name="image_identity" type="file" onchange="readURL1(this);">
                                                        </div>
                                                        <div class="col-6 pt-3">
                                                            <h6>صورة السجل التجاري</h6>
                                                            <img id="blah2" src="/site/assets/images/up-img.jpg" class="pb-3" alt="">
                                                            <input name="commercial_register" type="file" onchange="readURL2(this);">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-3"><img src="/site/assets/images/li2.png" alt=""></div>
                                            </div>
                                        </div>
                                        <div class="tabReg p-3 p-md-5">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="fw-bold mb-2">الباقات المتاحة</h5>
                                                    <p class="small">قم بإختيار الباقة الأنسب لك</p>
                                                    <div class="form-check">
                                                        <div class="row align-items-center border border-light rounded-4 mb-2 shadow">
                                                            <div class="col-3 p-3">
                                                                <img src="/site/assets/images/Logo-f.png" alt="">
                                                            </div>
                                                            <div class="col-9 bg-white rounded-4 p-3">
                                                                <h5 class="text-start text-primary">باقة 7 ايام</h5>
                                                                <small>لك 5 اعلانات</small>
                                                                <h6 class="text-end text-warning small">مجاناً</h6>
                                                                <input class="form-check-input float-end" type="radio" name="subscription_id" id="exampleRadios4" value="1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-check">
                                                        <div class="row align-items-center border border-light rounded-4 mb-2 shadow">
                                                            <div class="col-3 p-3">
                                                                <img src="/site/assets/images/Logo-f.png" alt="">
                                                            </div>
                                                            <div class="col-9 bg-white rounded-4 p-3">
                                                                <h5 class="text-start text-primary">باقة 14 يوم</h5>
                                                                <small>لك 7 اعلانات</small>
                                                                <h6 class="text-end text-warning small">200 ر.س</h6>
                                                                <input class="form-check-input float-end" type="radio" name="subscription_id" id="exampleRadios5" value="2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-check">
                                                        <div class="row align-items-center border border-light rounded-4 mb-2 shadow">
                                                            <div class="col-3 p-3">
                                                                <img src="/site/assets/images/Logo-f.png" alt="">
                                                            </div>
                                                            <div class="col-9 bg-white rounded-4 p-3">
                                                                <h5 class="text-start text-primary">باقة 30 يوم</h5>
                                                                <small>لك 10 اعلانات</small>
                                                                <h6 class="text-end text-warning small">500 ر.س</h6>
                                                                <input class="form-check-input float-end" type="radio" name="subscription_id" id="exampleRadios6" value="3">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-3"><img src="/site/assets/images/li3.png" alt=""></div>
                                            </div>
                                        </div>
                                        <div class="tabReg p-3 p-md-5">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="fw-bold mb-2">وسيلة الدفع</h5>
                                                    <p class="small">إختر وسيلة الدفع الأنسب لك</p>
                                                    <div class="form-check">
                                                        <div class="row align-items-center border border-light rounded-4 mb-2 shadow">
                                                            <div class="col-3 p-3">
                                                                <img src="/site/assets/images/Anb_bank.png" alt="">
                                                            </div>
                                                            <div class="col-9 bg-white rounded-4 p-3">
                                                                <small>الاسم : خيارك</small><br>
                                                                <small>رقم الحساب : 1234567890</small><br>
                                                                <small>الايبان : SA1234567890</small>
                                                                <input class="form-check-input float-end" type="radio" name="payment_method" id="exampleRadios7" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-check">
                                                        <div class="row align-items-center border border-light rounded-4 mb-2 shadow">
                                                            <div class="col-3 p-3">
                                                                <img src="/site/assets/images/rb.png" alt="">
                                                            </div>
                                                            <div class="col-9 bg-white rounded-4 p-3">
                                                                <small>الاسم : خيارك</small><br>
                                                                <small>رقم الحساب : 1234567890</small><br>
                                                                <small>الايبان : SA1234567890</small>
                                                                <input class="form-check-input float-end" type="radio" name="payment_method" id="exampleRadios8" value="1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-check">
                                                        <div class="row align-items-center border border-light rounded-4 mb-2 shadow">
                                                            <div class="col-3 p-3">
                                                                <img src="/site/assets/images/Alinma_Bank.png" alt="">
                                                            </div>
                                                            <div class="col-9 bg-white rounded-4 p-3">
                                                                <small>الاسم : خيارك</small><br>
                                                                <small>رقم الحساب : 1234567890</small><br>
                                                                <small>الايبان : SA1234567890</small>
                                                                <input class="form-check-input float-end" type="radio" name="payment_method" id="exampleRadios9" value="2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-3"><img src="/site/assets/images/li4.png" alt=""></div>
                                            </div>
                                        </div>
                                        <div class="tabReg p-3 p-md-5">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="fw-bold mb-2">تأكيد الحوالة البنكية</h5>
                                                    <p class="small">قم بإرفاق صورة الحوالة البنكية</p>
                                                    <img id="blah3" src="/site/assets/images/up-img.jpg" class="pb-3" alt="">
                                                    <input name="bank_transfer" type="file" onchange="readURL3(this);">
                                                </div>
                                                <div class="col-3"><img src="/site/assets/images/li5.png" alt=""></div>
                                            </div>
                                        </div>
                                        <div class="tabReg p-3 p-md-5 bg-primary text-center">
                                            <img src="/site/assets/images/li6.png" alt="">
                                            <h5 class="fw-bold mt-2 mb-2 text-warning">تم تسجيل بياناتك بنجاح</h5>
                                            <p class="small text-white fw-bold">سوف يتم مراجعة بياناتك والتحقق منها للتوثيق حيث يمكنك بعد ذلك انشاء أي اعلان بسهولة</p>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="text-white text-start p-2">بالقيام بالتسجيل فأنت توافق على <a href="terms-and-conditions" target="_blank">الشروط والأحكام الخاصة بنا</a></span>
                                            <button type="button" id="prevBtnReg" onclick="nextPrevReg(-1)"><i class="fa fa-arrow-right-long"></i></button>
                                            <button type="button" id="nextBtnReg" onclick="nextPrevReg(1)"></button></div>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                        
                        <script>
                            ////
                            //Multi step form - regForm.
                            ////
                            let currentTabReg = 0;
                            showTabReg(currentTabReg);

                            function showTabReg(n) {
                                // This function will display the specified tab of the form...
                                let x = document.getElementsByClassName("tabReg");
                                x[n].style.display = "block";
                                //... and fix the Previous/Next buttons:
                                if (n == 0) {
                                    document.getElementById("prevBtnReg").style.display = "none";
                                } else {
                                    document.getElementById("prevBtnReg").style.display = "inline";
                                }
                                if (n == (x.length - 1)) {
                                    document.getElementById("nextBtnReg").innerHTML = "إنهاء";
                                    document.getElementById("prevBtnReg").display = "none";
                                } else {
                                    document.getElementById("nextBtnReg").innerHTML = "<i class=\"fa fa-arrow-left-long\"></i>";
                                }
                                //... and run a function that will display the correct step indicator:
                                fixStepIndicatorReg(n)
                            }

                            function nextPrevReg(n) {
                                // This function will figure out which tab to display
                                let x = document.getElementsByClassName("tabReg");
                                // Exit the function if any field in the current tab is invalid:
                                if (n == 1 && !validateFormReg()) return false;
                                // Hide the current tab:
                                x[currentTabReg].style.display = "none";
                                // Increase or decrease the current tab by 1:
                                currentTabReg = currentTabReg + n;
                                // if you have reached the end of the form...
                                if (currentTabReg >= x.length) {
                                    // ... the form gets submitted:
                                    document.getElementById("regForm").submit();
                                    return false;
                                }
                                // Otherwise, display the correct tab:
                                showTabReg(currentTabReg);
                            }

                            function validateFormReg() {
                                // This function deals with validation of the form fields
                                let x, y, i, valid = true;
                                x = document.getElementsByClassName("tabReg");
                                y = x[currentTabReg].getElementsByTagName("input");
                                // A loop that checks every input field in the current tabReg:
                                for (i = 0; i < y.length; i++) {
                                    // If a field is empty...
                                    if (y[i].value == "") {
                                        // add an "invalid" class to the field:
                                        y[i].className += " invalid";
                                        // and set the current valid status to false
                                        valid = false;
                                    }
                                }
                                // If the valid status is true, mark the step as finished and valid:
                                if (valid) {
                                    document.getElementsByClassName("step")[currentTabReg].className += " finish";
                                }
                                return valid; // return the valid status
                            }

                            function fixStepIndicatorReg(n) {
                                // This function removes the "active" class of all steps...
                                let i, x = document.getElementsByClassName("step");
                                for (i = 0; i < x.length; i++) {
                                    x[i].className = x[i].className.replace(" active", "");
                                }
                                //... and adds the "active" class on the current step:
                                x[n].className += " active";
                            }
                        </script>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">@lang('site.login')</a>
                        <div class="modal fade z-3" id="loginModal" tabindex="-1" aria-labelledby="regModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="loginForm" action="/site/login-user" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row p-3 p-md-5">
                                            <div class="col-9">
                                                <h5 class="fw-bold mb-2">دخول الآن</h5>
                                                <p class="small">مرحباً بك</p>
                                                <input type="email" class="form-control mb-2" name="email" placeholder="اسم" required>
                                                <div class="mb-3">
                                                    <input class="form-control" type="password" name="password" placeholder="كلمة المرور" required>
                                                </div>
                                                <a href="#" class="text-warning fw-light float-end" data-bs-toggle="modal" data-bs-target="#restModal">نسيت كلمة المرور؟</a>
                                            </div>
                                            <div class="col-3"><img src="/site/assets/images/li7.png" alt=""></div>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="text-white text-start p-2">ليس لديك حساب؟ <a href="#" data-bs-toggle="modal" data-bs-target="#regModal">تسجيل جديد</a></span>
                                            <button type="submit">دخول</button>
                                            {{-- <input type="submit" value="دخول"> --}}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade z-3" id="restModal" tabindex="-1" aria-labelledby="restModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="restForm" action="">
                                        <div class="row p-3 p-md-5">
                                            <div class="col-9">
                                                <h5 class="fw-bold mb-2">تغيير كلمة المرور</h5>
                                                <p class="small">قم بإدخال رقم الجوال الخاص بك لكي تتمكن من تغيير كلمة المرور</p>
                                                <input type="text" class="form-control mb-2" placeholder="رقم الجوال">
                                            </div>
                                            <div class="col-3"><img src="/site/assets/images/li7.png" alt=""></div>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="text-white text-start p-2">ليس لديك حساب؟ <a href="#" data-bs-toggle="modal" data-bs-target="#regModal">تسجيل جديد</a></span>
                                            <button type="button">ارسال</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endif
                    <li class="nav-item lang">
                        @if(app()->getLocale() == 'ar')
                        <a class="nav-link" href="/en/site">EN</a>
                        @else
                        <a class="nav-link" href="/ar/site">AR</a>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
@if (Session::has('success'))
    <center><div class="alert alert-success alert-dismissible" style="z-index: 2" role="alert">
        <strong>Success !</strong> {{ session('success') }}
    </div></center>
@endif

@if (Session::has('error'))
    <center><div class="alert alert-danger alert-dismissible" style="z-index: 2" role="alert">
        <strong>Error !</strong> {{ session('error') }}
    </div></center>
@endif    
<section id="breadcrumb-home">
    
    <div class="overlay"></div>
    <div class="container">
        <div class="owl-carousel owl-theme home-carousel">
            <div class="item">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <img class="position-relative rounded-4 shadow" src="/site/assets/images/img-a.jpg" alt="">
                    </div>
                    <div class="col-md-5">
                        <h2>@lang('site.intro_desc')</h2>
                        <p>
                            @lang('site.everything_you_need')
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <img class="position-relative rounded-4 shadow" src="/site/assets/images/img-a.jpg" alt="">
                    </div>
                    <div class="col-md-5">
                        <h2>@lang('site.intro_desc')</h2>
                        <p>
                            @lang('site.everything_you_need')
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <img class="position-relative rounded-4 shadow" src="/site/assets/images/img-a.jpg" alt="">
                    </div>
                    <div class="col-md-5">
                        <h2>@lang('site.intro_desc')</h2>
                        <p>
                            @lang('site.everything_you_need')
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <img class="position-relative rounded-4 shadow" src="/site/assets/images/img-a.jpg" alt="">
                    </div>
                    <div class="col-md-5">
                        <h2>@lang('site.intro_desc')</h2>
                        <p>
                            @lang('site.everything_you_need')
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <img class="position-relative rounded-4 shadow" src="/site/assets/images/img-a.jpg" alt="">
                    </div>
                    <div class="col-md-5">
                        <h2>@lang('site.intro_desc')</h2>
                        <p>
                            @lang('site.everything_you_need')
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="catIcon">
    <div class="container">
        <div class="row">
            <div class="col-md-6 shadow rounded-5 p-5 bg-white z-3">
                <div class="row text-center">
                    <div class="col-md-6 border-end">
                        <a href="#">
                            <img src="/site/assets/images/cat-h-1.svg" alt="">
                            <span class="h4">@lang('site.diff_levels')</span>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="#">
                            <img src="/site/assets/images/cat-h-2.svg" alt="">
                            <span class="h4">@lang('site.auth_dealers')</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about">
    <div class="container">
        <h2 class="allTitle mb-5">@lang('site.who_us')</h2>
        <p>
            @lang('site.who_us_para1')

        </p>
        <p>
            @lang('site.who_us_para2')
        </p>
        {{-- <p>
            يحتفظ التطبيق بحق تعديل شروط الاستخدام الماثلة في أي وقت وبدون اشعار مسبق، وأنك توافق على أن كل زيارة تقوم بها للتطبيق سوف تخضع إلى شروط الاستخدام المفعلة في هذا الوقت، وأن إستخدامك للتطبيق في أي وقت يؤكد على أنك قمت بالقراءة و الموافقة على هذه الاحكام و الشروط
        </p> --}}
    </div>
</section>

<section id="categories">
    <div class="container bg-light p-5 rounded-4">
        <h2 class="allTitle">@lang('site.cats')</h2>
        <h6 class="mb-5 fw-light">@lang('site.choose_cat')</h6>
        <div class="owl-carousel owl-theme cat-carousel">
            @foreach($cats as $cat)
            <div class="item">
                <a href="#">
                    <div class="box">
                        <img src="/site/assets/images/{{ $cat->image }}" alt="">
                        <h6>{{ $cat->name }}</h6>
                    </div>
                </a>
            </div>
            @endforeach
            
        </div>
    </div>
</section>

<section id="recent">
    <div class="container">
        <h2 class="allTitle mb-5">@lang('site.newest_ads')</h2>
        <div class="owl-carousel owl-theme recent-carousel">
            @foreach($latest_ads as $ad)
            <div class="item">
                <div class="posts-box">
                    <a href="/site/ad_info/{{ $ad->id }}"><img src="/site/assets/images/{{ $ad->images->first()->image }}" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">{{$ad->start_date}}</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">{{$ad->price}} ر.س</h5>
                        <h6 class="fw-bold"><a href="#">{{$ad->title}}</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> {{$ad->address}}</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
@endforeach

            
        </div>
    </div>
</section>

<section>
    <div class="container">
        <a href="#">
            <img src="/site/assets/images/banner.jpg" alt="">
        </a>
    </div>
</section>

<section id="famous">
    <div class="container">
        <h2 class="allTitle mb-5">إعلانات مشهورة</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1 active">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                        <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                        <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1 active">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                        <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                        <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post1.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post2.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/area.svg" alt=""> <small>215م</small></span>
                        <span class="p-3"><img src="/site/assets/images/bathroom.svg" alt=""> <small>3</small></span>
                        <span class="p-3"><img src="/site/assets/images/bedroom.svg" alt=""> <small>3</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1 active">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="posts-box">
                    <a href="#"><img src="/site/assets/images/img-post3.jpg" class="attached" alt=""></a>
                    <div class="right1">
                        <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>هيونداي</small></span>
                        <span class="p-3"><img src="/site/assets/images/car-transmission.svg" alt=""> <small>اوتوماتيك</small></span>
                        <span class="p-3"><img src="/site/assets/images/car.svg" alt=""> <small>ازرق</small></span>
                    </div>
                    <div class="right2">
                        <small>ممول</small>
                    </div>
                    <div class="left1">
                        <a href="#"><i class="fa fa-heart"></i></a>
                    </div>
                    <div class="bottom-box">
                        <small class="fw-light">22/11/2022</small>
                        <h5 class="fw-bold text-primary pt-2 pb-2">500000 ر.س</h5>
                        <h6 class="fw-bold"><a href="#">فيلا دوبلكس</a> <small class="fw-light text-muted p-4">4.9 99 <i class="fa fa-star"></i></small></h6>
                        <small class="fw-light text-muted"><i class="fa fa-map-marker-alt"></i> جدة, السعودية</small>
                        <div class="phone">
                            <a href="#"><img src="/site/assets/images/phone.svg" alt=""></a>
                        </div>
                        <div class="whatsapp">
                            <a href="#"><img src="/site/assets/images/whatsapp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer id="site-footer">
    <div class="cols">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-3 p-4">
                    <a href="#"><i class="fa fa-arrow-up-long"></i></a>
                    <img src="/site/assets/images/Logo-f.png" alt="">
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent"><a href="about-us">من نحن</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">الفئات</a></li>
                        <li class="list-group-item bg-transparent"><a href="favorite">المفضلة</a></li>
                        <li class="list-group-item bg-transparent"><a href="wishlist">أمنياتي</a></li>
                        <li class="list-group-item bg-transparent"><a href="terms-and-conditions">الشروط والأحكام</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">سياسة الخصوصية</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 p-4">
                    <h4 class="mb-4 mt-4">تواصل معنا</h4>
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent">
                            <h6>عبر الهاتف</h6>
                            <a href="tel:96612346789" class="tel">+96612346789</a>
                        </li>
                        <li class="list-group-item bg-transparent">
                            <h6>عبر البريد الإلكتروني</h6>
                            <a href="mailto:info@khyark.com" class="email">info@khyark.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 p-4">
                    <a href="#">
                        <div class="bg-3">
                            <h3 class="text-white fw-bold">التجار المعتمدين</h3>
                        </div>
                    </a>
                    <h4 class="mb-4 mt-5">كن دائما على تواصل معنا</h4>
                    <form action="#" class="position-relative w-75">
                        <div class="mb-3">
                            <input type="email" class="form-control" placeholder="البريد الالكترونى">
                        </div>
                        <div class="mb-3">
                            <textarea class="form-control" rows="4" placeholder="رسالتك"></textarea>
                        </div>
                        <button type="submit" class="btn btn-contact"><i class="fa fa-arrow-left-long"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="copy">
        <small>جميع الحقوق محفوظة <span>©</span> 2023</small>
    </div>
    @if(Auth::user())
    <div class="container">
        <div class="bottom-btn">
            <a href="/site/add_ad" class="btn btn1">انشاء اعلان</a>
            <a href="/site/fund" class="btn btn2">طلب تمويل</a>
        </div>
    </div>
    @endif
</footer>

<!-- JavaScript -->
<script src="/site/assets/js/jquery.min.js"></script>
<script src="/site/assets/js/wow.min.js"></script>
<script src="/site/assets/js/owl.carousel.min.js"></script>
<script src="/site/assets/js/bootstrap.bundle.min.js"></script>
<script src="/site/assets/js/custom.js"></script>

</body>

</html>