@extends('site.layout.mainlayout')
@section('content')


<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item">الملف الشخصي</li>
    </ol>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="nav flex-column nav-pills bg-light p-4" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link bg-white rounded-3 p-4 mb-3 active" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="true">
                    <i class="fa fa-user"></i>الملف الشخصي
                </button>
                <button class="nav-link bg-white rounded-3 p-4 mb-3" id="v-pills-change-tab" data-bs-toggle="pill" data-bs-target="#v-pills-change" type="button" role="tab" aria-controls="v-pills-change" aria-selected="false">
                    <i class="fa fa-lock"></i>تغيير كلمة المرور
                </button>
                <button class="nav-link bg-white rounded-3 p-4 mb-3 text-warning" id="v-pills-delete-tab" data-bs-toggle="pill" data-bs-target="#v-pills-delete" type="button" role="tab" aria-controls="v-pills-delete" aria-selected="false">
                    <i class="fa fa-trash-can"></i>حذف الملف الشخصى
                </button>
            </div>

            <div class="bg-light p-3 mt-4 mb-4 d-none d-md-block">
                <h5 class="mt-3 fw-bold">الباقة الحالية</h5>
                <hr>
                <div class="row border border-white rounded-4 m-2 shadow">
                    <div class="col-4 p-3">
                        <img src="/assets/img/users/Logo-f.png" alt="">
                    </div>
                    <div class="col-8 bg-white rounded-4 p-3">
                        {{-- @dd($profile->subscription)  --}}
                        <h5 class="text-primary">@if(app()->getLocale() == 'ar') {{ $profile->subscription->subscriptionName->name_ar }} @else {{ $profile->subscription->subscriptionName->name_en }} @endif</h5>
                        <h6 class="text-muted">لك {{ $profile->subscription->subscriptionName->ads_number }} اعلانات</h6>
                        <h6 class="text-end text-warning small">متبقي {{$profile->subscription->remainder_ads}} اعلانات</h6>
                        <h6 class="text-end text-danger small">تنتهى فى {{date('Y-m-d',strtotime($profile->subscription->end_date))}}</h6>
                    </div>
                </div>
            </div>
            <div class="bg-light rounded-4 rounded-top-0 p-3 mt-4 mb-4 d-none d-md-block">
                <h5 class="mt-3 fw-bold">تقييمات المستخدمين</h5>
                <hr>
                <div class="text-center">
                    <img src="/site/assets/images/Ratings.png" alt="">
                </div>
                <div class="scroll-r">

                    @foreach($ratings as $rating)
                    <div class="bg-white p-3 position-relative border border-light">
                        <div class="ratings text-end small">
                            @for($i=1;$i<=round($rating->rate);$i++)
                            <span class="fa fa-star checked"></span>
                            @endfor
                        </div>
                        <p class="small">{{ $rating->comment }}</p>
                        <small class="fw-bold">{{ $profile->usersRate->where('id',$rating->user_id)->first()->name }}</small>
                        <small class="fw-light m-2">12/11/2022</small>
                    </div>
                    @endforeach

                </div>

            </div>
        </div>
        <div class="col-md-8">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane bg-light fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab" tabindex="0">
                    <div class="row p-3 p-md-5 m-2">
                        <div class="col-md-8">
                            <form action="/site/update_data" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
<?php $image = explode('/',$profile->image) ?>
                                <div class="mb-3">
                                    <!-- Button trigger modal -->
                                    <a type="button" data-bs-toggle="modal" data-bs-target="#profilePicture">
                                        <img src="/assets/img/users/{{ end($image) }}" alt="" >
                                    </a>
                                    <small class="fw-light text-muted p-4">(99) 4.9 <i class="fa fa-star"></i></small>

                                    <!-- Modal -->
                                    <div class="modal fade" id="profilePicture" tabindex="-1" aria-labelledby="profilePictureLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <input class="form-control" type="file" name="image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="الاسم" value="{{ $profile->name }}" name="name">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" dir="ltr" placeholder="رقم الجوال" value="{{ $profile->phone }}" name="phone">
                                </div>
                                <div class="mb-3">
                                    <input type="email" class="form-control" placeholder="البريد الإلكتروني" value="{{ $profile->email }}" name="email">
                                </div>
                                <div class="mb-3">
                                    <select class="form-select" name="governorate_id">
                                        <option value="{{ $profile->governorate_id }}" selected>@if(app()->getLocale() == 'ar') {{ $profile->governorate->name_ar }} @else {{ $profile->governorate->name_en }} @endif</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <div class="input-group has-validation">
                                        <input type="text" class="form-control" placeholder="الموقع" name="address">
                                        <input type="hidden" class="form-control" placeholder="lat" name="lat" value="111">
                                        <input type="hidden" class="form-control" placeholder="long" name="lng" value="222">
                                        <span class="m-2 mt-0 mb-0"><a href="https://maps.google.com" target="_blank"><img src="/site/assets/images/map.svg" alt=""></a></span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-warning text-white w-100 pt-3 pb-3 mt-4">تعديل</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane bg-light fade" id="v-pills-change" role="tabpanel" aria-labelledby="v-pills-change-tab" tabindex="0">
                    <div class="row p-3 p-md-5 m-2">
                        <div class="col-md-8">
                            <form method="post" action="/site/change_password" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h5 class="fw-bold mb-5">تغيير كلمة المرور</h5>
                                <div class="mb-3 field">
                                    <input class="form-control" type="password" name="old_password" placeholder="كلمة المرور القديمة">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="mb-3 field">
                                    <input class="form-control" type="password" name="password" placeholder="كلمة المرور الجديدة">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="mb-3 field">
                                    <input class="form-control" type="password" name="password" placeholder="تأكيد كلمة المرور الجديدة">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <button type="submit" class="btn btn-primary w-100 pt-3 pb-3 mt-4">تغيير كلمة المرور</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane bg-light p-3 p-md-5 m-2 fade" id="v-pills-delete" role="tabpanel" aria-labelledby="v-pills-delete-tab" tabindex="0">
                    <h5 class="fw-bold mb-5">حذف الملف الشخصى</h5>
                    <div class="bg-white p-3 p-md-5 m-3 m-md-5 rounded-3 text-center">
                        <h5 class="fw-bold mb-5">هل أنت متأكد من حذف الحساب؟</h5>
                        <hr class="border-light border-3">
                        <div class="row pt-3">
                            <div class="col">
                                <a href="#" class="btn btn-danger fw-bold w-75 shadow btn-lg">نعم حذف</a>
                            </div>
                            <div class="col">
                                <a href="#" class="btn btn-close-white text-danger fw-bold w-75 shadow btn-lg">لا شكراً</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
