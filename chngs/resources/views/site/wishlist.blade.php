<!doctype html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>خيارك</title>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800;900;1000&display=swap" rel="stylesheet">
    <link href="/site/assets/css/all.min.css" rel="stylesheet">
    <link href="/site/assets/css/animate.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site/assets/css/bootstrap.rtl.min.css" rel="stylesheet">
    <link href="/site/assets/css/style.css" rel="stylesheet">
</head>

<body>

<header id="site-header">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="/site/assets/images/Logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#siteNavbar" aria-controls="siteNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="siteNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="about-us.html">من نحن</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">الفئات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="favorite.html">المفضلة</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wishlist.html">أمنياتى</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">تواصل معنا</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="profile.html">الملف الشخصى</a>
                    </li>
                    <li class="nav-item lang">
                        <a class="nav-link" href="#">EN</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<nav id="breadcrumb">
    <ol class="container breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
        <li class="breadcrumb-item">أمنياتي</li>
    </ol>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="nav flex-column nav-pills me-3 bg-light p-4" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link bg-white rounded-3 p-4 mb-3 active" id="v-pills-wishes-tab" data-bs-toggle="pill" data-bs-target="#v-pills-wishes" type="button" role="tab" aria-controls="v-pills-wishes" aria-selected="true">
                    <i class="fa fa-tag"></i>أمنياتي
                </button>
                <button class="nav-link bg-white rounded-3 p-4 text-warning" id="v-pills-addition-tab" data-bs-toggle="pill" data-bs-target="#v-pills-addition" type="button" role="tab" aria-controls="v-pills-addition" aria-selected="false">
                    <i class="fa fa-plus"></i>إضافة أمنية جديدة
                </button>
            </div>
        </div>
        <div class="col-md-8">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane bg-light p-4 fade show active" id="v-pills-wishes" role="tabpanel" aria-labelledby="v-pills-wishes-tab" tabindex="0">
                    <h5 class="fw-bold mb-4">أمنياتي</h5>
                    <div class="bg-white rounded-3 mb-5 p-3 p-md-5">
                        <div class="pt-4 mb-4">
                            <div class="top-w">
                                <h6 class="fw-bold mb-3 pb-3 border-bottom">أمنية #1</h6>
                                <span class="left"><a href="#"><img src="/site/assets/images/delete.svg" alt=""></a></span>
                            </div>
                            <div class="bg-light rounded-3 p-3">
                                <div class="row">
                                    <div class="col-md-6 pb-3"><span class="w-title">الطابق : </span> 2</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">سنة البناء : </span> 2014</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">التشطيب : </span> سوبر لوكس</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">الاطلالة : </span> على النيل</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">البائع : </span> المالك</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">طريقة الدفع : </span> كاش</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">عدد الغرف : </span> 4</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">عدد الحمامات : </span> 2</div>
                                </div>
                            </div>
                        </div>
                        <div class="pt-4 mb-4">
                            <div class="top-w">
                                <h6 class="fw-bold mb-3 pb-3 border-bottom">أمنية #2</h6>
                                <span class="left"><a href="#"><img src="/site/assets/images/delete.svg" alt=""></a></span>
                            </div>
                            <div class="bg-light rounded-3 p-3">
                                <div class="row">
                                    <div class="col-md-6 pb-3"><span class="w-title">الطابق : </span> 2</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">سنة البناء : </span> 2014</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">التشطيب : </span> سوبر لوكس</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">الاطلالة : </span> على النيل</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">البائع : </span> المالك</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">طريقة الدفع : </span> كاش</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">عدد الغرف : </span> 4</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">عدد الحمامات : </span> 2</div>
                                </div>
                            </div>
                        </div>
                        <div class="pt-4 mb-4">
                            <div class="top-w">
                                <h6 class="fw-bold mb-3 pb-3 border-bottom">أمنية #3</h6>
                                <span class="left"><a href="#"><img src="/site/assets/images/delete.svg" alt=""></a></span>
                            </div>
                            <div class="bg-light rounded-3 p-3">
                                <div class="row">
                                    <div class="col-md-6 pb-3"><span class="w-title">الطابق : </span> 2</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">سنة البناء : </span> 2014</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">التشطيب : </span> سوبر لوكس</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">الاطلالة : </span> على النيل</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">البائع : </span> المالك</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">طريقة الدفع : </span> كاش</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">عدد الغرف : </span> 4</div>
                                    <div class="col-md-6 pb-3"><span class="w-title">عدد الحمامات : </span> 2</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="tab-pane bg-light p-4 fade" id="v-pills-addition" role="tabpanel" aria-labelledby="v-pills-addition-tab" tabindex="0">
                    <h5 class="fw-bold mb-4">إضافة أمنية جديدة</h5>
                    <div class="row bg-white p-3 p-md-5 m-2 rounded-3">
                        <div class="col-md-8">
                            <form>
                                <div class="mb-5">
                                    <label for="1" class="form-label fw-bold">الموقع</label>
                                    <div class="input-group has-validation">
                                        <input type="text" class="form-control" id="1">
                                        <span class="m-2 mt-0 mb-0"><a href="https://maps.google.com" target="_blank"><img src="/site/assets/images/map.svg" alt=""></a></span>
                                    </div>
                                </div>
                                <div class="mb-5">
                                    <label for="2" class="form-label fw-bold">النوع</label>
                                    <select class="form-select" id="2">
                                        <option selected>النوع</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-5">
                                    <label for="3" class="form-label fw-bold">الغرض</label>
                                    <select class="form-select" id="3">
                                        <option selected>غرض الإعلان</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-5">
                                    <label for="4" class="form-label fw-bold">المساحة</label>
                                    <select class="form-select" id="4">
                                        <option selected>المساحة</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-5">
                                    <label class="form-label fw-bold">السعر</label><span class="float-end text-primary">ر.س</span>
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="s1" class="form-label">من</label>
                                            <input type="text" class="form-control" id="s1">
                                        </div>
                                        <div class="col-6">
                                            <label for="s2" class="form-label">إلى</label>
                                            <input type="text" class="form-control" id="s2">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-5">
                                    <label for="6" class="form-label fw-bold">عدد الغرف</label>
                                    <select class="form-select" id="6">
                                        <option selected>عدد الغرف</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-5">
                                    <label for="7" class="form-label fw-bold">طريقة الدفع</label>
                                    <select class="form-select" id="7">
                                        <option selected>طريقة الدفع</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-5">
                                    <label for="8" class="form-label fw-bold">الاطلالة</label>
                                    <select class="form-select" id="8">
                                        <option selected>الاطلالة</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-5">
                                    <label for="9" class="form-label fw-bold">نوع التشطيب</label>
                                    <select class="form-select" id="9">
                                        <option selected>نوع التشطيب</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary w-100 pt-3 pb-3 mt-4">إضافة</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer id="site-footer">
    <div class="cols">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-3 p-4">
                    <a href="#"><i class="fa fa-arrow-up-long"></i></a>
                    <img src="/site/assets/images/Logo-f.png" alt="">
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent"><a href="about-us.html">من نحن</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">الفئات</a></li>
                        <li class="list-group-item bg-transparent"><a href="favorite.html">المفضلة</a></li>
                        <li class="list-group-item bg-transparent"><a href="wishlist.html">أمنياتي</a></li>
                        <li class="list-group-item bg-transparent"><a href="terms-and-conditions.html">الشروط والأحكام</a></li>
                        <li class="list-group-item bg-transparent"><a href="#">سياسة الخصوصية</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 p-4">
                    <h4 class="mb-4 mt-4">تواصل معنا</h4>
                    <ul class="list-group list-group-flush mt-3">
                        <li class="list-group-item bg-transparent">
                            <h6>عبر الهاتف</h6>
                            <a href="tel:96612346789" class="tel">+96612346789</a>
                        </li>
                        <li class="list-group-item bg-transparent">
                            <h6>عبر البريد الإلكتروني</h6>
                            <a href="mailto:info@khyark.com" class="email">info@khyark.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 p-4">
                    <a href="#">
                        <div class="bg-3">
                            <h3 class="text-white fw-bold">التجار المعتمدين</h3>
                        </div>
                    </a>
                    <h4 class="mb-4 mt-5">كن دائما على تواصل معنا</h4>
                    <form action="#" class="position-relative w-75">
                        <div class="mb-3">
                            <input type="email" class="form-control" placeholder="البريد الالكترونى">
                        </div>
                        <div class="mb-3">
                            <textarea class="form-control" rows="4" placeholder="رسالتك"></textarea>
                        </div>
                        <button type="submit" class="btn btn-contact"><i class="fa fa-arrow-left-long"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="copy">
        <small>جميع الحقوق محفوظة <span>©</span> 2023</small>
    </div>
    <div class="container">
        <div class="bottom-btn">
            <a href="add-ad.html" class="btn btn1">انشاء اعلان</a>
            <a href="#" class="btn btn2">طلب تمويل</a>
        </div>
    </div>
</footer>

<!-- JavaScript -->
<script src="/site/assets/js/jquery.min.js"></script>
<script src="/site/assets/js/wow.min.js"></script>
<script src="/site/assets/js/owl.carousel.min.js"></script>
<script src="/site/assets/js/bootstrap.bundle.min.js"></script>
<script src="/site/assets/js/custom.js"></script>

</body>

</html>