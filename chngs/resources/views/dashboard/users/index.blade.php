<?php $page="admin.users";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
									<h4 class="card-title mb-0">@lang('user.user')</h4>
									<p class="card-text">
                                        <form action="" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                    @if(Auth::guard('admin')->user()->hasPermission('users_create'))
                                                        <a href="{{route('admin.users.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @else
                                                        <a href="#" class="btn btn-primary" disabled><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </p>
								</div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>@lang('user.first_name')</th>
                                                    <th>@lang('user.last_name')</th>
													<th>@lang('user.username')</th>
													<th>@lang('user.image')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($users as $item)
                                                    <tr>
                                                        <td>
                                                            {{$item->first_name}}
                                                        </td>
                                                        <td>
                                                            {{$item->last_name}}
                                                        </td>

                                                        <td>
                                                            {{$item->username}}
                                                        </td>
                                                        <td>
                                                            <img src="{{ $item->image_path }}" style="width:50px" class="img-thumbnail" alt="">
                                                        </td>
                                                        <td>
                                                            @if(Auth::guard('admin')->user()->hasPermission('users_update'))
                                                                <a href="{{route('admin.users.edit',$item->id)}}" class="btn btn-info btn-sm">@lang('user.edit')</a>
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('users_delete'))
                                                                <form action="{{route('admin.users.destroy',$item->id)}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
