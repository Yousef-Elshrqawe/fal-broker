<?php $page="admin.coupons";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.coupons')
                                        <small>{{ $total }}</small>
                                    </h3>
									<p class="card-text">
                                        <div class="row">
                                            <div class="col-md-4">
                                                @if(Auth::guard('admin')->user()->hasPermission('cats_create'))
                                                    <a href="{{route('admin.coupons.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                @else
                                                    <a href="#" class="btn btn-primary" disabled><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                @endif
                                            </div>
                                        </div>
                                    </p>
								</div>

								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.coupon_code')</th>
													<th>@lang('user.discount_percent')</th>
                                                    <th>@lang('user.count_of_use')</th>
                                                    <th>@lang('user.times_for_user')</th>
                                                    <th>@lang('user.end_date')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($coupons as $index => $item)
                                                    <tr>
                                                        <td>{{$index + 1}}</td>
                                                        <td>
                                                            {{$item->code}}
                                                        </td>
                                                        <td>
                                                            {{$item->percent}}
                                                        </td>
                                                        <td>
                                                            {{$item->count_of_use}}
                                                        </td>
                                                        <td>
                                                            {{$item->times_for_user}}
                                                        </td>
                                                        <td>
                                                            {{$item->end_date}}
                                                        </td>
                                                        <td>
                                                            @if(Auth::guard('admin')->user()->hasPermission('cats_update'))
                                                                <a href="{{route('admin.coupons.edit',$item->id)}}" class="btn btn-info btn-sm">@lang('user.edit')</a>
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                            @endif

                                                            {{-- @if(Auth::guard('admin')->user()->hasPermission('cats_delete'))
                                                                <form action="{{route('admin.coupons.destroy',$item->id)}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif --}}
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
