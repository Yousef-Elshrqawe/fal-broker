
 @if (App::isLocale('en'))
 <!-- jQuery -->
 <script src="{{asset('assets/js/jquery-3.5.0.min.js')}}"></script>
 <!-- Bootstrap Core JS -->
 <script src="{{asset('assets/js/popper.min.js')}}"></script>
 <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
 <!-- Slimscroll JS -->
 <script src="{{asset('assets/js/jquery.slimscroll.min.js')}}"></script>
 <!-- Form Validation JS -->
 <script src="{{asset('assets/js/form-validation.js')}}"></script>
 <!-- Mask JS -->
 <script src="{{asset('assets/js/jquery.maskedinput.min.js')}}"></script>
 <script src="{{asset('assets/js/mask.js')}}"></script>
 <!-- Select2 JS -->
 <script src="{{asset('assets/js/select2.min.js')}}"></script>
 <!-- Datatable JS -->
 <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
 <!-- Datetimepicker JS -->
 <script src="{{asset('assets/js/moment.min.js')}}"></script>
 <script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
 <!-- Tagsinput JS -->
 <script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
 <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
 <!-- Chart JS -->
 @if(Route::is(['pagee','leads-dashboard','projects-dashboard']))
 <script src="{{asset('assets/js/morris.js')}}"></script>
 <script src="{{asset('assets/plugins/raphael/raphael.min.js')}}"></script>
 <script src="{{asset('assets/js/chart.js')}}"></script>
 <script src="{{asset('assets/js/linebar.min.js')}}"></script>
 <script src="{{asset('assets/js/piechart.js')}}"></script>
 <script src="{{asset('assets/js/apex.min.js')}}"></script>
 @endif
 <!-- theme JS -->
 <script src="{{asset('assets/js/theme-settings.js')}}"></script>
 <!-- Custom JS -->
 <script src="{{asset('assets/js/app.js')}}"></script>
 <script src="{{asset('assets/js/sticky.js')}}"></script>

@else
 <!-- jQuery -->
 <script src="{{asset('assets/js/jquery-3.5.0.min.js')}}"></script>
 <!-- Bootstrap Core JS -->
 <script src="{{asset('assets/js/popper.min.js')}}"></script>
 <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
 <!-- Slimscroll JS -->
 <script src="{{asset('assets/js/jquery.slimscroll.min.js')}}"></script>
 <!-- Form Validation JS -->
 <script src="{{asset('assets/js/form-validation.js')}}"></script>
 <!-- Mask JS -->
 <script src="{{asset('assets/js/jquery.maskedinput.min.js')}}"></script>
 <script src="{{asset('assets/js/mask.js')}}"></script>
 <!-- Select2 JS -->
 <script src="{{asset('assets/js/select2.min.js')}}"></script>
 <!-- Datatable JS -->
 <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
 <!-- Datetimepicker JS -->
 <script src="{{asset('assets/js/moment.min.js')}}"></script>
 <script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
 <!-- Tagsinput JS -->
 <script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
 <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
 <!-- Chart JS -->
 @if(Route::is(['pagee','leads-dashboard','projects-dashboard']))
 <script src="{{asset('assets/js/morris.js')}}"></script>
 <script src="{{asset('assets/plugins/raphael/raphael.min.js')}}"></script>
 <script src="{{asset('assets/js/chart.js')}}"></script>
 <script src="{{asset('assets/js/linebar.min.js')}}"></script>
 <script src="{{asset('assets/js/piechart.js')}}"></script>
 <script src="{{asset('assets/js/apex.min.js')}}"></script>
 @endif
 <!-- theme JS -->
 <script src="{{asset('assets/js/theme-settings.js')}}"></script>
 <!-- Custom JS -->
 <script src="{{asset('assets/js/app.js')}}"></script>
 <script src="{{asset('assets/js/sticky.js')}}"></script>
@endif

 {{--<!-- Bootstrap 3.3.7 -->--}}
 <script src="{{ asset('assets/bootstrap.min.js') }}"></script>

 <!-- custom.. -->
 <script src="{{ asset('assets/custom/image_preview.js') }}"></script>
 <script src="{{ asset('assets/custom/order.js') }}"></script>
 {{--icheck--}}
 <script src="{{ asset('assets/icheck/icheck.min.js') }}"></script>

<script>
 $(document).ready(function () {
     $('.sidebar-menu').tree();
     //icheck
     $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
         checkboxClass: 'icheckbox_minimal-blue',
         radioClass: 'iradio_minimal-blue'
     });
    //  $('.delete').click(function (e) {
    //      var that = $(this)
    //      e.preventDefault();
    //      var n = new Noty({
    //          text: "@lang('site.confirm_delete')",
    //          type: "warning",
    //          killer: true,
    //          buttons: [
    //              Noty.button("@lang('site.yes')", 'btn btn-success mr-2', function () {
    //                  that.closest('form').submit();
    //              }),
    //              Noty.button("@lang('site.no')", 'btn btn-primary mr-2', function () {
    //                  n.close();
    //              })
    //          ]
    //      });
    //      n.show();
    //  });//end of delete
     //image preview
     $(".image").change(function() {
         if (this.files && this.files[0]) {
             var reader = new FileReader();
             reader.onload = function(e) {
                 $('.image-preview').attr('src', e.target.result);
             }
             reader.readAsDataURL(this.files[0]); // convert to base64 string
         }
     });
     $(".image-ar").change(function () {
         if (this.files && this.files[0]) {
             var reader = new FileReader();
             reader.onload = function (e) {
                 $('.image-preview-ar').attr('src', e.target.result);
             }
             reader.readAsDataURL(this.files[0]);
         }
     });
     $(".image-en").change(function () {
         if (this.files && this.files[0]) {
             var reader = new FileReader();
             reader.onload = function (e) {
                 $('.image-preview-en').attr('src', e.target.result);
             }
             reader.readAsDataURL(this.files[0]);
         }
     });
     //ckeditor direction
     CKEDITOR.config.language = "{{ app()->getLocale() }}"
 })
</script>

{{-- dependent select boxes --}}
<script>
 $(document).ready(function(){
 
  $('.dynamic').change(function(){
   if($(this).val() != '')
   {
    var select = $(this).attr("id");
    var value = $(this).val();
    var dependent = $(this).data('dependent');
    var _token = $('input[name="_token"]').val();
    $.ajax({
     url:"{{ route('admin.ajaxLoad') }}",
     method:"POST",
     data:{select:select, value:value, _token:_token, dependent:dependent},
     success:function(result)
     {
      $('#'+dependent).html(result);
     }
 
    })
   }
  });
 
  $('#category').change(function(){
   $('#subcategory').val('');
   $('#city').val('');
  });
 
  $('#subcategory').change(function(){
   $('#city').val('');
  });
  
 
 });
 </script>

{{-- confirm delete --}}
<script>
    $(document).ready(function(){

    $('.delete').click(function (e) {
        var that = $(this)
        e.preventDefault();
        var n = new Noty({
            text: "@lang('user.confirm_delete')",
            type: "warning",
            killer: true,
            buttons: [
                Noty.button("@lang('user.yes')", 'btn btn-success mr-2', function () {
                    that.closest('form').submit();
                }),
                Noty.button("@lang('user.no')", 'btn btn-primary mr-2', function () {
                    n.close();
                })
            ]
        });
        n.show();
    });//end of delete
   });
</script>
@yield('scripts')
