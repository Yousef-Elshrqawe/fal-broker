<?php $page="admin.categories";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.attributes')
                                        <small>{{ $total }}</small>
                                    </h3>
                                    @if(Auth::guard('admin')->user()->hasPermission('attribute_create'))
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" id="add-task" data-toggle="modal" data-target="#add_task">@lang('user.add')</button>
                                      @else
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" disabled>@lang('user.add')</button>
                                    @endif
									<p class="card-text">
                                        {{-- <form action="{{route('admin.categories.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                    {{-- @if(Auth::guard('admin')->user()->hasPermission('create_cats'))
                                                        <a href="{{route('admin.categories.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @else
                                                        <a href="#" class="btn btn-primary" disabled><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @endif --}}
                                                {{-- </div>
                                            </div>
                                        </form>  --}}
                                    </p>
								</div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.attr_name')</th>
                                                    <th>@lang('user.attrIcon')</th>
                                                    <th>@lang('user.attrType')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($attributes as $index => $item)
                                                    <tr>
                                                        <td>{{$index + 1}}</td>
                                                        @if (App::isLocale('en'))
                                                            <td>
                                                                {{$item->name_en}}
                                                            </td>
                                                        @else
                                                            <td>
                                                                {{$item->name_ar}}
                                                            </td>
                                                        @endif

                                                        <td>
                                                            <img src="{{ $item->icon_path }}" style="width:50px" class="img-thumbnail" alt="">
                                                        </td>
                                                        
                                                        @if ($item->type == 0)
                                                            <td>
                                                                @lang('user.multichoiceAttr')
                                                            </td>
                                                        @elseif ($item->type == 1) 
                                                            <td>
                                                                @lang('user.numericAttr')
                                                            </td>
                                                        @elseif ($item->type == 2) 
                                                            <td>
                                                                @lang('user.textAttr')
                                                            </td>
                                                        @endif
                                                        

                                                        <td>
                                                            @if(Auth::guard('admin')->user()->hasPermission('attribute_read'))
                                                                @if ($item->type == 0)
                                                                <a href="{{route('admin.attributes.show',['cat'=>$cat,'subcat'=>$subcat->id,'attribute'=>$item->id])}}" 
                                                                    class="btn btn-info btn-sm">@lang('user.subattr')</a>
                                                                @endif
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.subattr')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('attribute_update'))
                                                                <a class="modal-effect btn btn-info btn-sm" data-effect="effect-scale" id="edit-task" data-toggle="modal" data-target="#edit_task"
                                                                        data-id="{{ $item->id }}" data-country_ar="{{ $item->name_ar }}"
                                                                        data-country_en="{{ $item->name_en }}" data-icon="{{ $item->icon_path}}">
                                                                    @lang('user.edit')
                                                                </a>

                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('attribute_delete'))
                                                                <form action="{{route('admin.attributes.destroy',['cat'=>$cat , 'subcat'=>$subcat->id, 'attribute'=>$item->id])}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <input type="hidden" name="attr_id" value="{{$item->id}}" id="">
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
                    @include('dashboard.attributes.create')
                    @include('dashboard.attributes.edit')
				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
@section('scripts')
<script>
    //Edit Catgeoy
    $('#edit_task').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name_ar = button.data('country_ar')
        var name_en = button.data('country_en')
        var icon = button.data('icon')
        var modal = $(this)
        modal.find('.modal-body #country_id').val(id);
        modal.find('.modal-body #country-AR').val(name_ar);
        modal.find('.modal-body #country-EN').val(name_en);
        modal.find('.modal-body #country-icon').attr("src", icon);
        //$("#country-icon").attr("src", icon);
    })
</script>
@endsection

