<section class="content">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.add')</h3>
        </div>
        <div class="box-body">

            @include('partials._errors')
            <form action="{{ route('admin.subattributes.save' , ['cat'=>$cat,'subcat'=>$attribute->subcat_id]) }}" method="post" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label>@lang('user.subattrAr')</label>
                    <input type="text" name="name_ar" class="form-control" placeholder="@lang('user.subattrAr')" value="{{ old('name_ar') }}" >
                </div>
                <div class="form-group">
                    <label>@lang('user.subattrEn')</label>
                    <input type="text" name="name_en" placeholder="@lang('user.subattrEn')" class="form-control" value="{{ old('name_en') }}" >
                </div>

                {{-- <div class="form-group">
                    <label>@lang('user.value')</label>
                    <input type="number" name="value" placeholder="@lang('user.value')" class="form-control" value="{{ old('value') }}" >
                </div> --}}
                
                <input type="hidden" name="attr_id" value="{{$attribute->id}}">
                <div class="form-group">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i>@lang('user.add')</button>
                </div>
            </form>
        </div>
    </div>

</section><!-- end of content -->
