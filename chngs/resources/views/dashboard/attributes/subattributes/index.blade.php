<?php $page="admin.categories";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<div class="page-wrapper">

    <section class="content">

        <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title" style="margin-bottom: 10px;">
                @if (App::isLocale('en'))
                    {{ $attribute->name_en }} 
                @else
                    {{ $attribute->name_ar }}
                @endif
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        @include('dashboard.attributes.subattributes.create' ,  ['attribute' => $attribute])

                    </div>
                    <div class="col-md-6">
                        @include('dashboard.attributes.subattributes.show' , ['subattributes' => $attribute->subattributes()->paginate(5)])
                    </div>
                </div>

            </div>
        </div>

    </section><!-- end of content -->

</div><!-- end of content wrapper -->
@endsection
