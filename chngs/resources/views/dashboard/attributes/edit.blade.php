 <!-- Modal -->
 <div class="modal fade" id="edit_task" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close md-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title text-center">@lang('user.attributes')</h4>
                <button type="button" class="close xs-close" data-dismiss="modal">×</button>
              </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('admin.attributes.update' , ['cat'=>$cat , 'subcat'=>$subcat->id ,'attribute'=>$total==0?'0':$item])}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            {{-- <h4>Task Details</h4> --}}
                            <div class="form-group row">
                                <input type="hidden" name="id" id="country_id" value="">
                                <div class="col-sm-6">
                                     <label class="col-form-label">@lang('user.attrAr') <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name_ar" id="country-AR" placeholder="@lang('user.attrAr')">
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">@lang('user.attrEn') <span class="text-danger">*</span></label>
                                   <input class="form-control" type="text" name="name_en" id="country-EN" placeholder="@lang('user.attrEn')">
                               </div>
                               {{-- <div class="col-sm-6">
                                    <label>@lang('user.attrType')</label>
                                    <select name ="type" class="form-control" aria-label=".form-select-lg example">
                                        <option value="0">@lang('user.multichoiceAttr')</option>
                                        <option value="1">@lang('user.textAttr')</option>
                                    </select>
                                </div> --}}
                                <div class="col-sm-6">
                                    <label class="col-form-label">@lang('user.attrIcon')</label>
                                    <input class="form-control image" type="file" name="icon"  placeholder="@lang('user.attrIcon')">
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <img id="country-icon" src=""
                                                class="img-thumbnail image-preview" style="width: 100px;">
                                        </div>
                                    </div>
                               </div>
                            </div>
                            <div class="text-center py-3">
                                <button type="submit" class="border-0 btn btn-primary btn-gradient-primary btn-rounded">@lang('user.edit')</button>&nbsp;&nbsp;
                                {{-- <button type="button" class="btn btn-secondary btn-rounded">Cancel</button> --}}
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
<!-- modal -->
