<?php $page="terms";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.terms')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.setting.terms.update',$setting->id)}}" method="POST" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <h4 class="card-title">@lang('user.terms')</h4>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.term_ar')</label>
                                            <textarea name="term_ar" id="" class="form-control" cols="30" rows="10">{!! $setting->term_ar !!}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.term_en')</label>
                                            <textarea name="term_en" id="" class="form-control" cols="30" rows="10">{!! $setting->term_en !!}</textarea>
                                        </div>
                                    </div>


                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.submit')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
