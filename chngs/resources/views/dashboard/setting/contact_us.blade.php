<?php $page="contact_us";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.technical-support')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.setting.contact-us.update',$setting->id)}}" method="POST" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <h4 class="card-title">@lang('user.technical-support')</h4>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.phone')</label>
                                            <input type="text" name="phone" class="form-control" value="{{ $setting->phone}}" >
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.email')</label>
                                            <input type="text" name="email" class="form-control" value="{{ $setting->email}}" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.website')</label>
                                            <input type="text" name="website" class="form-control" value="{{ $setting->website}}" required>
                                        </div>
                                    </div>


                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.submit')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
