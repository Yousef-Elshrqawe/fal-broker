<?php $page="banner";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('users.banner')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.setting.banner.update',$setting->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <h4 class="card-title">@lang('users.banner')</h4>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('users.link_banner')</label>
                                            <input type="text" name="link_banner" class="form-control" value="{{ $setting->link_banner}}" >
                                        </div>
                                    </div>
                                    <div class="form-row">
                                    <div class="col-md-6 md-3">
                                        <div class="form-group">
                                            <label>{{ __('users.image_banner') }}</label>
                                            <input type="file" name="image_banner"
                                                class="form-control" value="{{ $setting->image_banner }}" >
                                            @error('photo')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{$setting->image_banner ??''}}" target="blank">
                                        <img src="{{$setting->image_banner }}" class="img-thumbnail" >
                                        {{--  <img src="{{$setting->image_banner }}" class="img-thumbnail" style="margin-top: 2.3rem !important;width: 100px;">  --}}
                                        </a>
                                    </div>
                                    </div>


                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.submit')</button>
                                    <a href="{{route('admin.setting.banner.delete', ['id'=> $setting->id])}}"
                                        class="btn btn-outline-danger box-shadow-3 mr-1 ">@lang('users.delete_image_banner')</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
