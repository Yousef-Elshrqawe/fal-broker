<?php $page="admin.country";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<div class="page-wrapper">

    <section class="content">

        <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title" style="margin-bottom: 10px;">
                @if (App::isLocale('en'))
                    {{ $govern->name_en }}
                @else
                    {{ $govern->name_ar }}
                @endif
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        @include('dashboard.country.governorates.districts.create' ,  ['govern' => $govern])

                    </div>
                    <div class="col-md-6">
                        @include('dashboard.country.governorates.districts.show' , ['districts' => $govern->districts()->latest()->paginate(3)])
                    </div>
                </div>

            </div>
        </div>

    </section><!-- end of content -->

</div><!-- end of content wrapper -->
@endsection
