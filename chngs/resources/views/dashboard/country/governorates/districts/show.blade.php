<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.districts')
                <small>{{ $districts->total() }}</small>
            </h3>
        </div>
        <div class="box-body">

            @if($districts->count() > 0)
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                            <th>@lang('user.districts')</th>
                        <th>@lang('user.operations')</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($districts as $index => $district)
                        <tr>
                            <td>{{ $index +1 }}</td>
                            @if (App::isLocale('en'))
                                <td>{{ $district->name_en }}</td>
                            @else
                                <td>{{ $district->name_ar }}</td>
                            @endif
                            <td>
                                <form method="post"

                                    action="{{route('admin.districts.destroy' , ['country'=>$country->id,'governorate'=>$govern->id, 'district'=>$district->id])}}"
                                    style="display: inline-block">
                                    @csrf()
                                    @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>@lang('user.delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {{ $districts->appends(request()->query())->links() }}

            @else
                <h2>@lang('site.no_data_found')</h2>
            @endif

        </div>
    </div>

</section><!-- end of content -->
