<?php $page="admin.advertisements";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                
                <div class="row">
                    <div class="col-md-6">{{-- advertisement Details --}}
                        <div class="card-body">
                            <div class="table-responsive">
                                <label style="font-syle:bold; font-size:14px">@lang('user.advertisement_details')</label>
                                <table class="datatable table table-stripped mb-0 datatables table-bordered">
                                    <tr>
                                        <th>#</th>
                                        <td>{{$item->id}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.name')</th>
                                        <td>{{$item->title}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.desc')</th>
                                        <td>{{$item->desc}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.type')</th>
                                        @if (app()->getlocale() == 'ar')
                                            <td>{{$item->getType->name_ar}}</td>
                                        @else
                                            <td>{{$item->getType->name_en}}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>@lang('user.price')</th>
                                        <td>{{$item->price}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.customerName')</th>
                                        <td>{{$item->user->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.phone')</th>
                                        <td>{{$item->user->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.address')</th>
                                        <td>{{$item->address}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.status')</th>
                                        <td>
                                            @if ($item->finished == 1)
                                                @lang('user.finished')
                                            @else
                                                @lang('user.current')
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.subscribe')</th>
                                        <td>
                                            @if ($item->user->subscription)
                                                @if ($item->user->subscription->remainder_ads > 0)
                                                    @if (app()->getlocale()=='ar')
                                                        {{$item->user->subscription->subscriptionName->name_ar}}
                                                    @else
                                                        {{$item->user->subscription->subscriptionName->name_en}}
                                                    @endif
                                                @endif
                                            @else
                                                
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.paymentWay')</th>
                                        <td>{{$item->payment_way}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.start_date')</th>
                                        <td>{{$item->start_date}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.end_date')</th>
                                        <td>{{$item->end_date}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        
        


            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
