<?php $page="admin.advertisements";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
									<h4 class="card-title mb-0">@lang('user.advertisements')</h4>
									<p class="card-text">
                                        <form action="{{route('admin.advertisements.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                            </div>
                                        </form>
                                    </p>
								</div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.name')</th>
                                                    <th>@lang('user.status')</th>
                                                    <th>@lang('user.address')</th>
                                                    <th>@lang('user.start_date')</th>
													<th>@lang('user.end_date')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($advertisements as $index=>$item)
                                                    <tr>
                                                        <td>{{$item->id}}</td>
                                                        <td>
                                                            {{$item->title}}
                                                        </td>
                                                        <td>
                                                            {{$item->finished==1?__("user.finished"): __("user.current")}}
                                                        </td>
                                                        <td>{{$item->address}}</td>
                                                        <td>
                                                            {{$item->start_date}}
                                                        </td>
                                                        <td>
                                                            {{$item->end_date}}
                                                        </td>
                                                        <td>
                                                            @if(Auth::guard('admin')->user()->hasPermission('advertisement_read'))
                                                                <a href="{{route('admin.advertisements.show',$item->id)}}" class="btn btn-info btn-sm">@lang('user.show')</a>
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.show')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('advertisement_delete'))
                                                                <form action="{{route('admin.advertisements.destroy',$item->id)}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
