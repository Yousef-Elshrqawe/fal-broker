<?php $page="admin.special_advertisements";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
                            <div class="crms-title row bg-white">
                                    <h3 class="page-title m-0">
                                      {{--  @if(Auth::guard('admin')->user()->hasPermission('special_ads_create'))  --}}
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" id="add-task" data-toggle="modal" data-target="#add_task">@lang('user.add')</button>
                                      {{--  @else  --}}
                                        {{--  <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" disabled>@lang('user.add')</button>  --}}
                                      {{--  @endif  --}}
                            </div>
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.special_advertisements')
                                        <small>{{ $total }}</small>
                                    </h3>
									{{-- <p class="card-text">
                                        <form action="{{route('admin.advertisement-purposes.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                </div>
                                            </div>
                                        </form>
                                    </p> --}}
								</div>

								<div class="card-body">
                                    @include('partials._errors')
                                    @include('partials._session')
									<div class="table-responsive">
                                        @if($specialAds->count() > 0)
                                            <table class="datatable table table-stripped mb-0 datatables">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th>@lang('user.cat_name')</th>
                                                        <th>@lang('user.expiration')</th>
                                                        <th>@lang('user.price')</th>
                                                        <th>@lang('user.operations')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($specialAds as $index=>$item)
                                                        <tr>
                                                            <th scope="row">{{ $index +1 }}</th>
                                                            <td>
                                                                {{$item->category->name_ar}}
                                                            </td>
                                                            <td>
                                                                {{$item->expiration}}
                                                            </td>
                                                            <td>
                                                                {{$item->price}}
                                                            </td>
                                                            <td>
                                                                {{--  @if(Auth::guard('admin')->user()->hasPermission('special_ads_update'))  --}}
                                                                    <a class="modal-effect btn btn-info btn-sm" data-effect="effect-scale" id="edit-task" data-toggle="modal" data-target="#edit_task"
                                                                            data-id="{{ $item->id }}" data-expiration="{{ $item->expiration }}"
                                                                            data-price="{{ $item->price }}" data-cat_id="{{ $item->cat_id }}">
                                                                        @lang('user.edit')
                                                                    </a>
                                                                {{--  @else  --}}
                                                                    {{--  <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>  --}}
                                                                {{--  @endif  --}}

                                                                {{--  @if(Auth::guard('admin')->user()->hasPermission('special_ads_delete'))  --}}
                                                                    <form action="{{route('admin.special-advertisements.destroy',$item->id)}}" method="POST"
                                                                        style="display:inline-block">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                    </form> <!--end of form -->
                                                                {{--  @else  --}}
                                                                    {{--  <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>  --}}
                                                                {{--  @endif  --}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>@lang('user.no_data_found')</h2>
                                        @endif
									</div>
								</div>
							</div>
						</div>
					</div>
                    @include('dashboard.special_advertisements.create')
                    @include('dashboard.special_advertisements.edit')
				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
@section('scripts')
<script>
    //Edit Catgeoy
    $('#edit_task').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var expiration = button.data('expiration')
        var price = button.data('price')
        var cat_id = button.data('cat_id')
        var modal = $(this)
        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #expiration').val(expiration);
        modal.find('.modal-body #price').val(price);
        modal.find('.modal-body #cat_id').val(cat_id);
    })
</script>
@endsection
