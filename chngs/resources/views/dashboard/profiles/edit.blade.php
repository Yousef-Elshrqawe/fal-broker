@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
    <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="crms-title row bg-white">
                    <div class="col  p-0">
                        <h3 class="page-title m-0">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                          <i class="fa fa-cog" aria-hidden="true"></i>
                        </span> @lang('user.editProfile') </h3>
                    </div>
                </div>


                <div class="row pt-4">
                    <div class="col-md-12">
                        <div class="card mb-0">
                            <div class="card-body">
                                @include('partials._errors')
                                @include('partials._session')
                                <form action="{{route('admin.profiles.update')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>@lang('user.first_name')</label>
                                            <input class="form-control" type="text" name="first_name" value="{{ $admin->first_name}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>@lang('user.last_name')</label>
                                            <input class="form-control" name="last_name" value="{{ $admin->last_name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>@lang('user.username')</label>
                                            <input class="form-control" name="username" value="{{ $admin->username}}" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>@lang('user.email')</label>
                                            <input class="form-control" name="email" value="{{ $admin->email}}" type="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3p">
                                        <label>@lang('user.image')</label>
                                        <input type="file" name="image" class="form-control image" >
                                    </div>
                                </div>
                                <div class="form-row mt-2">
                                    <div class="col-md-6 mb-3">
                                        <img src="{{ $admin->image_path }}"
                                            class="img-thumbnail image-preview" style="width: 100px;">
                                    </div>
                                </div>
                                <div class="submit-section">
                                    <button class="btn btn-primary submit-btn">Save</button>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
    <!--theme settings modal-->

@endsection
