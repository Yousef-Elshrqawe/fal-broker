<?php $page="admin.report";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body">
                            <div class="table-responsive">
                                <label style="font-syle:bold; font-size:14px">@lang('user.report')</label>
                                <p class="card-text">
                                    <form action="{{route('admin.report.index')}}" method="get">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="from">@lang('user.from')</label>
                                                <input id="from" type="date" name="from" class="form-control" value="{{ request()->from }}"
                                                    placeholder="@lang('user.from')">
                                            </div>
                                            <div class="col-md-2">
                                                <label for="to">@lang('user.to')</label>
                                                <input id="to" type="date" name="to" class="form-control" value="{{ request()->to }}"
                                                    placeholder="@lang('user.to')">
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                            </div>
                                        </div>
                                    </form>
                                </p>
                            </div>

                                <table class="datatable table table-stripped mb-0 datatables table-bordered">
                                    <tr>
                                        <th>@lang('user.total_users_subscriptions')</th>
                                        <td>{{$totalUsersSubscriptions}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.total_ads_earnings')</th>
                                        <td>{{$totalAdsEarnings}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.total_used_coupons')</th>
                                        <td>{{$usedCoupons->count()}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.total_coupons_discount')</th>
                                        <td>{{$totalCouponsDiscount}}</td>
                                    </tr>
                                    <tr class="font-weight-bold">
                                        <th>@lang('user.final_total_earnings')</th>
                                        <td>{{$finalTotalEarnings}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        
        


            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
