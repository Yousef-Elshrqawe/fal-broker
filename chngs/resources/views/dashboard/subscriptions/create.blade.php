<?php $page="admin.subscriptions"?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.subscription')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.subscriptions.store')}}" method="POST" autocomplete="off">
                                @csrf
                                {{-- <h4 class="card-title">@lang('user.add_new_slider')</h4> --}}
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.subscription_name_ar')</label>
                                            <input type="text" name="name_ar" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.subscription_name_en')</label>
                                            <input type="text" name="name_en" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.ads_number')</label>
                                            <input type="number" name="ads_number" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.price')</label>
                                            <input type="number" step="0.01" name="price" class="form-control" required>
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="col-md-6 mb-4p">
                                            <label>@lang('user.user_type')</label>
                                            {{--  <div class="col-lg-6 col-sm-12 mt-3">  --}}
                                                <select  name="user_type" class="custom-select h-1000 ">
                                                    {{--  <select  name="type" class="custom-select w-100 p-2 rounded">  --}}
                                                    <option value="2" >الكل</option>
                                                    <option value="0" >فرد</option>
                                                    <option value="1" >تاجر</option>
                                                </select>
                                            {{--  </div>  --}}
                                        </div>
                                    </div><br>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.add')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




<!-- Modal -->
 {{-- <div class="modal fade" id="add_task" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close md-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title text-center">@lang('user.cat_name')</h4>
                <button type="button" class="close xs-close" data-dismiss="modal">×</button>
              </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('admin.slider.store')}}" method="POST" autocomplete="off">
                            @csrf
                            
                            <div class="form-group row">
                                <div class="col-sm-6">
                                     <label class="col-form-label">@lang('user.cat_name') <span class="text-danger">*</span></label>
                                    <select name="cat_id" id="" class="form-control">
                                        <option value="" selected disabled>@lang('user.cat_name') </option>
                                        @foreach ($cats as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="text-center py-3">
                                <button type="submit" class="border-0 btn btn-primary btn-gradient-primary btn-rounded">@lang('user.add')</button>&nbsp;&nbsp;
                                
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div> --}}
<!-- modal -->
