<?php $page="admin.funds";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">


                <div class="row">
                    <div class="col-md-6">{{-- advertisement Details --}}
                        <div class="card-body">
                            <div class="table-responsive">
                                <label style="font-syle:bold; font-size:14px">@lang('user.fund_details')</label>

                                @if ($fund->user->type == '0')
                                    <a href="{{route('admin.customer.show',$fund->user->id)}}" class="btn btn-info btn-sm" target="_blank">@lang('user.More_details')</a>
                                @else
                                    <a href="{{route('admin.merchant.show',$fund->user->id)}}" class="btn btn-info btn-sm" target="_blank">@lang('user.More_details')</a>
                                @endif

                                <table class="datatable table table-stripped mb-0 datatables table-bordered">
                                    <tr>
                                        <th>#</th>
                                        <td>{{$fund->id}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.customerName')</th>
                                        <td>{{$fund->user->name ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.phone')</th>
                                        <td>{{$fund->user->phone?? ''}} ({{ $fund->user->country_code?? '' }})</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.user_type')</th>
                                        <td>{{$fund->user->type == '0' ? __('user.an_individual')  :  '' }}
                                            {{$fund->user->type == '1' ? __('user.merchant')  :  '' }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.subscribe')</th>
                                        <td>
                                            @if ($fund->user->subscription)
                                                @if ($fund->user->subscription->remainder_ads > 0)
                                                    @if (app()->getlocale()=='ar')
                                                        {{$fund->user->subscription->subscriptionName->name_ar ?? ''}}
                                                    @else
                                                        {{$fund->user->subscription->subscriptionName->name_en ?? ''}}
                                                    @endif
                                                @endif
                                            @else

                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.fund_type')</th>
                                        <td>{{$fund->fund_type == '0' ? __('user.realestute')  :  '' }}
                                            {{$fund->fund_type == '1' ? __('user.personal')  :  '' }}
                                            {{$fund->fund_type == '2' ? __('user.cars')  :  '' }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.financing_period')</th>
                                        <td>{{$fund->financing_periad}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.birth_date')</th>
                                        <td>{{$fund->birth_date}}</td>
                                    </tr>

                                    <tr>
                                        <th>@lang('user.nationlity')</th>
                                        <td>{{$fund->nationlity}}</td>
                                    </tr>

                                    <tr>
                                        <th>@lang('user.sector')</th>
                                        <td>{{$fund->sector}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.monthly_income')</th>
                                        <td>{{$fund->monthly_income}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.salary_type')</th>
                                        <td>{{$fund->salary_type}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.monthly_commit')</th>
                                        <td>{{$fund->monthly_commit}}</td>
                                    </tr>
                                    @if ($fund->fund_type == '2')
                                    <tr>
                                        <th>@lang('user.car_price')</th>
                                        <td>{{$fund->car_price}}</td>
                                    </tr>    
                                    @endif
                                    
                                    <tr>
                                        <th>@lang('user.downpayment')</th>
                                        <td>{{$fund->downpayment}}</td>
                                    </tr>
                                    @if ($fund->fund_type == '0')
                                    <tr>
                                        <th>@lang('user.property_value')</th>
                                        <td>{{$fund->property_value}}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('user.account_type')</th>
                                        <td>{{$fund->account_type}}</td>
                                    </tr>    
                                    @endif
                                    
                                    <tr>
                                        <th>@lang('user.created_at')</th>
                                        <td>{{$fund->created_at}} ( {{$fund->created_at->diffForHumans()?? ''}} )</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>




            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
