 <!-- Modal -->
 <div class="modal fade" id="add_task" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close md-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title text-center">@lang('user.advertisement_types')</h4>
                <button type="button" class="close xs-close" data-dismiss="modal">×</button>
              </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('admin.advertisement-types.store')}}" method="POST" autocomplete="off">
                            @csrf
                            {{-- <h4>Task Details</h4> --}}
                            <div class="form-group row">
                                <div class="col-sm-6">
                                     <label class="col-form-label">@lang('user.nameAr') <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name_ar" id="country-name-ar" placeholder="@lang('user.nameAr')">
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">@lang('user.nameEn') <span class="text-danger">*</span></label>
                                   <input class="form-control" type="text" name="name_en" id="country-name-en" placeholder="@lang('user.nameEn')">
                               </div>
                            </div>
                            <div class="text-center py-3">
                                <button type="submit" class="border-0 btn btn-primary btn-gradient-primary btn-rounded">@lang('user.add')</button>&nbsp;&nbsp;
                                {{-- <button type="button" class="btn btn-secondary btn-rounded">Cancel</button> --}}
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
<!-- modal -->
