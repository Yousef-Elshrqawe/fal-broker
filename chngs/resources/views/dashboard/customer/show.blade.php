<?php $page="admin.customer";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="crms-title row bg-white">
                    <div class="col  p-0">
                        <h3 class="page-title m-0">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                          <i class="feather-user"></i>
                        </span>
                        <a href="{{route('admin.customer.index')}}">
                            @lang('user.customer') </h3>
                        </a>
                    </div>
                </div>

                <!-- Page Header -->
                <div class="page-header pt-3 mb-0">
                    <div class="card ">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-view" style="font-size: 16px">
                                    <div class="profile-img-wrap">
                                        <div class="profile-img">
                                            <a href="{{route('admin.customer.show',$customer->id)}}"><img alt="" src={{$customer->image}}></a>
                                        </div>
                                    </div>
                                    <div class="profile-basic">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="profile-info-left">
                                                    <h3 class="user-name m-t-0 mb-0">{{$customer->name}}</h3><br>
                                                    <h3 class="user-name m-t-0 mb-0"> @lang('user.avg_rate') : {{number_format($customer->avg_rate, 2, '.', '')}}</h3>
                                                    <h3 class="user-name m-t-0 mb-0"> @lang('user.count_rate') : {{$customer->count_rate}} </h3> <br>
                                                    
                                                    @if($customer->view_rate == '1')
                                                        <h3> <a href="{{route('admin.merchant.changeViewRate', ['id'=> $customer->id])}}"
                                                                class="btn btn-outline-danger box-shadow-6 mr-1 ">@lang('user.Not_view_rate')</a></h3>
                                                    @else
                                                       <h3> <a href="{{route('admin.merchant.changeViewRate', ['id'=> $customer->id])}}"
                                                                class="btn btn-outline-primary box-shadow-6 mr-2 "> @lang('user.view_rate') </a> </h3>
                                                    @endif
                                                    {{-- <div class="staff-id">
                                                        @if ($provider->sub->status == 0)
                                                            @lang('user.active')
                                                        @else
                                                            @lang('user.disactive')
                                                        @endif
                                                    </div> --}}
                                                    {{-- <div class="small doj text-muted">{{$driver->balance}}</div> --}}
                                                    {{-- @if ($rating)
                                                        <div class="staff-msg" style="color: royalblue"> @lang('user.rate') : {{round($rating,2)}}</div>
                                                    @else
                                                    <div class="staff-msg" style="color: royalblue"> @lang('user.rate') : 0 </div>
                                                    @endif --}}
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <ul class="personal-info">
                                                    <li>
                                                        <div class="title">@lang('user.phone'):</div>
                                                        <div class="text">{{$customer->phone}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="title">@lang('user.country_code'):</div>
                                                        <div class="text">{{$customer->country_code}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="title">@lang('user.email'):</div>
                                                        <div class="text">{{$customer->email}}</div>
                                                    </li>

                                                    <li>
                                                        <div class="title">@lang('user.address')</div>
                                                        <div class="text">{{$customer->address?? ''}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="title">@lang('user.number_identity'):</div>
                                                        <div class="text">{{$customer->number_identity?? ''}}</div>
                                                    </li>
                                                    <br>
                                                    <li>
                                                        <div class="title">@lang('user.subscribe')</div>
                                                        @if ($customer->subscription)

                                                            @if (app()->getlocale()=='ar')
                                                                <div class="text">{{$customer->subscription->subscriptionName->name_ar}}</div>
                                                            @else
                                                                <div class="text">{{$customer->subscription->subscriptionName->name_en}}</div>
                                                            @endif

                                                            <div class="title">@lang('user.remainder_ads')</div>
                                                            <div class="text">{{$customer->subscription->remainder_ads}}</div>
                                                        @else

                                                        @endif
                                                    </li>
                                                    <br>
                                                    <li>

                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="pro-edit"><a data-target="#profile_info" data-toggle="modal" class="edit-icon" href="#"><i class="fa fa-pencil"></i></a></div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-content p-0">

                </div>


                </div>
                <!-- /Page Header -->


            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
