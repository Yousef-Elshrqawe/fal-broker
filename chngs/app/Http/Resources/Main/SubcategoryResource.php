<?php

namespace App\Http\Resources\Main;

use Attribute;
use Illuminate\Http\Resources\Json\JsonResource;

class SubcategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return attributes of each subcat
        $attributes = $this->attributes;
        foreach($attributes as $attribute){
            if($attribute->icon != null)
                $attribute->icon = $attribute->icon_path;
                
            if(app()->getLocale() == 'ar')
                $attribute->name = $attribute->name_ar;
            else
                $attribute->name = $attribute->name_en;

            $attribute->subattributes  = $attribute->subattributes;

            foreach($attribute->subattributes as $subattribute){
                if(app()->getLocale() == 'ar')
                    $subattribute->name = $subattribute->name_ar;
                else
                    $subattribute->name = $subattribute->name_en;
            }
        }

        return [
            'id'        =>$this->id,
            'name'      =>$this->name,
            'cat_id'    =>$this->cat_id,
            'image'     =>$this->image_path,
            'attributes'=>$attributes
        ];
    }
}
