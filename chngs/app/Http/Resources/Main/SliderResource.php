<?php

namespace App\Http\Resources\Main;

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $desc = '';
        $desc_lang = 'desc_'.app()->getLocale();
        $desc = $this->$desc_lang;

        $name = 'name_'.app()->getLocale();
        $subcatName = '';
        $subcat = Subcategory::find($this->subcat_id);
        if($subcat){
            $subcatName = $subcat->$name;
        }

        $catName = '';
        $cat = Category::find($this->cat_id);
        if($cat){
            $catName = $cat->$name;
        }

        return [
            'id'        =>$this->id,
            'cat_id'    =>$this->cat_id,
            'subcat_id' =>$this->subcat_id,
            'image'     =>$this->image_path,
            'desc'      =>$desc,
            'link'      =>$this->link,
            'cat_name'  =>$catName,
            'subcat_name'=>$subcatName
        ];
    }
}
