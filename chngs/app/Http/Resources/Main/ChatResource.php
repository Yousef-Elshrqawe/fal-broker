<?php

namespace App\Http\Resources\Main;

use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return image and name of user who chat with current user 
        if($request->user_id == $this->sender_id){
            $user = User::find($this->receiver_id);
        }
        else{
            $user = User::find($this->sender_id);
        }

        //return last message
        $chat = Chat::find($this->id);
        $last_message = $chat->messages->last();
        $last_message = MessageResource::make($last_message);

        //return all messages in this chat
        //$messages = MessageResource::collection($chat->messages);

        return [
            'id'            => $this->id,
            'user_name'     => $user->name,
            'user_id'       => $user->id,
            // 'sender_id'     => $this->sender_id,
            // 'receiver_id'   => $this->receiver_id,
            'user_image'        => $user->image_path,
            'user_status'       => $user->active,
            'last_message'  => $last_message,
            //'messages'      => $messages,
        ];
    }
}
