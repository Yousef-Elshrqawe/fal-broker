<?php

namespace App\Http\Resources\Main;

use App\Models\AdvertisementAttribute;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
        return [

            'id'      => $this->id,
            'user_id' => $this->user_id,
            'user_name' => $this->user->name,
            'user_type' => $this->user->type,
            'rate_user_id' => $this->rate_user_id,
            'advertisement_id' => $this->ad_id,
            'rate'      => number_format($this->rate, 2, '.', ''),
            'comment'   => $this->comment,
            'created_at' => $this->created_at,
            
        ];

    } // end of toArray

} // end of php ReviewResource
