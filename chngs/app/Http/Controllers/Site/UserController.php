<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\Advertisement;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UsersSubscription;
// use Dotenv\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator as Validator;
use App\Traits\backendTraits;

class UserController extends Controller
{
    use backendTraits;
    public function login(LoginRequest $request)
    {
        $credentials = $request->getCredentials();
        if (!Auth::validate($credentials)) {
            return redirect()->to('en/site')
                ->with('error', 'Wrong Password or user is not approved yet');
        }

        $user = Auth::getProvider()->retrieveByCredentials($credentials);
        Auth::login($user);
        return $this->authenticated($request, $user);
    }
    public function logout()
    {
        Session::flush();

        Auth::logout();

        return redirect('en/site');
    }
    protected function authenticated(Request $request, $user)
    {
        return redirect()->back();
    }
    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password'  => 'required',
            'password'   => 'required|min:6|max:20',
        ]);
        $old_password = $request->old_password;
        $user = User::find(Auth::user()->id);
        if (Hash::check($old_password, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();
        }
        return redirect()->back();
    }
    public function updateData(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $validator = Validator::make(
            $request->all(),
            [
                'name'          => ['required', 'string', 'min:3', 'max:20'],
                'email'         => 'required|unique:users,email,' . $user->id,
                'phone'         => 'required|unique:users,phone,' . $user->id,
                'governorate_id'    => 'required|exists:governorates,id',
                'address'           => 'required|string',
                'image'             => 'nullable|image',
                //'country_id'    => 'required',
            ],
            [
                'name.required'           => __('user.req_username'),
                'email.required'          => __('user.reqEmail'),
                'email.unique'            => __('user.unique_email'),
                'address.required'        => __('user.reqAddress'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $data = $request->except(['image']);

        // If image request
        $old_image = $user->image;
        if ($request->hasFile('image')) {
            if ($old_image != "default.png") {
                Storage::disk('uploads')->delete('customers_images/' . $old_image);
            }
            $data['image'] = $this->upploadImage($request->image, 'assets/img/users/');
        } else {
            $user->image  = $old_image;
        }
        $user->update($data);
        return redirect()->back();
    }
    //***********************************Login System ******************************************************/
    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'          => ['required', 'string', 'min:3', 'max:70'],
                'type'          => 'required|in:0,1',
                'phone'         => 'required|unique:users',
                'country_code'  => 'required',
                'email'         => 'required|email|unique:users',
                'password'      => 'required|min:6',
                'subscription_id'   => 'nullable|exists:subscriptions,id',
                'governorate_id'    => 'required|exists:governorates,id',
                'address'           => 'required|string',
                'lat'               => 'required',
                'lng'               => 'required',
                'number_identity'   => 'nullable|min:3',
                'image_identity'    => 'nullable|image',
                'commercial_registration_no' => 'nullable|min:3',
                'commercial_register'  => 'nullable|image',
                'payment_method'   => 'nullable|in:0,1',
                'bank_id'          => 'nullable|exists:bank_accounts,id',
                'bank_transfer'    => 'nullable|image',
                // 'coupon_id'=>'nullable|exists:coupons,id',
            ],
            [
                'name.required'           => __('user.req_username'),
                'phone.required'          => __('user.reqPhone'),
                'phone.unique'            => __('user.uniquePhone'),
                'email.required'          => __('user.reqEmail'),
                'email.unique'            => __('user.unique_email'),
                'password.required'       => __("user.reqPassword"),
                'country_id.required'     => __('user.reqCountry'),
                'address.required'        =>  __('user.reqAddress'),
                'idImage.required'        =>  __('user.reqImageID'),
                'lat.required'         =>  __('user.latitude'),
            ]
        );


        $data = $request->except(['password', 'subscription_id', 'image_identity', 'commercial_register', 'bank_transfer', 'bank_id', 'payment_method']);
        $data['password']  = bcrypt($request->password);

        $user = User::create($data);
        // save image user
        if ($request->hasFile('image_identity')) {
            $user->image_identity = $this->upploadImage($request->File('image_identity'), 'assets/img/users/');
            $user->save();
        }
        if ($request->hasFile('commercial_register')) {
            $user->commercial_register = $this->upploadImage($request->File('commercial_register'), 'assets/img/users/');
            $user->save();
        } // end of upload photo

        if ($user->type == 0) {
            $user->active = 1;
            $user->save();
        }

        // Subscription
        if ($request->subscription_id) {
            $subscription = Subscription::find($request->subscription_id);
            $userSubscription = UsersSubscription::create([
                'user_id'           => $user->id,
                'subscription_id'   => $subscription->id,
                'remainder_ads'     => $subscription->ads_number,
                'payment_method'    => $request->payment_method,
            ]);
            // $user->user_subscription =  $UsersSubscription;
            // save image user subscription
            if ($request->hasFile('bank_transfer')) {
                $userSubscription->bank_transfer = $this->upploadImage($request->File('bank_transfer'), 'assets/img/users/');
                $userSubscription->bank_id = $request->bank_id;
                $user->save();
            }
        } // end of Subscription

        $token = NULL;
        $credentials = request(['phone', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user->token =  $token;
        return redirect()->back()->with('success', "Account successfully registered.");
    } // end of register

    public function sendOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone'          => 'required',
            'country_code'   => 'required',
        ], [
            'phone.required' => __("user.reqPhone"),
        ]);
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        //   return "$request->country_code$request->phone";
        //header("Content-Type: text/html; charset=utf-8");
        $code = rand(1111, 9999);
        $messageContent = __('user.your_verify_code') . " $code";
        // $app_id = "api key";
        $app_id = "jEQAuWYrR6UndwAl7OVahUdVcaoGeIiTjS3KYZMe";
        // $app_sec = "api secret";
        $app_sec = "gRzEEXXfP5vDafGhIwUWArOTKKZmtrWt1KsB2McrxekeQJmwaj6TONlcHSeGWmDM2wlvZkv1A9yijb6ssIJbFOevsGVJMfBOH73N";
        $app_hash = base64_encode("{$app_id}:{$app_sec}");

        // country_code
        $messages = [

            "messages" => [
                [
                    "text" => $messageContent,
                    "numbers" => ["$request->country_code$request->phone"],
                    "sender" => "KHIARK"
                ]
            ]
        ];

        $url = "https://api-sms.4jawaly.com/api/v1/account/area/sms/send";
        $headers = [
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic {$app_hash}"
        ];

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($messages));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $response_json = json_decode($response, true);

        if ($status_code == 200) {
            if (isset($response_json["messages"][0]["err_text"])) {
                return  $response_json["messages"][0]["err_text"];
            } else {
                return "تم الارسال بنجاح  " . " job id:" . $response_json["job_id"];
            }
        } elseif ($status_code == 400) {
            return $response_json["message"];
        } elseif ($status_code == 422) {
            return "نص الرسالة فارغ";
        } else {
            return "محظور بواسطة كلاودفلير. Status code: {$status_code}";
        }
    }
}
