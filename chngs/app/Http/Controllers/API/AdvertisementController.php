<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Main\AdvertisementResource;
use App\Http\Resources\Main\ReviewResource;
use App\Http\Resources\Main\SubcategoryResource;
use App\Models\{Advertisement, SpecialAdvertisement, Subscription, UsersSubscription, Reason};
use App\Models\AdvertisementAttribute;
use App\Models\AdvertisementExpiration;
use App\Models\AdvertisementFee;
use App\Models\AdvertisementImages;
use App\Models\AdvertisementType;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Subattribute;
use App\Models\Subcategory;
use App\Models\User;
use App\Models\UserCoupon;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\backendTraits;
use Illuminate\Support\Facades\Storage;

class AdvertisementController extends BaseController
{
    use backendTraits;

    public function getAdvertisementTypes(){
        $advertisementTypes = AdvertisementType::select('id','name_'.app()->getLocale().' AS name')->get();
        return $this->returnData('advertisement_types',$advertisementTypes);
    }


    //return advertisement count_image
    public function getCountImageAdvertisement(){
        $count_image = AdvertisementExpiration::first()->count_image;
        return $this->returnData('count_image_advertisement',$count_image);
    } // end of getCountImageAdvertisement

    public function createAdvertisement(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
                'subcat_id' => 'required|exists:subcategories,id',
                'title'     =>'required',
                'payment_way'   =>'nullable',
                'payment_method'   => 'nullable|in:0,1',
                'price'     =>'required|numeric|min:1',
                'type'      =>'required|exists:advertisement_types,id',
                'address'   =>'required',
                'lat'       =>'required',
                'lng'       =>'required',
                'ad_images' =>'array',
                'ad_details'=>'required|array',
                'country_id'=>'nullable|exists:countries,id',
                'special_ad_id'=>'nullable|exists:special_advertisements,id',
                'subscription_id'   => 'nullable|exists:subscriptions,id',
                'govern_id'=>'nullable|exists:governorates,id',
                'district_id'=>'nullable|exists:districts,id',
                'coupon_id'=>'nullable|exists:coupons,id',

            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if ($user) {
            // update or create UsersSubscription (subscription_id)
            if ($request->subscription_id) {
               $subscription = Subscription::find($request->subscription_id);
               $usersSubscription = UsersSubscription::where('user_id', $request->user_id)->first();
                if ($usersSubscription) {
                    // update
                    $usersSubscription->subscription_id = $request->subscription_id;
                    $usersSubscription->remainder_ads = $subscription->ads_number;
                    $usersSubscription->payment_method = $request->payment_method?? null;
                    $usersSubscription->status = 0;
                    $usersSubscription->save();

                } else {
                    $subscription = Subscription::find($request->subscription_id);
                    $userSubscription = UsersSubscription::create([
                        'user_id'           => $user->id,
                        'subscription_id'   => $request->subscription_id,
                        'remainder_ads'     => $subscription->ads_number,
                        'payment_method'   => $request->payment_method?? null,
                    ]);
                }
            } //end of if (update or create UsersSubscription)

            //store Advertisement
            $data = $request->except('ad_images','ad_details','coupon_id', 'subscription_id');
            $data['start_date'] = Carbon::now();
            $data['end_date'] = Carbon::now()->addDay(AdvertisementExpiration::first()->days);

            $userSubscription = $user->subscription;
            if($userSubscription && $userSubscription->remainder_ads > 0){
                $data['is_subscribed'] = 1;
            }

            // check if special ad sent
            if($request->special_ad_id && $request->special_ad_id != null){
                $data['special_ad_start'] = now();    // or  Carbon::now()
            }
            $advertisement = Advertisement::create($data);

            //decrease remainder ads number if user has subscription
            $userSubscription = $user->subscription;
            if($userSubscription){
                if($userSubscription->remainder_ads > 0 ){
                    $userSubscription->remainder_ads -= 1;
                    $userSubscription->save();
                }
            }

            //decrease coupon count_of_use if coupon sent
            if($request->coupon_id && $request->coupon_id != null){
                $coupon = Coupon::find($request->coupon_id);
                if($coupon){
                    $coupon->count_of_use -= 1;
                    $coupon->save();

                    $fees = 0;
                    $ad_fees = AdvertisementFee::first();
                    if($ad_fees){
                        $fees = $ad_fees->fees;
                    }
                    //to know how many times this user used this coupon
                    UserCoupon::create([
                        'user_id'=>     $user->id,
                        'coupon_id'=>   $coupon->id,
                        'discount_value'=> $coupon->percent * $fees /100
                    ]);
                }
            }

            //store Advertisement images
            if ($request->hasFile('ad_images')) {
                foreach ($request->ad_images as $image) {
                    $image_name = $this->upploadImage($image, 'uploads/advertisements_images/');
                    // sleep(1); //to delay image saving beacuse the two images have the same name in ordinary case
                    AdvertisementImages::create([
                        'ad_id'     =>$advertisement->id,
                        'image'     =>$image_name,
                    ]);
                } 
            }

            //store Advertisement details
            if(sizeof($request->ad_details)>0){
                foreach($request->ad_details as $index=>$value){
                    $attribute = Attribute::find($index);
                    if($attribute){
                        if($attribute->type == 0){
                            $subattribute = Subattribute::find($value);
                            if($subattribute){
                                AdvertisementAttribute::create([
                                    'ad_id'         =>$advertisement->id,
                                    'attr_id'       =>$attribute->id,
                                    'subattr_id'    =>$subattribute->id,
                                    'attr_type'     =>$attribute->type,
                                ]);
                            }
                        }else{
                            AdvertisementAttribute::create([
                                'ad_id'         =>$advertisement->id,
                                'attr_id'       =>$attribute->id,
                                'attr_type'     =>$attribute->type,
                                'value'         =>$value
                            ]);
                        }
                    }
                }
            }

            return $this->returnSuccessMessage('Advertisement Successfully Added');
        } else {
            return $this->returnError('success', __("user.usernotexist"));
        }
    } // end of createAdvertisement

    public function updateAdvertisement(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'ad_id'     =>'required|exists:advertisements,id',
                'user_id'   =>'required',
                'title'     =>'required',
                //'payment_way'   =>'required',
                'price'     =>'required|numeric|min:1',
                'type'      =>'required|exists:advertisement_types,id',
                'address'   =>'required',
                'lat'       =>'required',
                'lng'       =>'required',
                'ad_images' =>'array',
                'country_id'=>'required|exists:countries,id',
                'govern_id'=>'nullable|exists:governorates,id',
                'district_id'=>'nullable|exists:districts,id',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        if ($user) {
            //store Advertisement
            $data = $request->except('ad_images','ad_details','ad_id');

            $advertisement = Advertisement::find($request->ad_id);
            if($advertisement){
                $advertisement->update($data);
            }else{
                return $this->returnError('','this ad not found');
            }

            //if new images sent store Advertisement new images and delete old ones
            if ($request->hasFile('ad_images')) {
                $old_images = $advertisement->images;
                if(sizeof($old_images)>0){
                    foreach($old_images as $old_image){
                        Storage::disk('uploads')->delete('advertisements_images/' . $old_image->image);
                        $old_image->delete();
                    }
                }
                foreach ($request->ad_images as $image) {
                    $image_name = $this->upploadImage($image, 'uploads/advertisements_images/');
                    // sleep(1); //to delay image saving beacuse the two images have the same name in ordinary case
                    AdvertisementImages::create([
                        'ad_id'     =>$advertisement->id,
                        'image'     =>$image_name,
                    ]);
                }
            }
            return $this->returnSuccessMessage('Advertisement Successfully Updated');
        } else {
            return $this->returnError('success', __("user.usernotexist"));
        }
    }

    public function ReasonsForRefusal(){
        $reasons = Reason::select("id", "name_" . app()->getLocale() . ' as name')->get();
        return $this->returnData('reasons', $reasons);
    } // end of ReasonsForRefusal

    public function cancelAdvertisement(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'ad_id'   => 'required|exists:advertisements,id',
                'reason_id'  => 'required|exists:reasons,id',
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $advertisement = Advertisement::find($request->ad_id);
        $advertisement->canceled  = 1;
        $advertisement->reason_id = $request->reason_id;
        $advertisement->finished  = 1;
        $advertisement->save();

        return $this->returnSuccessMessage('Advertisement successfully canceled');
    } // end of cancelAdvertisement


    public function deleteAdvertisement(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'ad_id'   =>'required|exists:advertisements,id'
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $advertisement = Advertisement::where('user_id',$request->user_id)
        ->where('id',$request->ad_id)->first();
        if($advertisement){
            //delete Advertisement Images
            $advertisementImages = AdvertisementImages::select('id','image')->where('ad_id',$advertisement->id)->get();
            foreach($advertisementImages as $advertisementImage){
                Storage::disk('uploads')->delete('advertisements_images/' . $advertisementImage->image);
            }

            //delete Advertisement
            $deleted = $advertisement->delete();
            if($deleted){
                return $this->returnSuccessMessage('Advertisement successfully deleted');
            }else{
                return $this->returnError(null , 'somthing wentwrong!');
            }
        }else{
            return $this->returnSuccessMessage('Advertisement not found');
        }
    }

    //return Advertisements latest
    public function getAdsLatest(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'=>'nullable|exists:users,id'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }
        $advertisements = Advertisement::where('finished',0)->orderBy('id', 'DESC')->limit(10)->get();
        // $advertisements = Advertisement::latest()->take(10)->get(); // or
        $advertisements = AdvertisementResource::collection($advertisements);

        return $this->returnData('advertisements' , $advertisements);
    } // end of getAdsLatest


    //return Advertisements popular
    public function getAdsPopular(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'=>'nullable|exists:users,id'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }
        $advertisements = Advertisement::where(['popular'=> 1, 'finished' => 0])->get();
        $advertisements = AdvertisementResource::collection($advertisements);

        return $this->returnData('advertisements' , $advertisements);
    } // end of getAdsPopular


    //Special Ads
    public function getSpecialAdvertisements(){
        $specialAds = Advertisement::where('special_ad_id','!=',null)
            ->where('finished' , 0 )->get();

        $selectedAds = [];
        foreach($specialAds as $specialAd){
            $special = SpecialAdvertisement::select('id','expiration','price')->find($specialAd->special_ad_id);
            if($special){
                $special_ad_end = Carbon::parse($specialAd->special_ad_start)->addDays($special->expiration);
                if($special_ad_end >= now())
                    $selectedAds[] = $specialAd;
            }
        }

        $selectedAds = AdvertisementResource::collection($selectedAds);

        return $this->returnData('special_ads',$selectedAds);
    } // end of getSpecialAdvertisements



    //get special ads
    public function getSpecialAds(Request $request){
        if(isset($request->cat_id)){
            // return $request;
            $specialAds = SpecialAdvertisement::where('cat_id', $request->cat_id)->select('id','expiration','price','cat_id')->get();
        }else{
            $specialAds = SpecialAdvertisement::select('id','expiration','price','cat_id')->get();
        }
        return $this->returnData('special_ads',$specialAds);
    } // end of getSpecialAds

    //get special ads
    // public function getSpecialAds($cat_id){
    //     // $validator = Validator::make(
    //     //     $request->all(),
    //     //     [
    //     //         'cat_id'      =>'required|exists:Categories,id',
    //     //     ],
    //     //     []
    //     // );
    //     // if ($validator->fails()) {
    //     //     $code = $this->returnCodeAccordingToInput($validator);
    //     //     return $this->returnValidationError($code, $validator);
    //     // }
    //     $category = Category::find($cat_id);
    //     if ( ! $category ){
    //         return $this->returnError('','cat id not font');
    //     }

    //     $specialAds = SpecialAdvertisement::select('id','expiration','price','cat_id')->where('cat_id', $cat_id)->get();
    //     return $this->returnData('special_ads',$specialAds);
    // } // end of getSpecialAds


    //return Advertisements by cat
    public function getAdsByCat(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'cat_id'=>'required|exists:categories,id'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $subcatIds = [];
        $subcategories = Category::find($request->cat_id)->subcategories;
        foreach($subcategories as $subcategory){
            $subcatIds[] = $subcategory->id;
        }

        $advertisements = Advertisement::where('finished',0)->whereIn('subcat_id',$subcatIds)->get();
        $advertisements = AdvertisementResource::collection($advertisements);

        return $this->returnData('advertisements' , $advertisements);
    }

    //return Advertisements by subcat
    public function getAdsBySubcat(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'subcat_id'=>'required|exists:subcategories,id'
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $advertisements = Advertisement::where('finished',0)->where('subcat_id',$request->subcat_id)->get();
        $advertisements = AdvertisementResource::collection($advertisements);

        return $this->returnData('advertisements' , $advertisements);
    }

    public function getAdsById(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'ad_id'=>'required|exists:advertisements,id'
            ],
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $advertisement = Advertisement::find($request->ad_id);

        $advertisement = AdvertisementResource::make($advertisement);
        return $this->returnData('advertisement' , $advertisement);
    }

    public function myAdvertisements(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        $advertisements = AdvertisementResource::collection($user->advertisements);
        return $this->returnData('advertisements',$advertisements);
    }

    public function search(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
               // 'query' => 'required',
            ],
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);

        }
        $advertisements = Advertisement::where('title','like','%' . $request->search . '%')->get();
        $advertisements = AdvertisementResource::collection($advertisements);

        return $this->returnData('advertisements',$advertisements);
    }

    //filter ADs by attributes
    public function filterAds(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'cat_id'          =>'required|exists:categories,id',
                'subcat_id'       =>'exists:subcategories,id',
                'from'            =>'nullable|min:0',
                'to'              =>'nullable',
                'lat'             =>'nullable',
                'lng'             =>'nullable',
                'country_id'      =>'exists:countries,id',
                'govern_id'       =>'exists:governorates,id',
                'district_id'     =>'exists:districts,id',
                'type'            =>'exists:advertisement_types,id'
            ]
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        if($request->subcat_id){
            $advertisements = Advertisement::with(['attributes', 'rates'])->where(['subcat_id'=>$request->subcat_id ,'finished'=>0])
            ->whereBetween('price' , [$request->from!=''? $request->from : 0 , $request->to!=''? $request->to : random_int(9999999999,99999999999)])->get();
        }else{
            $subcatsIds = [];
            $subcats = Subcategory::select('id')->where('cat_id',$request->cat_id)->get();
            foreach($subcats as $subcat){
                $subcatsIds[] = $subcat->id;
            }

            $advertisements = Advertisement::with(['attributes', 'rates'])->whereIn('subcat_id',$subcatsIds)->where(['finished'=>0])
            ->whereBetween('price' , [$request->from!=''? $request->from : 0 , $request->to!=''? $request->to : random_int(9999999999,99999999999)])->get();
        }

        // if($request->lat){
        //     $lat = $request->lat;
        //     $lng = $request->lng;
        //     // $distance = 2;
        //     // $haversine = "(
        //     //     6371 * acos(
        //     //         cos(radians(" .$latitude. "))
        //     //         * cos(radians(`latitude`))
        //     //         * cos(radians(`longitude`) - radians(" .$longitude. "))
        //     //         + sin(radians(" .$latitude. ")) * sin(radians(`latitude`))
        //     //     )
        //     // )";

        //     // $cities = City::select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
        //     // ->having('distance', '<', 25)
        //     // ->orderBy('distance')
        //     // ->get();

        //     // $users = User::select("id")
        //     //     ->selectRaw("$haversine AS distance")
        //     //     ->having("distance", "<=", $distance)
        //     //     ->orderby("distance", "desc")
        //     //     ->limit(5)
        //     //     ->get();
        //     // $advertisements = $advertisements->where(['lat'=> $request->lat, 'lng'=> $request->lng]);
        //     $advertisements = Advertisement::select(DB::raw('*, ( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) ) AS distance'))
        //     ->having('distance', '<', 2)
        //     ->orderBy('distance')
        //     ->get();
        //     return $advertisements;
        // } // end of if

        if($request->type){
            $advertisements = $advertisements->where('type',$request->type);
        }

        if($request->country_id){
            $advertisements = $advertisements->where('country_id',$request->country_id);
        }
        if($request->govern_id){
            $advertisements = $advertisements->where('govern_id',$request->govern_id);
        }
        if($request->district_id){
            $advertisements = $advertisements->where('district_id',$request->district_id);
        }

        if(!$request->ad_details){
            $advertisements = AdvertisementResource::collection($advertisements);
            return $this->returnData('advertisements' , $advertisements);
        }elseif(sizeof($request->ad_details)>0){
            foreach($request->ad_details as $index=>$value){
                if($value == '' || $value==null){
                    $advertisements = AdvertisementResource::collection($advertisements);
                    return $this->returnData('advertisements' , $advertisements);
                }
            }
        }

        //extract filter attributes,subattributes ids and values
        $attrsIds = [];
        $subattrsIds = [];
        $values = [];

        if($request->ad_details){
            if(sizeof($request->ad_details)>0){
                foreach($request->ad_details as $index=>$value){
                    $attribute = Attribute::find($index);
                    if($attribute){
                        $attrsIds[] = $index;
                        if($attribute->type == 0){
                            $subattrsIds[] = $value;
                            $values[] = -1;
                        }
                        elseif($attribute->type == 1 || $attribute->type == 2){
                            $subattrsIds[] = 0;
                            $values[] = $value;
                        }
                    }
                }
            }
        }
        //return $values;
        //loop on ads attributes with filter attributes to get matched ads
        $selectedAds = [];
        $matching_points = 0;
        foreach($advertisements as $advertisement){
            $matching_points = 0;
            foreach($advertisement->attributes as $attribute){
                for($i=0; $i<count($attrsIds); $i++){
                    if($values[$i] == -1){
                        if($attribute->attr_id==$attrsIds[$i] && $attribute->subattr_id==$subattrsIds[$i]){
                            $matching_points++;
                        }
                    }else{
                        if($attribute->attr_id==$attrsIds[$i] && $attribute->value==$values[$i]){
                            $matching_points++;
                        }
                    }
                }
            }
            if($matching_points == count($values)){//equality here means that ad exactly match
                $selectedAds[] = $advertisement;
            }
        }

        $advertisements = AdvertisementResource::collection($selectedAds);
        return $this->returnData('advertisements' , $advertisements);
    }
}
