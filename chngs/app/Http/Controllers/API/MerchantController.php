<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Main\AdvertisementResource;
use App\Http\Resources\Main\UserResource;
use App\Models\Advertisement;
use App\Models\{User, BankAccount};
use App\Models\UserCoupon;
use App\Models\UsersSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\backendTraits;
use Tymon\JWTAuth\Facades\JWTAuth;

class MerchantController extends BaseController
{
    use backendTraits;


    //get All Merchants
    public function getMerchants(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'=>'nullable|exists:users,id',
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $merchants = User::where('type', 1)->get();
        $merchants = UserResource::collection($merchants);

        return $this->returnData('merchants', $merchants);
    } // end of getMerchants

    //return Advertisements by Merchant
    public function getAdsByMerchant(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'=>'required|exists:users,id',
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $advertisements = Advertisement::where('finished',0)->where('user_id',$request->user_id)->get();
        $advertisements = AdvertisementResource::collection($advertisements);

        return $this->returnData('advertisements' , $advertisements);
    } // end of getAdsByMerchant




} // end of MerchantController
