<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Main\DesireResource;
use App\Models\Attribute;
use App\Models\Desire;
use App\Models\DesireAttribute;
use App\Models\Subattribute;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\backendTraits;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class DesireController extends BaseController
{
    use backendTraits;

    public function addDesire(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'subcat_id' => 'required|exists:subcategories,id',
                'from_price'     =>'required|numeric',
                'to_price'     =>'numeric',
                'type'      =>'exists:advertisement_types,id',
                'ad_details'=>'nullable|array',
                'country_id'=>'exists:countries,id',
                'govern_id'=>'exists:governorates,id',
                'district_id'=>'exists:districts,id',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $user = User::find($request->user_id);
        
       
        //store Desire
        $data = $request->except('ad_details');
        $desire = Desire::create($data);

        //store Desire details
        if($request->ad_details && sizeof($request->ad_details)>0){
            foreach($request->ad_details as $index=>$value){
                $attribute = Attribute::find($index);
                if($attribute){
                    if($attribute->type == 0){
                        $subattribute = Subattribute::find($value);
                        if($subattribute){
                            DesireAttribute::create([
                                'desire_id'         =>$desire->id,
                                'attr_id'       =>$attribute->id,
                                'subattr_id'    =>$subattribute->id,
                                'attr_type'     =>$attribute->type,
                            ]);
                        }
                    }else{
                        DesireAttribute::create([
                            'desire_id'         =>$desire->id,
                            'attr_id'       =>$attribute->id,
                            'attr_type'     =>$attribute->type,
                            'value'         =>$value
                        ]);
                    }
                }
            }
        }

        return $this->returnSuccessMessage('Desire Successfully Added');
        
    }

    public function deleteDesire(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'desire_id'   =>'required|exists:desires,id'
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $desire = Desire::where('user_id',$request->user_id)
        ->where('id',$request->desire_id)->first();
        if($desire){

            //delete desire
            $deleted = $desire->delete();
            if($deleted){
                return $this->returnSuccessMessage('Desire successfully deleted');
            }else{
                return $this->returnError(null , 'somthing wentwrong!');
            }
        }else{
            return $this->returnSuccessMessage('Desire not found');
        }
    }

    public function myDesires(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ],
            [
                'user_id.required' => __('user.reqCustomer'),
            ]
        );
        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }
        
        $user = User::find($request->user_id);
        $desires = DesireResource::collection($user->desires);
        return $this->returnData('desires',$desires);
    }
}
