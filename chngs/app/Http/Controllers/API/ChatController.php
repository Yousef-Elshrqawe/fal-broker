<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\NotificationController;
use App\Http\Resources\Main\ChatResource;
use App\Http\Resources\Main\MessageResource;
use App\Models\Chat;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatController extends BaseController
{
    //sendinig message to anothor user
    public function sendMessage(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'sender_id'     => 'required|exists:users,id',
                'receiver_id'   => 'required|exists:users,id',
                'message'       => 'required'
            ],
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        //check if there is a previous chat between sender and receiver
        $chat = Chat::where('sender_id',$request->sender_id)
        ->where('receiver_id' , $request->receiver_id)->first();
        if(!$chat)
            $chat = Chat::where('sender_id',$request->receiver_id)
            ->where('receiver_id' , $request->sender_id)->first();
        if($chat){
            $message = Message::create([
                'sender_id'     =>$request->sender_id,
                'receiver_id'   =>$request->receiver_id,
                'message'       =>$request->message,
                'chat_id'       =>$chat->id
            ]);
            if($message){
                NotificationController::sendMessageNotification($request->receiver_id,
                User::find($request->sender_id)->name, $request->message, $request->sender_id);
                return $this->returnSuccessMessage('message successfully sent');
            }
            else
                return $this->returnError(null , 'something went wrong!');
        }else{
            $chat = Chat::create([
                'sender_id'     =>$request->sender_id,
                'receiver_id'   =>$request->receiver_id,
            ]);

            $message = Message::create([
                'sender_id'     =>$request->sender_id,
                'receiver_id'   =>$request->receiver_id,
                'message'       =>$request->message,
                'chat_id'       =>$chat->id
            ]);
            if($message){
                NotificationController::sendMessageNotification($request->receiver_id,
                User::find($request->sender_id)->name, $request->message, $request->sender_id);
                return $this->returnSuccessMessage('message successfully sent');
            }
            else
                return $this->returnError(null , 'something went wrong!');
        }
    }

    //return all user chats
    public function myChats(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'     => 'required|exists:users,id',
            ],
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $chats = Chat::where('sender_id' , $request->user_id)
        ->orwhere('receiver_id' , $request->user_id)->get();

        $chats = ChatResource::collection($chats);

        return $this->returnData('chats' , $chats);
    }

    //get Messages By Chat id
    public function getMessagesByChat(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'chat_id'     => 'required|exists:chats,id',
            ],
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        $chat = Chat::find($request->chat_id);
        $messages = MessageResource::collection($chat->messages);
        
        return $this->returnData('messages' , $messages);
    }

    public function getMessagesBySenderAndReceiver(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'sender_id'     => 'required|exists:users,id',
                'receiver_id'   => 'required|exists:users,id',
            ],
        );

        if ($validator->fails()) {
            $code = $this->returnCodeAccordingToInput($validator);
            return $this->returnValidationError($code, $validator);
        }

        // return $chat = Chat::where(['sender_id'=>$request->sender_id , 'receiver_id'=>$request->receiver_id])
        // ->orwhere(['receiver_id'=>$request->sender_id , 'sender_id'=>$request->receiver_id])->first();

        $chat = Chat::where('sender_id',$request->sender_id)
        ->where('receiver_id' , $request->receiver_id)->first();
        if(!$chat)
            $chat = Chat::where('sender_id',$request->receiver_id)
            ->where('receiver_id' , $request->sender_id)->first();
        if($chat){
            $messages = MessageResource::collection($chat->messages);
            return $this->returnData('messages' , $messages);
        }

        return $this->returnData('messages' , []);
    }
}
