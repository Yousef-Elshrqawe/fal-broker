<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use LaravelLocalization;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['countries'] = Country::select('id','name_ar','name_en','name_' . LaravelLocalization::getCurrentLocale() . ' as name')->
        when($request->search , function ($q) use ($request){
            return $q->where('name_ar' , 'like' , '%'. $request->search. '%')
                    ->orWhere('name_en' , 'like' , '%'. $request->search. '%');
        })->latest()->get();

        $data['total'] = Country::count();

        return view('dashboard.country.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar'  => 'required|string|unique:countries',
            'name_en'  => 'required|string|unique:countries',
        ],[
            'name_ar.required'    => __('user.reqStatNameAr'),
            'name_ar.unique'      => __('user.uniqueStatNameAr'),
            'name_en.required'    => __('user.reqStatNameEn'),
            'name_en.unique'      => __('user.uniqueStatNameEn'),
        ]
        );
            $data = $request->all();

            Country::create($data);

            session()->flash('success', __('user.added_successfully'));

            return redirect()->route('admin.countries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::find($request->id);
        $request->validate([
            'name_ar'  => ['required','string', Rule::unique('countries')->ignore($country->id)],
            'name_en'  => ['required','string', Rule::unique('countries')->ignore($country->id)],
        ],[
            'name_ar.required'    => __('user.reqStatNameAr'),
            'name_ar.unique'      => __('user.uniqueStatNameAr'),
            'name_en.required'    => __('user.reqStatNameEn'),
            'name_en.unique'      => __('user.uniqueStatNameEn'),
        ]);
        $data = $request->except('_token','_method');

        $country->update($data);
        session()->flash('success', __('user.updated_successfully'));
        return redirect()->route('admin.countries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        $country->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->route('admin.countries.index');
    }
}
