<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Subattribute;
use Illuminate\Http\Request;
use App\Traits\backendTraits;
use Illuminate\Support\Facades\Storage;

class AttributeController extends Controller
{
    use backendTraits;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar'  => 'required|string',
            'name_en'  => 'required|string',
            'type'     =>'required',
        ],[
            'name_ar.required'    => __('user.reqGovernNameAr'),
            'name_ar.unique'      => __('user.uniqueGovernNameAr'),
            'name_en.required'    => __('user.reqGovernNameEn'),
            'name_en.unique'      => __('user.uniqueGovernNameEn'),
        ]
        );
        $data = $request->all();
        $data['subcat_id'] = $request->subcat_id;

        if($request->icon){
            $data['icon'] = $this->upploadImage($request->icon, 'uploads/attributes_images/');
        }
        
        $attr = Attribute::create($data);

        session()->flash('success', __('user.added_successfully'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cat,$subcat,$attribute)
    {
        $data['cat'] = $cat;
        $data['subattributes'] = Subattribute::where('attr_id',$attribute);
        $data['attribute']  = Attribute::find($attribute);

        return view('dashboard.attributes.subattributes.index')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$cat,$subcat, $attribute)
    {
        $attr = Attribute::find($request->id);
        $request->validate([
            'name_ar'  => ['required','string'],
            'name_en'  => ['required','string']
        ],[
            'name_ar.required'    => __('user.reqStatNameAr'),
            'name_ar.unique'      => __('user.uniqueStatNameAr'),
            'name_en.required'    => __('user.reqStatNameEn'),
            'name_en.unique'      => __('user.uniqueStatNameEn'),
        ]);
        $data = $request->except('_token','_method');
    
        if($request->icon){
            if($request->icon != $attr->icon){
                Storage::disk('uploads')->delete('attributes_images/' . $attr->icon);
                $data['icon'] = $this->upploadImage($request->icon, 'uploads/attributes_images/');
            }
        }

        $attr->update($data);
        session()->flash('success', __('user.updated_successfully'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cat, $subcat, $attribute)
    {
        $attr = Attribute::find($attribute);
        if($attr->icon != null){
            Storage::disk('uploads')->delete('attributes_images/' . $attr->icon);
        }
        $attr->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->back();
    }

    public function createSub(Request $request){
        $request->validate([
            'name_ar'  => ['required','string'],
            'name_en'  => ['required','string'],
        ],[
            'name_ar.required'    => __('user.reqStatNameAr'),
            'name_ar.unique'      => __('user.uniqueStatNameAr'),
            'name_en.required'    => __('user.reqStatNameEn'),
            'name_en.unique'      => __('user.uniqueStatNameEn'),
        ]);
        
        $data = $request->except('_token');
        Subattribute::create($data);

        session()->flash('success', __('user.added_successfully'));

        return redirect()->back();

    }

    public function deleteSub($cat ,$subcat, $subattr_id){
        $subattr = Subattribute::find($subattr_id);
        $subattr->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->back();
    }
}
