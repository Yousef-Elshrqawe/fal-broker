<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = Subscription::select('id','name_'.app()->getLocale().' AS name','ads_number','price', 'user_type')->get();
        return view('dashboard.subscriptions.index' , compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.subscriptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name_ar'    => 'required',
                'name_en'    => 'required',
                'ads_number' => 'required',
                'price'      => 'required',
                'user_type'      => 'required',
            ],
        );

        $sub = Subscription::create($request->all());

        if($sub)
            session()->flash('success', __('user.added_successfully'));

        return redirect()->route('admin.subscriptions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription = Subscription::find($id);
        return view('dashboard.subscriptions.edit' , compact('subscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name_ar'    => 'required',
                'name_en'    => 'required',
                'ads_number' => 'required',
                'price'      => 'required',
                'user_type'  => 'required',
            ],
        );

        $sub = Subscription::find($id);
        $updated = $sub->update($request->all());

        if($updated)
            session()->flash('success', __('user.updated_successfully'));
        else
            session()->flash('success','something wentwrong!');

        return redirect()->route('admin.subscriptions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub = Subscription::find($id);
        $deleted = $sub->delete();

        if($deleted)
            session()->flash('success', __('user.deleted_successfully'));
        else
            session()->flash('success','something wentwrong!');

        return redirect()->back();
    }
}
