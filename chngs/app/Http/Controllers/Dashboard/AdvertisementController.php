<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use App\Models\AdvertisementExpiration;
use App\Models\AdvertisementFee;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['advertisements'] = Advertisement::when($request->search , function ($q) use ($request){
            return $q->where('title' , 'like' , '%'. $request->search. '%')
            ->orWhere('start_date' , 'like' , '%'. $request->search. '%')
            ->orWhere('end_date' , 'like' , '%'. $request->search. '%')
            ->orwhere('id','like',$request->search);
        })->latest()->get();

      return view('dashboard.advertisements.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //     $log_column_names = LogColumnsName::where('log_id', $log->id)
        //              ->skip(2)->take(30)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Advertisement::find($id);
        return view('dashboard.advertisements.show' , compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Advertisement::find($id);
        $ad->delete();

        session()->flash('success', __('user.deleted_successfully'));
        return redirect()->back();

    }

    //return advertisement fees and expiration
    public function getFeesExpiration(){
        $fees = AdvertisementFee::first()->fees;
        $days = AdvertisementExpiration::first()->days;
        $count_image = AdvertisementExpiration::first()->count_image;
        return view('dashboard.advertisement_fees_expiration.index' , compact('fees','days', 'count_image'));
    }

    public function updateFeesExpiration(Request $request){
        $this->validate($request, [
            'fees'      =>'required|numeric|min:0',
            'days'      =>'required|numeric|min:1',
            'count_image' =>'required|numeric|min:1',
        ]);

        $advertisementFee = AdvertisementFee::first();
        if($advertisementFee){
            $advertisementFee->fees = $request->fees;
            $advertisementFee->save();
        }
        $advertisementExpiration = AdvertisementExpiration::first();
        if($advertisementExpiration){
            $advertisementExpiration->days = $request->days;
            $advertisementExpiration->count_image = $request->count_image;
            $advertisementExpiration->save();
        }

        session()->flash('success', __('user.updated_successfully'));

        return redirect()->back();
    }
    
}
