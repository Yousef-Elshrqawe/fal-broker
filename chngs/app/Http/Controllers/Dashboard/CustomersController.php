<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['customer'] = User::where('type', 0)->when($request->search , function ($q) use ($request){
            return $q->where('phone' , 'like' , '%'. $request->search. '%')
            ->orWhere('name' , 'like' , '%'. $request->search. '%')
            ->orWhere('email' , 'like' , '%'. $request->search. '%');
        })->latest()->get();

      return view('dashboard.customer.index')->with($data);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['customer'] = User::find($id);

        return view('dashboard.customer.show')->with($data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->image != 'default.png') {
            Storage::disk('uploads')->delete('/customers_images/' . $user->image);
        }

        if ($user->imageId != 'default2.png') {
            Storage::disk('uploads')->delete('/customers_images/' . $user->idImage);
        }

        $deleted = $user->delete();
        if($deleted){
            session()->flash('success', __('user.deleted_successfully'));
        }
        return redirect()->back();
    }

}
