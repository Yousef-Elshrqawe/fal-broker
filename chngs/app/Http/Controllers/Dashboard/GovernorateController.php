<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Governorate;
use Illuminate\Http\Request;

class GovernorateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country_id)
    {
        $governs = Governorate::select('id','name_ar','name_en','name_'.app()->getLocale().' as name',
        'country_id')
        ->where('country_id',$country_id)->get();
        return view('dashboard.country.governorates.index',compact('country_id','governs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar'  => 'required|string|unique:governorates',
            'name_en'  => 'required|string|unique:governorates',
            'country_id'=>'required|exists:countries,id'
        ],[
            'name_ar.required'    => __('user.reqGovernNameAr'),
            'name_ar.unique'      => __('user.uniqueGovernNameAr'),
            'name_en.required'    => __('user.reqGovernNameEn'),
            'name_en.unique'      => __('user.uniqueGovernNameEn'),
        ]
        );
            $data = $request->all();

            $govern = Governorate::create($data);

            session()->flash('success', __('user.added_successfully'));

            return redirect()->route('admin.governorates.index',$govern->country->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_ar'  => 'required|string',
            'name_en'  => 'required|string',
        ],[
            'name_ar.required'    => __('user.reqGovernNameAr'),
            'name_ar.unique'      => __('user.uniqueGovernNameAr'),
            'name_en.required'    => __('user.reqGovernNameEn'),
            'name_en.unique'      => __('user.uniqueGovernNameEn'),
        ]
        );
        $data = $request->except('_token','_method');

        $governorate = Governorate::find($request->id);
        $governorate->update($data);

        session()->flash('success', __('user.updated_successfully'));
        return redirect()->route('admin.governorates.index',$governorate->country->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country ,Governorate $governorate)
    {
        $governorate->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->route('admin.governorates.index',$country->id);
    }
}
