<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AdvertisementType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class AdvertisementTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adTypes = AdvertisementType::select('id','name_ar','name_en',
        'name_'.app()->getLocale().' AS name')->get();
        return view('dashboard.advertisement_types.index' , compact('adTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar'  => 'required|string|min:3|max:20|unique:advertisement_types',
            'name_en'  => 'required|string|min:3|max:20|unique:advertisement_types',
        ],[
            
        ]
        );
            $data = $request->all();

            AdvertisementType::create($data);

            session()->flash('success', __('user.added_successfully'));

            return redirect()->route('admin.advertisement-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = AdvertisementType::find($request->id);
        $request->validate([
            'name_ar'  => ['required','string','min:3','max:20', Rule::unique('advertisement_types')->ignore($type->id)],
            'name_en'  => ['required','string','min:3','max:20', Rule::unique('advertisement_types')->ignore($type->id)],
        ],[
            'name_ar.required'    => __('user.reqStatNameAr'),
            'name_ar.unique'      => __('user.uniqueStatNameAr'),
            'name_en.required'    => __('user.reqStatNameEn'),
            'name_en.unique'      => __('user.uniqueStatNameEn'),
        ]);
        $data = $request->except('_token','_method');

        $type->update($data);
        session()->flash('success', __('user.updated_successfully'));
        return redirect()->route('admin.advertisement-types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = AdvertisementType::find($id);
        $type->delete();
        session()->flash('success',  __('user.deleted_successfully'));
        return redirect()->route('admin.advertisement-types.index');
    }
}
