<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Governorate extends Model
{
    protected $guarded = [];

    public function country(){
        return $this->belongsTo(Country::class , 'country_id');
    }

    public function districts(){
        return $this->hasMany(District::class,'govern_id');
    }

    public function users(){
        return $this->hasMany(User::class , 'country_id');
    }

}
