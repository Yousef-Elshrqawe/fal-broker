<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertisementAttribute extends Model
{
    protected $guarded = [];
}
