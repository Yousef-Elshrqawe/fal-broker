<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategories';
    protected $guarded = [];

    public function attributes(){
        return $this->hasMany(Attribute::class , 'subcat_id');
    }

    public function  getImagePathAttribute()
    {
      return asset('uploads/categories/'. $this->image);
    }// end of get Image Path
}
