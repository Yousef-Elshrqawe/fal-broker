<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subattribute extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at','value','name_ar','name_en'];


    public function attribute(){
        return $this->belongsTo(Attribute::class , 'attr_id');
    }
}
