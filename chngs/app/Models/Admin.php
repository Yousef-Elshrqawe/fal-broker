<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class Admin extends Authenticatable
{
    // Make Class make authentication

    use Notifiable;
    use LaratrustUserTrait;

    protected $fillable = [
        'first_name','last_name','email','username', 'image', 'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function  getImagePathAttribute()
    {
      return asset('uploads/users_images/'. $this->image);
    }// end of get Image Path
}
