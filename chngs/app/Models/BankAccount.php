<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $guarded = [];

    
    // public function  getImagePathAttribute()
    // {
    //   return asset('uploads/bank_account_images/'. $this->image);
    // }// end of get Image Path
    public function  getImageIdentityAttribute($val)
    {
        return ($val !== null) ? asset('assets/img/uploads/bank_account_images/' . $val) : "";
    }
    
    public function usersSubscription(){
        return $this->hasMany(UsersSubscription::class , 'bank_id');
    }

} // end of 
