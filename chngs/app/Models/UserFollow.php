<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFollow extends Model
{
    //
    protected $table = 'user_follows';

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class , 'user_id' , 'id');
    }

    public function follow(){
        return $this->belongsTo(User::class , 'follow_id' , 'id');
    }

}
