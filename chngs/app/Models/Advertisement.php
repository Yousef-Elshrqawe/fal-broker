<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class , 'user_id');
    }

    public function images(){
        return $this->hasMany(AdvertisementImages::class , 'ad_id');
    }

    public function attributes(){
        return $this->hasMany(AdvertisementAttribute::class , 'ad_id');
    }

    public function getType(){
        return $this->belongsTo(AdvertisementType::class , 'type');
    }
    
    public function rates(){
        return $this->hasMany(UserRate::class , 'ad_id');
    }

}
