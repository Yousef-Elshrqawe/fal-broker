<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Desire extends Model
{
    protected $guarded = [];

    public function attributes(){
        return $this->hasMany(DesireAttribute::class , 'desire_id');
    }
}
