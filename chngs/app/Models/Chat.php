<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';
    protected $guarded = [];

    public function messages(){
        return $this->hasMany(Message::class , 'chat_id');
    }
    
}
