<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRate extends Model
{
    //
    protected $table = 'user_rates';

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class , 'user_id' , 'id');
    }

    public function rate_user(){
        return $this->belongsTo(User::class , 'rate_user_id' , 'id');
    }

    
    public function advert(){
        return $this->belongsTo(Advertisement::class , 'ad_id');
    }

}
