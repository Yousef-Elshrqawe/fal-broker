<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'super_admin' => [
            'main'               => 'r',
            'users'              => 'c,r,u,d',
            'cats'               => 'c,r,u,d',
            'subcats'            => 'c,r,u,d',
            'advertisement'      => 'c,r,u,d',
            'country'            => 'c,r,u,d',
            'subscription'       => 'c,r,u,d',
            'attribute'          => 'c,r,u,d',
            'subattribute'       => 'c,r,u,d',
            'notification'       => 'c,r,u,d',
            'customers'          => 'r,d',
            'setting'            => 'r',
            'sliders'            => 'c,r,u,d',
            'terms'              => 'c,r,u,d',
            'rate'               => 'r,d',
            'about'              => 'r,u',

        ],
        'admin' => [],
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
