<?php $page="admin.advertisement_fees_expiration";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
                        <div class="col">
                            <form action="{{route('admin.fees-expiration.update')}}" method="POST" autocomplete="off">
                                @csrf
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.ad_fees')</label>
                                            <input type="number" step='0.01' name="fees" value={{$fees}} class="form-control" placeholder="@lang('user.ad_fees')" required>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.ad_expiration')</label>
                                            <input type="number" class="form-control" min="0"  name="days" value="{{$days}}" placeholder="@lang('user.ad_expiration')" required>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.ad_count_images')</label>
                                            <input type="number" class="form-control" min="0"  name="count_image" value="{{$count_image}}" placeholder="@lang('user.ad_count_images')" required>
                                        </div>
                                    </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.edit')</button>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->

@endsection