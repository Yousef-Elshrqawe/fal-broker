<?php $page="admin.merchant";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
									<h4 class="card-title mb-0">@lang('user.merchants')</h4>
									<p class="card-text">
                                        <form action="{{route('admin.merchant.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                            </div>
                                        </form>
                                    </p>
								</div>

                                <div style="display:flex; justify-content:center; align-items:center; text-align:center;">
                                    <form action="{{route('admin.merchant.index.excel')}}" method="get">
                                        <div class="row">
                                            <div class="col-md-2" style="display:none">
                                                <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                       placeholder="@lang('user.search')">
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-success">@lang('user.excel')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.name')</th>
                                                    <th>@lang('user.phone')</th>
                                                    <th>@lang('user.email')</th>
													<th>@lang('user.image')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($merchant as $index=>$item)
                                                    <tr>
                                                        <td>{{$index+1}}</td>
                                                        <td>
                                                            {{$item->name}}
                                                        </td>
                                                        <td>
                                                            {{$item->phone}}  ( {{ $item->country_code }})
                                                        </td>
                                                        <td>{{$item->email}}</td>
                                                        <td>
                                                            <img src="{{ $item->image }}" style="width:50px" class="img-thumbnail" alt="">
                                                        </td>
                                                        <td>
                                                            @if($item->dependence == '1')
                                                                <h3> <a href="{{route('admin.merchant.dependence', ['id'=> $item->id])}}"
                                                                        class="btn btn-outline-danger box-shadow-6 mr-1 ">@lang('user.Not_dependence')</a></h3>
                                                            @else
                                                                <h3> <a href="{{route('admin.merchant.dependence', ['id'=> $item->id])}}"
                                                                        class="btn btn-outline-primary box-shadow-6 mr-2 "> @lang('user.dependence') </a> </h3>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('customers_read'))
                                                                <a href="{{route('admin.merchant.show',$item->id)}}" class="btn btn-info btn-sm">@lang('user.show')</a>
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.show')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('customers_delete'))
                                                                <form action="{{route('admin.merchant.destroy',$item->id)}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
