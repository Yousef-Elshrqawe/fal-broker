@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Main Wrapper -->
<div class="main-wrapper">
			<div class="account-content">

				<div class="container">

					<!-- Account Logo -->
					<div class="account-logo">
						<a href="{{ route('pagee') }}"><img src="{{asset('assets/img/logo.png')}}" alt="Dreamguy's Technologies"></a>
					</div>
					<!-- /Account Logo -->

					<div class="account-box">
						<div class="account-wrapper">
							<h3 class="account-title">@lang('user.login')</h3>
							<p class="account-subtitle">@lang('user.access')</p>

							<!-- Account Form -->
                            @include('partials._errors')
                            @include('partials._session')
							<form action="{{route('admin.doLogin')}}" method="POST" autocomplete="off">
                                @csrf
								<div class="form-group">
									<label>@lang('user.username')</label>
									<input class="form-control" type="text" name="username" value="{{ old('username') }}">
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col">
											<label>@lang('user.password')</label>
										</div>
										<div class="col-auto">
											<a class="text-muted" href="forgot-password">
												@lang('user.forg_pass')
											</a>
										</div>
									</div>
									<input class="form-control" name="password" type="password">
								</div>
								<div class="form-group text-center">
									<button class="btn btn-primary account-btn" type="submit">@lang('user.login')</button>
								</div>
								{{-- <div class="account-footer">
									<p>Don't have an account yet? <a href="register">Register</a></p>
								</div> --}}
							</form>
							<!-- /Account Form -->

						</div>
					</div>
				</div>
			</div>
        </div>
		<!-- /Main Wrapper -->
@endsection
