<?php $page="admin.categories";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.cats')
                                        <small>{{ $total }}</small>
                                    </h3>
									<p class="card-text">
                                        <form action="{{route('admin.categories.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                    @if(Auth::guard('admin')->user()->hasPermission('cats_create'))
                                                        <a href="{{route('admin.categories.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @else
                                                        <a href="#" class="btn btn-primary" disabled><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </p>
								</div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.cat_name')</th>
                                                    <th>@lang('user.sorting')</th>
                                                    <th>@lang('user.type_name')</th>
                                                    <th>@lang('user.details_name')</th>
													<th>@lang('user.image')</th>
                                                    <th>@lang('user.icon')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($cats as $index => $item)
                                                    <tr>
                                                        <td>{{$index + 1}}</td>
                                                        <td>
                                                            {{$item->name}}
                                                        </td>
                                                        <td>
                                                            {{$item->sorting}}
                                                        </td>
                                                        <td>
                                                            {{$item->type_name}}
                                                        </td>
                                                        <td>
                                                            {{$item->details_name}}
                                                        </td>
                                                        <td>
                                                            <img src="{{ $item->image_path }}" style="width:50px" class="img-thumbnail" alt="">
                                                        </td>
                                                        <td>
                                                            <img src="{{ $item->icon_path }}" style="width:50px" class="img-thumbnail" alt="">
                                                        </td>
                                                        <td>
                                                            @if(Auth::guard('admin')->user()->hasPermission('subcats_read'))
                                                                <a href="{{route('admin.subcategories.index',['cat'=>$item->id])}}" class="btn btn-primary btn-sm">@lang('user.subcats')</a>
                                                            @else
                                                                <a href="#" class="btn btn-primary btn-sm" disabled>@lang('user.subcats')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('cats_update'))
                                                                <a href="{{route('admin.categories.edit',$item->id)}}" class="btn btn-info btn-sm">@lang('user.edit')</a>
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('cats_delete'))
                                                                <form action="{{route('admin.categories.destroy',$item->id)}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
