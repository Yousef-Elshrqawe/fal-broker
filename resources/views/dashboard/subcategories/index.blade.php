<?php $page="admin.categories";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.subcats')
                                        <small>{{ $total }}</small>
                                    </h3>
									<p class="card-text">
                                        <form action="{{route('admin.subcategories.index' , $cat_id)}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                    @if(Auth::guard('admin')->user()->hasPermission('subcats_create'))
                                                        <a href="{{route('admin.subcategories.create' , $cat_id)}}" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @else
                                                        <a href="#" class="btn btn-primary" disabled><i class="fa fa-plus"></i>@lang('user.add')</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </p>
								</div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.subcat_name')</th>
													<th>@lang('user.sorting')</th>
													<th>@lang('user.image')</th>
													<th>@lang('user.operations')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($subcats as $index => $item)
                                                    <tr>
                                                        <td>{{$index + 1}}</td>
                                                        <td>
                                                            {{$item->name}}
                                                        </td>
                                                        <td>
                                                            {{$item->sorting}}
                                                        </td>
                                                        <td>
                                                            <img src="{{ $item->image_path }}" style="width:50px" class="img-thumbnail" alt="">
                                                        </td>
                                                        <td>
                                                            @if(Auth::guard('admin')->user()->hasPermission('attribute_read'))
                                                                <a href="{{route('admin.subcategories.show',['cat'=>$cat_id , 'subcategory'=>$item->id])}}" class="btn btn-primary btn-sm">@lang('user.attributes')</a>
                                                            @else
                                                                <a href="#" class="btn btn-primary btn-sm" disabled>@lang('user.attributes')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('subcats_update'))
                                                                <a href="{{route('admin.subcategories.edit',['cat'=>$cat_id , 'subcategory'=>$item->id])}}" class="btn btn-info btn-sm">@lang('user.edit')</a>
                                                            @else
                                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                            @endif

                                                            @if(Auth::guard('admin')->user()->hasPermission('subcats_delete'))
                                                                <form action="{{route('admin.subcategories.destroy',['cat'=>$cat_id , 'subcategory'=>$item->id])}}" method="POST"
                                                                    style="display:inline-block">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                </form> <!--end of form -->
                                                            @else
                                                                <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
