<?php $page="admin.categories";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.subcats')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.subcategories.update',['cat'=>$subcat->cat_id , 'subcategory'=>$subcat->id])}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <h4 class="card-title">@lang('user.addNewCat')</h4>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <input type="hidden" name="id" value="{{$subcat->id}}">
                                            <label>@lang('user.catAr')</label>
                                            <input type="text" name="name_ar" class="form-control" value="{{$subcat->name_ar}}" placeholder="@lang('user.catAr')" required>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.catEn')</label>
                                            <input type="text" class="form-control" name="name_en" value="{{$subcat->name_en}}" placeholder="@lang('user.catEn')" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.sorting')</label>
                                            <input type="number" name="sorting" class="form-control" value="{{ $subcat->sorting }}" placeholder="@lang('user.sorting')" required>
                                        </div>
                                    </div>
                                    {{-- <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.mainCat')</label>
                                            <select name ="cat_id" class="form-control" aria-label=".form-select-lg example">
                                                @foreach ($cats as $cat)
                                                    <option {{$cat->id == $subcat->cat_id?'selected':''}} value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.image')</label>
                                            <input type="file" name="image" class="form-control image" >
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <img src="{{$subcat->image_path }}"
                                                class="img-thumbnail image-preview" style="width: 100px;">
                                        </div>
                                    </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.add')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

