<?php $page="admin.notification";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.notifications.send') }}" method="post">

                    @csrf
                    <div class="form-group">
                        <label>@lang('user.title')</label>
                        <input type="text" name="title" class="form-control" value="{{ old('title') }}" required>
                    </div>

                    <div class="form-group">
                        <label>@lang('user.body')</label>
                        <input type="text" name="body" class="form-control" value="{{ old('body') }}" required>
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="save" value="1" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            @lang('user.saveToDatabase')
                        </label>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"> <i class="fa fa-send"> </i> @lang('user.send')
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection