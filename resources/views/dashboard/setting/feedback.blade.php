<?php $page="feedback";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.feedback')
                                        <small>{{ $total }}</small>
                                    </h3>
									<p class="card-text">
                                        
                                    </p>
								</div>


								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped mb-0 datatables">
											<thead>
												<tr>
                                                    <th>#</th>
                                                    <th>@lang('user.user_name')</th>
                                                    <th>@lang('user.content')</th>
													<th>@lang('user.date')</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach ($feedbacks as $index => $item)
                                                    <tr>
                                                        <td>{{$index + 1}}</td>
                                                        <td>
                                                            {{$item->user->name?? ''}}
                                                        </td>
                                                        <td>
                                                            {{$item->message}}
                                                        </td>
                                                        <td>
                                                            {{$item->created_at}}
                                                        </td>
                                                    </tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
