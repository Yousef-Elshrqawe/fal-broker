<?php $page="about_us";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.aboutAs')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.setting.about.update',$setting->id)}}" method="POST" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <h4 class="card-title">@lang('user.aboutAs')</h4>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.about_ar')</label>
                                            <textarea name="about_ar" id="" class="form-control ckeditor" cols="30" rows="10">{!! $setting->about_ar !!}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label>@lang('user.about_en')</label>
                                            <textarea name="about_en" id="" class="form-control ckeditor" cols="30" rows="10">{!! $setting->about_en !!}</textarea>
                                        </div>
                                    </div>


                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.submit')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
