<?php $page="contact_us";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.technical-support')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.setting.contact-us.update',$setting->id)}}" method="POST" autocomplete="off">
                                @csrf
                                @method('PUT')
                                <h4 class="card-title">@lang('user.technical-support')</h4>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.phone')</label>
                                            <input type="text" name="phone" class="form-control" value="{{ $setting->phone}}"   placeholder="@lang('user.phone')">
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.email')</label>
                                            <input type="text" name="email" class="form-control" value="{{ $setting->email}}" required>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.complaint_email')</label>
                                            <input type="text" name="complaint_email" class="form-control" value="{{ $setting->complaint_email}}" required>
                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.suggestions_email')</label>
                                            <input type="text" name="suggestions_email" class="form-control" value="{{ $setting->suggestions_email}}" required>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.website')</label>
                                            <input type="text" name="website" class="form-control" value="{{ $setting->website}}" required>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.twitter')</label>
                                            <input type="text" name="twitter" class="form-control" value="{{ $setting->twitter}}"   placeholder="@lang('user.twitter')">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.snap_chat')</label>
                                            <input type="text" name="snap_chat" class="form-control" value="{{ $setting->snap_chat}}"   placeholder="@lang('user.snap_chat')">
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.tik_tok')</label>
                                            <input type="text" name="tik_tok" class="form-control" value="{{ $setting->tik_tok}}"   placeholder="@lang('user.tik_tok')">
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label>@lang('user.instagram')</label>
                                            <input type="text" name="instagram" class="form-control" value="{{ $setting->instagram}}"  placeholder="@lang('user.instagram')">
                                        </div>
                                    </div>
                                    <div class="form-row">

                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.facebook')</label>
                                            <input type="text" name="facebook" class="form-control" value="{{ $setting->facebook}}" placeholder="@lang('user.facebook')">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.count_ads_latest')</label>
                                            <input type="number" name="count_ads_latest" class="form-control" value="{{ $setting->count_ads_latest}}"  placeholder="@lang('user.count_ads_latest')">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        {{--  <div class="form-group">  --}}
                                            <label>@lang('user.financing_req')</label>
                                                <select name="financing_request"
                                                        class="form-control my-select-2 @error('financing_request') is-invalid @enderror">
                                                    {{--  <option value="" selected disabled>@lang('admin.select_role')</option>  --}}
                                                        <option {{ $setting->financing_request == 0 ? 'selected' : ''}}
                                                            value="0">@lang('user.turning_off')</option>
                                                        <option {{ $setting->financing_request == 1 ? 'selected' : ''}}
                                                                value="1">@lang('user.receive')</option>
                                                </select>

                                        {{--  </div>  --}}
                                    </div> <br>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.submit')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
