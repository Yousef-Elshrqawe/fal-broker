<?php $page="admin.users";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.user')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.users.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                <h4 class="card-title">@lang('user.add_new_user')</h4>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.first_name')</label>
                                            <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}" required>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.last_name')</label>
                                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.username')</label>
                                            <input type="text" name="username" class="form-control" value="{{ old('username') }}" required>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.email')</label>
                                            <input type="text" name="email" class="form-control" value="{{ old('email') }}" >
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.password')</label>
                                            <input type="password" name="password" class="form-control" required>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.rep_password')</label>
                                            <input type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.image')</label>
                                            <input type="file" name="image" class="form-control image" >
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <img src="{{ asset('uploads/users_images/default.png') }}"
                                                class="img-thumbnail image-preview" style="width: 100px;">
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('user.permission')</label>

                                            @php
                                                $models = [];
                                                $x = [];
                                                foreach( config('laratrust_seeder.roles_structure.super_admin') as $key=> $value){
                                                    array_push($models,$key);
                                                    $x[$key] = explode(',',$value);
                                                }
                                                //dd($x);
                                                $data   = config('laratrust_seeder.permissions_map');
                                                $maps   = ['create' , 'read' , 'update' , 'delete'];
                                            @endphp

                                            <nav class="nav">
                                                <ul class="nav nav-tabs">
                                                    @foreach($models as $index=>$model)
                                                        <li class="nav-item">
                                                            <a class="nav-link {{ $index == 0 ? 'active' : '' }}" href="#{{$model}}"
                                                                data-toggle="tab">@lang('user.'.$model)
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </nav>
                                            <!-- nav-tabs-custom -->
                                            <div class="tab-content">
                                                @foreach($models as $index=>$model)
                                                {{-- @php dd($value) @endphp --}}
                                                    <div class="tab-pane {{ $index == 0 ? 'active' : '' }}" id="{{$model}}">
                                                        @foreach($x as $model_name => $maps)
                                                            @foreach ($maps as $map)
                                                                {{-- @php dd($map) @endphp     --}}
                                                                @if ($model == $model_name)
                                                                <label><input type="checkbox" name="permissions[]"
                                                                    value="{{$model_name.'_'.$data[$map]}}"> @lang("user.$data[$map]")</label>

                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </div><!-- /.tab-pane -->
                                                @endforeach
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                    </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
