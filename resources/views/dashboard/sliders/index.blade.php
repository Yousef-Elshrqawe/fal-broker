<?php $page="slider";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
                            <div class="crms-title row bg-white">
                                    <h3 class="page-title m-0">
                                      @if(Auth::guard('admin')->user()->hasPermission('sliders_create'))
                                        {{-- <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" id="add-task" data-toggle="modal" data-target="#add_task">@lang('user.add')</button> --}}
                                        <a href="{{route('admin.slider.create')}}" class="btn btn-info">@lang('user.add')</a>
                                      {{-- @else
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" disabled>@lang('user.add')</button> --}}
                                      @endif
                            </div>
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.slider')
                                        <small>{{ $total }}</small>
                                    </h3>
									{{-- <p class="card-text">
                                        <form action="{{route('admin.slider.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                </div>
                                            </div>
                                        </form>
                                    </p> --}}
								</div>

								<div class="card-body">
                                    @include('partials._errors')
                                    @include('partials._session')
									<div class="table-responsive">
                                        @if($slider->count() > 0)
                                            <table class="datatable table table-stripped mb-0 datatables">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%" scope="col">#</th>
                                                        <th>@lang('user.cat_name')</th>
                                                        <th>@lang('user.subcat_name')</th>
                                                        <th>@lang('user.image')</th>
                                                        <th>@lang('user.slider_link')</th>
                                                        <th>@lang('user.desc')</th>
                                                        <th>@lang('user.operations')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($slider as $index=>$item)
                                                        <tr>
                                                            <th style="width: 5%" scope="row">{{ $index +1 }}</th>
                                                            <td>
                                                                @if (App::isLocale('en') && $item->cat_id != NULL)
                                                                    {{App\Models\Category::where('id',$item->cat_id)->select('name_en')->first()->name_en}}
                                                                @elseif(App::isLocale('ar') && $item->cat_id != NULL)
                                                                    {{App\Models\Category::where('id',$item->cat_id)->select('name_ar')->first()->name_ar}}
                                                                @else
                                                                    {{Null}}
                                                                @endif
                                                                    
                                                            </td>
                                                            <td>
                                                                @if (App::isLocale('en') && $item->subcat_id != NULL)
                                                                    {{App\Models\Subcategory::where('id',$item->subcat_id)->select('name_en')->first()->name_en}}
                                                                @elseif(App::isLocale('ar') && $item->subcat_id != NULL)
                                                                    {{App\Models\Subcategory::where('id',$item->subcat_id)->select('name_ar')->first()->name_ar}}
                                                                @else
                                                                    {{Null}}
                                                                @endif
                                                                    
                                                            </td>
                                                            <td>
                                                                <img src="{{ $item->image_path }}" style="width:50px" class="img-thumbnail" alt="">
                                                            </td>

                                                            <td><a href="{{$item->link}}"  target="_blank">{{$item->link}}</a></td>
                                                            
                                                            <td>{{$item->desc}}</td>
                                                            
                                                            <td>
                                                                @if(Auth::guard('admin')->user()->hasPermission('sliders_update'))
                                                                    <a href="{{route('admin.slider.edit',$item->id)}}" class="btn btn-info btn-sm">@lang('user.edit')</a>
                                                                @else
                                                                    <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                                @endif

                                                                @if(Auth::guard('admin')->user()->hasPermission('sliders_delete'))
                                                                    <form action="{{route('admin.slider.destroy',$item->id)}}" method="POST"
                                                                        style="display:inline-block">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                    </form> <!--end of form -->
                                                                @else
                                                                    <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>@lang('user.no_data_found')</h2>
                                        @endif
									</div>
								</div>
							</div>
						</div>
					</div>
                    {{-- @include('dashboard.sliders.create') --}}
				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
@section('scripts')
<script>
    //Edit Catgeoy
    $('#edit_task').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name_ar = button.data('country_ar')
        var name_en = button.data('country_en')
        var modal = $(this)
        modal.find('.modal-body #country_id').val(id);
        modal.find('.modal-body #country-AR').val(name_ar);
        modal.find('.modal-body #country-EN').val(name_en);
    })
</script>
@endsection
