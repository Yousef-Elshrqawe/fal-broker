<?php $page="slider"?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.slider')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.slider.update' , $slider->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                {{-- <h4 class="card-title">@lang('user.add_new_slider')</h4> --}}

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.slider_image_en')</label>
                                            <input type="file" name="image_en" class="form-control image-en">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <img src="{{$slider->image_en_path}}"
                                                class="img-thumbnail image-preview-en" style="width: 100px;">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3p">
                                            <label>@lang('user.slider_image_ar')</label>
                                            <input type="file" name="image_ar" class="form-control image-ar">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <img src="{{$slider->image_ar_path}}"
                                                class="img-thumbnail image-preview-ar" style="width: 100px;">
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.slider_link')</label>
                                            <input type="text" value="{{$slider->link}}" name="link" class="form-control" value="{{ old('link') }}">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.descEn')</label>
                                            <input type="text" value="{{$slider->desc_en}}" name="desc_en" class="form-control" value="{{ old('desc_en') }}">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.descAr')</label>
                                            <input type="text" value="{{$slider->desc_ar}}" name="desc_ar" class="form-control" value="{{ old('descAr') }}">
                                        </div>
                                    </div>

                                    {{-- <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.cats')</label>
                                            <select name="sub_cat" id="" class="form-control catSelect">
                                                <option id="catValue" value="0" selected>@lang('user.choose_cat')</option>
                                                @foreach ($main_cats as $cat)
                                                    @if (App::isLocale('en'))
                                                        <option value="{{$cat->id}}">{{$cat->name_en}}</option>
                                                    @else
                                                        <option value="{{$cat->id}}">{{$cat->name_ar}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label>@lang('user.cats')</label>
                                            <select name="cat_id" id="category" class="form-control input-lg dynamic" data-dependent="subcategory">
                                                <option value="">@lang('user.select_category')</option>
                                                @foreach($cats as $cat)
                                                @if (App::isLocale('en'))
                                                        <option value="{{$cat->id}}">{{$cat->name_en}}</option>
                                                    @else
                                                        <option value="{{$cat->id}}">{{$cat->name_ar}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--<div class="form-row">-->
                                    <!--    <div class="col-md-6 mb-3">-->
                                    <!--        <label>@lang('user.select_subcat')</label>-->
                                    <!--        <select name="subcat_id" id="subcategory" class="form-control input-lg dynamic" data-dependent="city">-->
                                    <!--            <option value="">@lang('user.select_subcat')</option>-->
                                    <!--        </select>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




<!-- Modal -->
 {{-- <div class="modal fade" id="add_task" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close md-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title text-center">@lang('user.cat_name')</h4>
                <button type="button" class="close xs-close" data-dismiss="modal">×</button>
              </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('admin.slider.store')}}" method="POST" autocomplete="off">
                            @csrf
                            
                            <div class="form-group row">
                                <div class="col-sm-6">
                                     <label class="col-form-label">@lang('user.cat_name') <span class="text-danger">*</span></label>
                                    <select name="cat_id" id="" class="form-control">
                                        <option value="" selected disabled>@lang('user.cat_name') </option>
                                        @foreach ($cats as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="text-center py-3">
                                <button type="submit" class="border-0 btn btn-primary btn-gradient-primary btn-rounded">@lang('user.add')</button>&nbsp;&nbsp;
                                
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div> --}}
<!-- modal -->
