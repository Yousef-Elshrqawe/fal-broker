<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.subattr')
                {{-- <small>{{ $subattributes->total() }}</small> --}}
            </h3>
        </div>
        <div class="box-body">

            @if($subattributes->count() > 0)
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                            <th>@lang('user.subattr')</th>
                            <th>@lang('user.value')</th>
                        <th>@lang('user.operations')</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($subattributes as $index => $subattr)
                        <tr>
                            <td>{{ $index +1 }}</td>
                            @if (App::isLocale('en'))
                                <td>{{ $subattr->name_en }}</td>
                            @else
                                <td>{{ $subattr->name_ar }}</td>
                            @endif

                            <td>{{$subattr->value}}</td>

                            <td>
                                <form method="post"

                                    action="{{route('admin.subattributes.delete' , ['cat'=>$cat, 'subcat'=>$attribute->subcat_id, 'id'=>$subattr->id])}}"
                                    style="display: inline-block">
                                    @csrf()
                                    
                                        <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i>@lang('user.delete')</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {{ $subattributes->appends(request()->query())->links() }}

            @else
                <h2>@lang('site.no_data_found')</h2>
            @endif

        </div>
    </div>

</section><!-- end of content -->
