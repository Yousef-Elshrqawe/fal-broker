<?php $page="admin.subscriptions";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
                            <div class="crms-title row bg-white">
                                    <h3 class="page-title m-0">
                                      @if(Auth::guard('admin')->user()->hasPermission('subscription_create'))
                                        {{-- <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" id="add-task" data-toggle="modal" data-target="#add_task">@lang('user.add')</button> --}}
                                        <a href="{{route('admin.subscriptions.create')}}" class="btn btn-info">@lang('user.add')</a>
                                      {{-- @else
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" disabled>@lang('user.add')</button> --}}
                                      @endif
                            </div>
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.subscription')
                                        <small>{{ $subscriptions->count() }}</small>
                                    </h3>
								</div>

								<div class="card-body">
                                    @include('partials._errors')
                                    @include('partials._session')
									<div class="table-responsive">
                                        @if($subscriptions->count() > 0)
                                            <table class="datatable table table-stripped mb-0 datatables">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%" scope="col">#</th>
                                                        <th>@lang('user.name')</th>
                                                        <th>@lang('user.ads_number')</th>
                                                        <th>@lang('user.price')</th>
                                                        <th>@lang('user.expiration')</th>
                                                        <th>@lang('user.user_type')</th>
                                                        <th>@lang('user.operations')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($subscriptions as $index=>$item)
                                                        <tr>
                                                            <th style="width: 5%" scope="row">{{ $index +1 }}</th>
                                                            <td>{{$item->name}}</td>
                                                            <td>{{$item->ads_number}}</td>
                                                            <td>{{$item->price}}</td>
                                                            <td>{{$item->period}}</td>

                                                            <td>{{$item->user_type == '0' ? 'فرد'  :  '' }}
                                                                {{$item->user_type == '1' ? 'تاجر'  :  '' }}
                                                                {{$item->user_type == '2' ? 'الكل'  :  '' }}</td>
                                                            {{--  <td>{{$item->user_type == 0 ? __('user.operations') : @if ($item->user_type == 1) __('user.operations') @else __('user.operations')}}@endif </td>  --}}

                                                            <td>
                                                                @if(Auth::guard('admin')->user()->hasPermission('subscription_update'))
                                                                    <a href="{{route('admin.subscriptions.edit',$item->id)}}" class="btn btn-info btn-sm">@lang('user.edit')</a>
                                                                @else
                                                                    <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                                @endif

                                                                @if(Auth::guard('admin')->user()->hasPermission('subscription_delete'))
                                                                    <form action="{{route('admin.subscriptions.destroy',$item->id)}}" method="POST"
                                                                        style="display:inline-block">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                    </form> <!--end of form -->
                                                                @else
                                                                    <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>@lang('user.no_data_found')</h2>
                                        @endif
									</div>
								</div>
							</div>
						</div>
					</div>
                    {{-- @include('dashboard.sliders.create') --}}
				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
@section('scripts')
<script>
    //Edit Catgeoy
    $('#edit_task').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name_ar = button.data('country_ar')
        var name_en = button.data('country_en')
        var modal = $(this)
        modal.find('.modal-body #country_id').val(id);
        modal.find('.modal-body #country-AR').val(name_ar);
        modal.find('.modal-body #country-EN').val(name_en);
    })
</script>
@endsection
