<section class="content">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.add')</h3>
        </div>
        <div class="box-body">

            @include('partials._errors')
            <form action="{{ route('admin.districts.store' , ['country'=>$country,'governorate'=>$govern]) }}" method="post" autocomplete="off">
                @csrf
                <div class="form-group">
                    <label>@lang('user.district_ar')</label>
                    <input type="text" name="name_ar" class="form-control" placeholder="@lang('user.district_ar')" value="{{ old('name_ar') }}" >
                </div>
                <div class="form-group">
                    <label>@lang('user.district_en')</label>
                    <input type="text" name="name_en" placeholder="@lang('user.district_en')" class="form-control" value="{{ old('name_en') }}" >
                </div>
                <input type="hidden" name="govern_id" value="{{$govern->id}}">
                <div class="form-group">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i>@lang('user.add')</button>
                </div>
            </form>
        </div>
    </div>

</section><!-- end of content -->
