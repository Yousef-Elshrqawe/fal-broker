<?php $page="admin.country";?>
@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12">
                            <div class="crms-title row bg-white">
                                    <h3 class="page-title m-0">
                                      @if(Auth::guard('admin')->user()->hasPermission('country_create'))
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" id="add-task" data-toggle="modal" data-target="#add_task">@lang('user.add')</button>
                                      @else
                                        <button class="add btn btn-gradient-primary font-weight-bold text-white todo-list-add-btn btn-rounded" disabled>@lang('user.state')</button>
                                      @endif
                            </div>
							<div class="card mb-0">
								<div class="card-header">
                                    <h3 class="box-title" style="margin-bottom: 10px;">@lang('user.govern')
                                        <small>{{ $governs->count() }}</small>
                                    </h3>
									{{-- <p class="card-text">
                                        <form action="{{route('admin.countries.index')}}" method="get">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                           placeholder="@lang('user.search')">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                </div>
                                            </div>
                                        </form>
                                    </p> --}}
								</div>

								<div class="card-body">
                                    @include('partials._errors')
                                    @include('partials._session')
									<div class="table-responsive">
                                        @if($governs->count() > 0)
                                            <table class="datatable table table-stripped mb-0 datatables">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%" scope="col">#</th>
                                                        <th style="width: 30%">@lang('user.govern')</th>
                                                        <th>@lang('user.operations')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($governs as $index=>$item)
                                                        <tr>
                                                            <th style="width: 5%" scope="row">{{ $index +1 }}</th>
                                                            <td style="width: 30%">
                                                                {{$item->name}}
                                                            </td>
                                                            <td>
                                                                @if(Auth::guard('admin')->user()->hasPermission('country_read'))
                                                                    <a href="{{route('admin.districts.index',['country'=>$country_id,'governorate'=>$item->id])}}" class="btn btn-primary btn-sm">@lang('user.districts')</a>
                                                                @else
                                                                    <a href="#" class="btn btn-primary btn-sm" disabled>@lang('user.districts')</a>
                                                                @endif
                                                                @if(Auth::guard('admin')->user()->hasPermission('country_update'))
                                                                    <a class="modal-effect btn btn-info btn-sm" data-effect="effect-scale" id="edit-task" data-toggle="modal" data-target="#edit_task"
                                                                            data-id="{{ $item->id }}" data-govern_ar="{{ $item->name_ar }}"
                                                                            data-govern_en="{{ $item->name_en }}">
                                                                        @lang('user.edit')
                                                                    </a>

                                                                @else
                                                                    <a href="#" class="btn btn-info btn-sm" disabled>@lang('user.edit')</a>
                                                                @endif

                                                                @if(Auth::guard('admin')->user()->hasPermission('country_delete'))
                                                                    <form action="{{route('admin.governorates.destroy',['country'=>$country_id,'governorate'=>$item->id])}}" method="POST"
                                                                        style="display:inline-block">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit" class="btb btn-danger btn-sm delete">@lang('user.delete')</button>
                                                                    </form> <!--end of form -->
                                                                @else
                                                                    <button type="submit" class="btb btn-danger btn-sm" disabled>@lang('user.delete')</button>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <h2>@lang('user.no_data_found')</h2>
                                        @endif
									</div>
								</div>
							</div>
						</div>
					</div>
                    @include('dashboard.country.governorates.create')
                    @include('dashboard.country.governorates.edit')
				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
<!--theme settings modal-->
@endsection
@section('scripts')
<script>
    //Edit Catgeoy
    $('#edit_task').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name_ar = button.data('govern_ar')
        var name_en = button.data('govern_en')
        var modal = $(this)
        modal.find('.modal-body #govern_id').val(id);
        modal.find('.modal-body #govern-AR').val(name_ar);
        modal.find('.modal-body #govern-EN').val(name_en);
    })
</script>
@endsection
