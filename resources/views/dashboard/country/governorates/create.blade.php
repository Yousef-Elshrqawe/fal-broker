 <!-- Modal -->
 <div class="modal fade" id="add_task" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close md-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title text-center">@lang('user.govern')</h4>
                <button type="button" class="close xs-close" data-dismiss="modal">×</button>
              </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('admin.governorates.store',['country'=>$country_id])}}" method="POST" autocomplete="off">
                            @csrf
                            {{-- <h4>Task Details</h4> --}}
                            <input type="hidden" name="country_id" value="{{$country_id}}">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                     <label class="col-form-label">@lang('user.governAr') <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name_ar" id="govern-name-ar" placeholder="@lang('user.governAr')">
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">@lang('user.governEn') <span class="text-danger">*</span></label>
                                   <input class="form-control" type="text" name="name_en" id="govern-name-en" placeholder="@lang('user.governEn')">
                               </div>
                            </div>
                            <div class="text-center py-3">
                                <button type="submit" class="border-0 btn btn-primary btn-gradient-primary btn-rounded">@lang('user.add')</button>&nbsp;&nbsp;
                                {{-- <button type="button" class="btn btn-secondary btn-rounded">Cancel</button> --}}
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
<!-- modal -->
