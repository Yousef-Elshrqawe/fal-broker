@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
    <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="crms-title row bg-white">
                    <div class="col  p-0">
                        <h3 class="page-title m-0">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                          <i class="fa fa-cog" aria-hidden="true"></i>
                        </span> @lang('user.changPass') </h3>
                    </div>
                </div>


                <div class="row pt-4">
                    <div class="col-md-12">
                        <div class="card mb-0">
                            <div class="card-body">
                                @include('partials._errors')
                                @include('partials._session')
                                <form action="{{route('admin.profiles.change_password_method')}}" method="post" autocomplete="off">
                                    @csrf
                                    @method('PUT')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input class="form-control" type="password" name="old_password" placeholder="@lang('user.oldPass')" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input class="form-control" type="password" name="password" placeholder="@lang('user.newPass')" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input class="form-control" type="password" name="password_confirmation" placeholder="@lang('user.password_confirmation')" required>
                                        </div>
                                    </div>
                                </div>


                                <div class="submit-section">
                                    <button class="btn btn-primary submit-btn">Save</button>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
    <!--theme settings modal-->

@endsection
