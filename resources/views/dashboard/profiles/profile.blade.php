@extends('dashboard.layout.mainlayout')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="crms-title row bg-white">
                    <div class="col  p-0">
                        <h3 class="page-title m-0">
                        <span class="page-title-icon bg-gradient-primary text-white mr-2">
                          <i class="feather-user"></i>
                        </span> @lang('user.profile') </h3>
                    </div>

                </div>

                <!-- Page Header -->
                <div class="page-header pt-3 mb-0">
                    <div class="card ">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-view">
                                    <div class="profile-img-wrap">
                                        <div class="profile-img">
                                            <img alt="" src="{{Auth::guard('admin')->user()->image_path}}">
                                        </div>
                                    </div>
                                    <div class="profile-basic">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="profile-info-left">
                                                    <h3 class="user-name m-t-0 mb-0">{{Auth::guard('admin')->user()->first_name}} {{Auth::guard('admin')->user()->last_name}}</h3>
                                                    <small class="text-muted">{{Auth::guard('admin')->user()->username}}</small>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <ul class="personal-info">
                                                    <li>
                                                        <div class="title">@lang('user.email'):</div>
                                                        <div class="text"><a href="">{{Auth::guard('admin')->user()->username}}</a></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">@lang('user.username'):</div>
                                                        <div class="text">{{Auth::guard('admin')->user()->username}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="title">@lang('user.first_name'):</div>
                                                        <div class="text">{{Auth::guard('admin')->user()->first_name}}</div>
                                                    </li>
                                                    <li>
                                                        <div class="title">@lang('user.last_name'):</div>
                                                        <div class="text">{{Auth::guard('admin')->user()->last_name}}</div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- /Page Header -->


            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
