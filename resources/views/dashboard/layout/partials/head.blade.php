<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> @yield('title') </title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

{{--<!-- Bootstrap 3.3.7 -->--}}
<link rel="stylesheet" href="{{ asset('dashboard/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard/css/skin-blue.min.css') }}">

{{-- @if (app()->getLocale() == 'ar') --}}
    <link rel="stylesheet" href="{{ asset('dashboard/css/font-awesome-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/css/AdminLTE-rtl.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('dashboard/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/css/rtl.css') }}">

    <style>
        body, h1, h2, h3, h4, h5, h6 {
            font-family: 'Cairo', sans-serif !important;
        }
    </style>
{{-- @else
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="{{ asset('dashboard/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/css/AdminLTE.min.css') }}">
@endif --}}

<style>
    .mr-2{
        margin-right: 5px;
    }
</style>
{{--<!-- jQuery 3 -->--}}
<script src="{{ asset('dashboard/js/jquery.min.js') }}"></script>

{{--noty--}}
<link rel="stylesheet" href="{{ asset('dashboard/plugins/noty/noty.css') }}">
<script src="{{ asset('dashboard/plugins/noty/noty.min.js') }}"></script>

{{--<!-- iCheck -->--}}
<link rel="stylesheet" href="{{ asset('dashboard/plugins/icheck/all.css') }}">

{{--html in  ie--}}
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

{{-- -------------------end of styles links for dashboard index page-------------------------------------------------------------- --}}
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
<meta name="description" content="CRMS - Bootstrap Admin Template">
<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
<meta name="author" content="Dreamguys - Bootstrap Admin Template">
<meta name="robots" content="noindex, nofollow">
    @if(!Route::is(['terms','tasks','tables-basic','settings','reports','register','projects','projects-kanban-view','projects-dashboard','profile','privacy-policy','mail-view','login','leads','leads-kanban-view','leads-dashboard','form-vertical','form-validation','form-mask','form-input-groups','form-horizontal','form-basic-inputs','faq','error-500','error-404','email','deals','deals-kanban-view','activities','blank-page','companies','components','contacts','data-tables','deals-dashboard']))
        <title>Dashboard - khiark admin laravel template</title>
    @endif
    @if(Route::is(['categories']))
        <title>categories</title>
    @endif
    @if(Route::is(['blank-page']))
        <title>Blank Page - khiark admin template</title>
    @endif
    @if(Route::is(['companies']))
        <title>Companies - khiark admin template</title>
    @endif
    @if(Route::is(['components']))
        <title>Components - khiark admin template</title>
    @endif
    @if(Route::is(['contacts']))
        <title>Contacts - khiark admin template</title>
    @endif
    @if(Route::is(['data-tables']))
        <title> Data Tables - khiark admin template</title>
    @endif
    @if(Route::is(['deals-kanban-view']))
        <title>Deals Kanban View - khiark admin template</title>
    @endif
    @if(Route::is(['deals']))
        <title>Deals - khiark admin template</title>
    @endif
    @if(Route::is(['email']))
        <title>Inbox - khiark admin template</title>
    @endif
    @if(Route::is(['error-404']))
        <title>Error 404 - khiark Admin Template</title>
    @endif
    @if(Route::is(['error-500']))
        <title>Error 500 - khiark Admin Template</title>
    @endif
    @if(Route::is(['faq']))
        <title>FAQ - khiark admin template</title>
    @endif
    @if(Route::is(['form-basic-inputs']))
        <title>Form Basic Inputs - khiark admin template</title>
    @endif
    @if(Route::is(['form-horizontal']))
        <title>Horizontal Form - khiark admin template</title>
    @endif
    @if(Route::is(['form-input-groups']))
        <title>Forms Input Groups - khiark admin template</title>
    @endif
    @if(Route::is(['form-mask']))
        <title> Form Mask - khiark admin template</title>
    @endif
    @if(Route::is(['form-validation']))
        <title> Form Validation - khiark admin template</title>
    @endif
    @if(Route::is(['form-vertical']))
        <title>Vertical Form - khiark admin template</title>
    @endif
    @if(Route::is(['leads-dashboard']))
        <title>Leads Dashboard - khiark admin template</title>
    @endif
    @if(Route::is(['leads-kanban-view']))
        <title>Leads Kanban View - khiark admin template</title>
    @endif
    @if(Route::is(['leads']))
        <title>Leads - khiark admin template</title>
    @endif
    @if(Route::is(['login']))
        <title>Login - CRMS admin template</title>
    @endif
    @if(Route::is(['mail-view']))
        <title>Mail view - khiark admin template</title>
    @endif
    @if(Route::is(['privacy-policy']))
        <title>Privacy Policy - khiark admin template</title>
    @endif
    @if(Route::is(['profile']))
        <title>Employee Profile - khiark admin template</title>
    @endif
    @if(Route::is(['projects-dashboard']))
        <title>Projects Dashboard - khiark admin template</title>
    @endif
    @if(Route::is(['projects-kanban-view']))
        <title>Projects Kanban View - khiark admin template</title>
    @endif
    @if(Route::is(['projects']))
        <title>Projects - khiark admin template</title>
    @endif
    @if(Route::is(['register']))
        <title>Register - khiark admin template</title>
    @endif
    @if(Route::is(['reports']))
        <title>Reports - khiark admin template</title>
    @endif
    @if(Route::is(['settings']))
        <title>Settings - khiark admin template</title>
    @endif
    @if(Route::is(['tables-basic']))
        <title> Basic Tables - khiark admin template</title>
    @endif
    @if(Route::is(['tasks']))
        <title>Tasks - khiark admin template</title>
    @endif
    @if(Route::is(['terms']))
        <title>Terms - khiark admin template</title>
    @endif

    @if (App::isLocale('en'))
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
        <!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/feather.css')}}">
        <!--font style-->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300;400;500;600&display=swap" rel="stylesheet">
        <!-- Lineawesome CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/line-awesome.min.css')}}">
        <!-- Select2 CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}">
        <!-- Datetimepicker CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">
        <!-- Tagsinput CSS -->
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
        <!-- Datatable CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}">
        <!-- Chart CSS -->
        <link rel="stylesheet" href="{{asset('assets/plugins/morris/morris.css')}}">
        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/theme-settings.css')}}">
        <!-- Main CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    @else
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets_rtl/img/favicon.png')}}">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/bootstrap.min.css')}}">
        <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/font-awesome.min.css')}}">
        <!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/feather.css')}}">
        <!--font style-->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300;400;500;600&display=swap" rel="stylesheet">
        <!-- Lineawesome CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/line-awesome.min.css')}}">
        <!-- Select2 CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/select2.min.css')}}">
        <!-- Datetimepicker CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/bootstrap-datetimepicker.min.css')}}">
        <!-- Tagsinput CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
        <!-- Datatable CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/dataTables.bootstrap4.min.css')}}">
        <!-- Chart CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/plugins/morris/morris.css')}}">
        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/theme-settings.css')}}">
        <!-- Main CSS -->
        <link rel="stylesheet" href="{{asset('assets_rtl/css/style.css')}}">
    @endif
    {{--noty--}}
    <link rel="stylesheet" href="{{ asset('assets/noty/noty.css') }}">
    <script src="{{ asset('assets/noty/noty.min.js') }}"></script>
    {{--<!-- iCheck -->--}}
    <link rel="stylesheet" href="{{ asset('assets/icheck/all.css') }}">
    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    @yield('styles')
