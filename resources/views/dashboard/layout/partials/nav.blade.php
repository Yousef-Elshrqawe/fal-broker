
 @if (auth()->guard('admin')->id())
    <?php error_reporting(0);?>
    @if(!Route::is(['email','mail-view','components']))
    <!-- Sidebar -->

        <div class="sidebar" id="sidebar">
                    <div class="sidebar-inner slimscroll">
                        <form action="search" class="mobile-view">
                            <input class="form-control" type="text" placeholder="Search here">
                            <button class="btn" type="button"><i class="fa fa-search"></i></button>
                        </form>
                        <div id="sidebar-menu" class="sidebar-menu">

                            <ul>
                                <li class="nav-item nav-profile">
                                <a href="#" class="nav-link">
                                    <div class="nav-profile-image">
                                    <img src="{{Auth::guard('admin')->user()->image_path}}" alt="profile">

                                    </div>
                                    <div class="nav-profile-text d-flex flex-column">
                                    <span class="font-weight-bold mb-2">{{Auth::guard('admin')->user()->first_name}} {{Auth::guard('admin')->user()->last_name}}</span>
                                    {{-- <span class="text-white text-small">Project Manager</span> --}}
                                    </div>
                                    <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
                                </a>
                                </li>
                                {{-- <li class="menu-title">
                                    <span>Main</span>
                                </li> --}}

                                @if(Auth::guard('admin')->user()->hasPermission('main_read'))
                                <li class="<?php if($page=="pagee") { echo 'active'; } ?>">
                                    <a href="{{route('pagee')}}"><i class="feather-home"></i> <span>@lang('user.main')</span></a>
                                </li>
                                @endif


                                @if(Auth::guard('admin')->user()->hasPermission('users_read'))
                                    <li class="<?php if($page=="admin.users") { echo 'active'; } ?>">
                                        <a href="{{route('admin.users.index')}}"><i class="feather-user"></i> <span>@lang('user.user')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('customers_read'))
                                    <li class="<?php if($page=="admin.merchant") { echo 'active'; } ?>">
                                        <a href="{{route('admin.merchant.index')}}"><i class="feather-user"></i> <span>@lang('user.merchants')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('customers_read'))
                                    <li class="<?php if($page=="admin.customer") { echo 'active'; } ?>">
                                        <a href="{{route('admin.customer.index')}}"><i class="feather-user"></i> <span>@lang('user.customer')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('country_read'))
                                    <li class="<?php if($page=="admin.country") { echo 'active'; } ?>">
                                        <a href="{{route('admin.countries.index')}}"><i class="feather-clipboard"></i> <span>@lang('user.country')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('cats_read'))
                                    <li class="<?php if($page=="admin.categories") { echo 'active'; } ?>">
                                        <a href="{{route('admin.categories.index')}}"><i class="feather-git-branch"></i> <span>@lang('user.cats')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('advertisement_read'))
                                    <li class="<?php if($page=="admin.advertisements") { echo 'active'; } ?>">
                                        <a href="{{route('admin.advertisements.index')}}"><i class="fa fa-hourglass"></i> <span>@lang('user.advertisements')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('advertisement_read'))
                                    <li class="<?php if($page=="admin.advertisement_types") { echo 'active'; } ?>">
                                        <a href="{{route('admin.advertisement-types.index')}}"><i class="feather-list"></i> <span>@lang('user.advertisement_types')</span></a>
                                    </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('advertisement_read'))
                                    <li class="<?php if($page=="admin.advertisement_fees_expiration") { echo 'active'; } ?>">
                                        <a href="{{route('admin.fees-expiration.index')}}"><i class="feather-clock"></i> <span>@lang('user.advertisement_fees_expiration')</span></a>
                                    </li>
                                @endif


                                {{--  @if(Auth::guard('admin')->user()->hasPermission('special_ads_read'))  --}}
                                    <li class="<?php if($page=="admin.special_advertisements") { echo 'active'; } ?>">
                                        <a href="{{route('admin.special-advertisements.index')}}"><i class="feather-list"></i> <span>@lang('user.special_advertisements')</span></a>
                                    </li>
                                {{--  @endif  --}}

                                @if(Auth::guard('admin')->user()->hasPermission('subscription_read'))
                                    <li class="<?php if($page=="admin.subscriptions") { echo 'active'; } ?>">
                                        <a href="{{route('admin.subscriptions.index')}}"><i class="feather-clipboard"></i> <span>@lang('user.subscriptions')</span></a>
                                    </li>
                                @endif

                                {{--  @if(Auth::guard('admin')->user()->hasPermission('subscription_read'))  --}}
                                    {{--  <li class="<?php if($page=="admin.funds") { echo 'active'; } ?>">
                                        <a href="{{route('admin.funds.index')}}"><i class="feather-clipboard"></i> <span>@lang('user.funds')</span></a>
                                    </li>  --}}
                                {{--  @endif  --}}

                                @if(Auth::guard('admin')->user()->hasPermission('notification_read'))
                                    <li class="<?php if($page=="admin.notification") { echo 'active'; } ?>">
                                        <a href="{{route('admin.notifications.index')}}"><i class="fa fa-bell"></i> <span>@lang('user.notifications')</span></a>
                                    </li>
                                @endif

                                <li class="<?php if($page=="admin.coupons") { echo 'active'; } ?>">
                                    <a href="{{route('admin.coupons.index')}}"><i class="fa fa-tags"></i> <span>@lang('user.coupons')</span></a>
                                </li>

                                <li class="<?php if($page=="admin.report") { echo 'active'; } ?>">
                                    <a href="{{route('admin.report.index')}}"><i class="feather-clipboard"></i> <span>@lang('user.report')</span></a>
                                </li>

                                @if(Auth::guard('admin')->user()->hasPermission('setting_read'))
                                    <li class="submenu">
                                        <a href="#"><i class="feather-hard-drive"></i> <span> @lang('user.setting')</span> <span class="menu-arrow"></span></a>
                                        <ul class="sub-menus">
                                            <li><a class="<?php if($page=="slider") { echo 'active'; } ?>" href="{{route('admin.slider.index')}}">@lang('user.slider')</a></li>
                                            <li><a class="<?php if($page=="banner") { echo 'active'; } ?>" href="{{route('admin.setting.banner')}}">@lang('users.banner')</a></li>
                                            <li><a class="<?php if($page=="feedback") { echo 'active'; } ?>" href="{{route('admin.setting.feedback')}}">@lang('user.feedback')</a></li>
                                            <li><a class="<?php if($page=="formContactUs") { echo 'active'; } ?>" href="{{route('admin.setting.form_contact_us')}}">@lang('user.formContactUs')</a></li>
                                            <li><a class="<?php if($page=="contact_us") { echo 'active'; } ?>" href="{{route('admin.setting.contact-us')}}">@lang('user.contactUs')</a></li>
                                            <li><a class="<?php if($page=="terms") { echo 'active'; } ?>" href="{{route('admin.setting.terms')}}">@lang('user.terms')</a></li>
                                            <li><a class="<?php if($page=="about_us") { echo 'active'; } ?>" href="{{route('admin.setting.about')}}">@lang('user.aboutAs')</a></li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
        </div>
                <!-- /Sidebar -->
    @endif
    <!-- Sidebar -->
    @if(Route::is(['email','mail-view']))
    <div class="sidebar" id="sidebar">
                    <div class="sidebar-inner slimscroll">
                        <form action="search" class="mobile-view">
                            <input class="form-control" type="text" placeholder="Search here">
                            <button class="btn" type="button"><i class="fa fa-search"></i></button>
                        </form>
                        <div class="sidebar-menu">
                            <ul>
                                <li>
                                    <a href="index"><i class="fa fa-home" aria-hidden="true"></i> <span>Back to Home</span></a>
                                </li>
                                <li class="{{ Request::is('email') ? 'active' : '' }}">
                                    <a href="email"><i class="fa fa-envelope menu-icon" aria-hidden="true"></i> <span>Inbox <span class="mail-count">(21)</span></span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-star menu-icon" aria-hidden="true"></i> <span>Starred</span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-paper-plane menu-icon" aria-hidden="true"></i> <span>Sent Mail</span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-trash menu-icon" aria-hidden="true"></i> <span>Trash</span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-folder-open-o menu-icon" aria-hidden="true"></i> <span>Draft <span class="mail-count">(8)</span></span></a>
                                </li>


                                <li class="menu-title xs-hidden">Label <a href="#" class="label-icon"><i class="fa fa-plus"></i></a></li>
                                <li class="xs-hidden">
                                    <a href="#"><i class="fa fa-circle text-success mail-label"></i> Work</a>
                                </li>
                                <li class="xs-hidden">
                                    <a href="#"><i class="fa fa-circle text-danger mail-label"></i> Office</a>
                                </li>
                                <li class="xs-hidden">
                                    <a href="#"><i class="fa fa-circle text-warning mail-label"></i> Personal</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /Sidebar -->
                @endif
    @if(Route::is(['components']))
    <!-- Sidebar -->
    <div class="sidebar stickyside" id="sidebar">
                <div class="sidebar-inner slimscroll">
                	<form action="search" class="mobile-view">
						<input class="form-control" type="text" placeholder="Search here">
						<button class="btn" type="button"><i class="fa fa-search"></i></button>
					</form>
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li>
								<a href="index">Back To Home</a>
							</li>
							<li class="menu-title">
								Components
							</li>
							<li>
								<a href="#comp_alerts" class="active">Alerts</a>
							</li>
							<li>
								<a href="#comp_breadcrumbs">Breadcrumbs</a>
							</li>
							<li>
								<a href="#comp_buttons">Buttons</a>
							</li>
							<li>
								<a href="#comp_cards">Cards</a>
							</li>
							<li>
								<a href="#comp_dropdowns">Dropdowns</a>
							</li>
							<li>
								<a href="#comp_pagination">Pagination</a>
							</li>
							<li>
								<a href="#comp_progress">Progress</a>
							</li>
							<li>
								<a href="#comp_tabs">Tabs</a>
							</li>
							<li>
								<a href="#comp_typography">Typography</a>
							</li>
						</ul>
					</div>
                </div>
            </div>
			<!-- /Sidebar -->
		@endif
@endif
