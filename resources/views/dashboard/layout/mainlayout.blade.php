<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('dashboard.layout.partials.head')
</head>
    @if(Route::is(['login','register']))
        <body class="account-page">
    @endif
    @if(Route::is(['error-404','error-500']))
        <body class="error-page">
    @endif
    @if(!Route::is(['login','register','error-404','error-500']))
        @include('dashboard.layout.partials.header')
        @include('dashboard.layout.partials.nav')
    @endif
    @yield('content')
    @include('partials._session')
    @include('dashboard.layout.partials.footer-scripts')
  </body>
</html>
