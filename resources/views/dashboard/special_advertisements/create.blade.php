 <!-- Modal -->
 <div class="modal fade" id="add_task" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <button type="button" class="close md-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title text-center">@lang('user.special_advertisements')</h4>
                <button type="button" class="close xs-close" data-dismiss="modal">×</button>
              </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('admin.special-advertisements.store')}}" method="POST" autocomplete="off">
                            @csrf
                            {{-- <h4>Task Details</h4> --}}
                            <div class="form-group row">
                                <div class="col-sm-6">
                                     <label class="col-form-label">@lang('user.expiration') <span class="text-danger">*</span></label>
                                    <input class="form-control" type="number" step="1" min="0" name="expiration" id="expiration" placeholder="@lang('user.expiration')">
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">@lang('user.price') <span class="text-danger">*</span></label>
                                   <input class="form-control" type="number" step="0.01" min="0" name="price" id="price" placeholder="@lang('user.price')">
                               </div>

                            </div>


                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label>@lang('user.cats')<span class="text-danger">*</span></label>
                                    <select name="cat_id" id="category" class="form-control input-lg dynamic" data-dependent="subcategory">
                                        <option value="">@lang('user.select_category')</option>
                                        @foreach($cats as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="text-center py-3">
                                <button type="submit" class="border-0 btn btn-primary btn-gradient-primary btn-rounded">@lang('user.add')</button>&nbsp;&nbsp;
                                {{-- <button type="button" class="btn btn-secondary btn-rounded">Cancel</button> --}}
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
<!-- modal -->
