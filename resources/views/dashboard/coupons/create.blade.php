<?php $page="admin.coupons";?>
@extends('dashboard.layout.mainlayout')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">@lang('user.coupons')</h4>
                        </div>
                        @include('partials._errors')
                        @include('partials._session')
                        <div class="card-body">
                            <form action="{{route('admin.coupons.store')}}" method="POST" autocomplete="off">
                                @csrf
                                <h4 class="card-title">@lang('user.add')</h4>
                                
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label>@lang('user.coupon_code')</label>
                                        <input type="text" name="code" class="form-control" value="{{ old('code') }}" placeholder="@lang('user.coupon_code')" required>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label>@lang('user.discount_percent')</label>
                                        <input type="number" step="0.1" class="form-control" name="percent" value="{{ old('percent') }}" placeholder="@lang('user.discount_percent')" required>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label>@lang('user.count_of_use')</label>
                                        <input type="number" step="1" class="form-control" name="count_of_use" value="{{ old('count_of_use') }}" placeholder="@lang('user.count_of_use')" required>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label>@lang('user.times_for_user')</label>
                                        <input type="number" step="1" class="form-control" name="times_for_user" value="{{ old('times_for_user') }}" placeholder="@lang('user.times_for_user')" required>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <label>@lang('user.end_date')</label>
                                        <input type="date" step="0.1" class="form-control" name="end_date" value="{{ old('end_date') }}" placeholder="@lang('user.end_date')" required>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">@lang('user.add')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

