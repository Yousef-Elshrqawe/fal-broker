<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Admin::create([
            'first_name'   => 'super',
            'last_name'    => 'admin',
            'username'     => 'super-admin',
            'email'        => 'super_admin@yahoo.com',
            'password'     => bcrypt('123456'),
            'image'        => 'default.png',
        ]);
        $user->attachRole('super_admin');
    }
}
