<?php

namespace Database\Seeders;

use App\Models\ContactUs;
use Illuminate\Database\Seeder;

class ContactUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactUs::create([
            'phone'   => "0184546546",
            'website' => "https://www.google.com/",
            'email'   => "abdullah@gmail.com",
        ]);
    }
}
