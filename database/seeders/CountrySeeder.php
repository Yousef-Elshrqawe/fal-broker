<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'name_en'   => "Egypt",
            'name_ar'   => "مصر",
        ]);

        Country::create([
            'name_en'   => "USA",
            'name_ar'   => "الولايات المتحده الامريكيه",
        ]);

        Country::create([
            'name_en'   => "Saudi",
            'name_ar'   => "السعوديه"
        ]);

        Country::create([
            'name_en'   => "Kuwait",
            'name_ar'   => "الكويت",
        ]);


        Country::create([
            'name_en'   => "Yemeni",
            'name_ar'   => "اليمن",
        ]);


        Country::create([
            'name_en'   => "Tunisia",
            'name_ar'   => "تونس",
        ]);

        Country::create([
            'name_en'   => "Libya",
            'name_ar'   => "ليبيا",
        ]);
    }
}
