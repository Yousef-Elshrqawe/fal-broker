<?php
namespace Database\Seeders;

use App\Models\AdvertisementExpiration;
use Illuminate\Database\Seeder;

class AdvertisementExpirationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdvertisementExpiration::create([
            'days'      =>1
        ]);
    }
}
