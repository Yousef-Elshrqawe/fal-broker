<?php

namespace Database\Seeders;

use App\Models\Subattribute;
use Illuminate\Database\Seeder;

class SubattributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //milk subattributes
        Subattribute::create([
            'name_en'   => 'Gary',
            'name_ar'   => 'رصاصى',
            'attr_id'   => 3
        ]);
        Subattribute::create([
            'name_en'   => 'Black',
            'name_ar'    => 'أسود',
            'attr_id'   => 3
        ]);
    }
}
