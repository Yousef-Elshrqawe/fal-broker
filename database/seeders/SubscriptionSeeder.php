<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscription::create([
            'name_en'   =>'Bundel of 5 ADs',
            'name_ar'   =>'باقة 5 اعلانات',
            'price'     =>'100',
            'ads_number'=>5,
        ]);
        Subscription::create([
            'name_en'   =>'Bundel of 10 ADs',
            'name_ar'   =>'باقة 10 اعلانات',
            'price'     =>'200',
            'ads_number'=>10,
        ]);
    }
}
