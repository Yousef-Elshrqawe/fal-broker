<?php

namespace Database\Seeders;

use App\Models\AdvertisementFee;
use Illuminate\Database\Seeder;

class AdvertisementFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdvertisementFee::create([
            'fees'      => 50
        ]);
    }
}
