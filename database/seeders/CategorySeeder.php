<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name_en'   => "Real Estates",
            'name_ar'   => "عقارات",
            'image'     => 'default.png',
        ]);

        Category::create([
            'name_en'   => "Cars",
            'name_ar'   => "سيارات",
            'image'     => 'default.png',
        ]);
    }
}
