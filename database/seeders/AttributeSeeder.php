<?php

namespace Database\Seeders;

use App\Models\Attribute;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attribute::create([
            'name_en'   => 'Number of bed rooms',
            'name_ar'   => 'عدد غرف النوم',
            'subcat_id'    => 1,
            'type'      => 1
        ]);
        Attribute::create([
            'name_en'   => 'Floors number',
            'name_ar'   => 'عدد الطوابق',
            'subcat_id'    => 2,
            'type'      => 1
        ]);

        Attribute::create([
            'name_en'   => 'Color',
            'name_ar'   => 'اللون',
            'subcat_id'    => 3,
            'type'      => 0
        ]);
        Attribute::create([
            'name_en'   => 'Number of seats',
            'name_ar'   => 'عدد المقاعد',
            'subcat_id'    => 4,
            'type'      => 1
        ]);
    }
}
