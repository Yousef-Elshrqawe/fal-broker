<?php

namespace Database\Seeders;

use App\Models\Subcategory;
use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subcategory::create([
            'name_en'   => "Apartment",
            'name_ar'   => "شقة",
            'image'     => 'default.png',
            'cat_id'    => 1
        ]);

        Subcategory::create([
            'name_en'   => "Building",
            'name_ar'   => "عمارة",
            'image'     => 'default.png',
            'cat_id'    => 1
        ]);

        Subcategory::create([
            'name_en'   => "Sedan",
            'name_ar'   => "سيدان",
            'image'     => 'default.png',
            'cat_id'    => 2
        ]);

        Subcategory::create([
            'name_en'   => "Hatchback",
            'name_ar'   => "هاتش باك",
            'image'     => 'default.png',
            'cat_id'    => 2
        ]);
    }
}
