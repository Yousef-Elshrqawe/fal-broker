<?php

namespace Database\Seeders;

use App\Models\Terms;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Terms::create([
            'term_en' => Str::random(500),
            'term_ar' => "​الخصوصية وبيان سريّة
            لم نقم بتصميم هذا الموقع من أجل تجميع بياناتك الشخصية من جهاز الكمبيوتر الخاص بك أثناء تصفحك لهذا الموقع, وإنما سيتم فقط استخدام البيانات المقدمة من قبلك بمعرفتك ومحض إرادت",
        ]);
    }
}
