<?php

namespace Database\Seeders;

use App\Models\AdvertisementType;
use Illuminate\Database\Seeder;

class AdvertisementTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdvertisementType::create([
            'name_en'   =>'Selling',
            'name_ar'   =>'بيع',
        ]);

        AdvertisementType::create([
            'name_en'   =>'Renting',
            'name_ar'   =>'أيجار',
        ]);
    }
}
