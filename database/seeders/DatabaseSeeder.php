<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(LaratrustSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(AdvertisementTypesSeeder::class);
        $this->call(AdvertisementFeeSeeder::class);
        $this->call(AdvertisementExpirationSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(GovernorateSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(SubcategorySeeder::class);
        $this->call(AttributeSeeder::class);
        $this->call(SubattributeSeeder::class);
        $this->call(AboutUsSeeder::class);
        $this->call(ContactUsSeeder::class);
        $this->call(TermSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(SubscriptionSeeder::class);
        $this->call(UserSeeder::class);
    }
}
