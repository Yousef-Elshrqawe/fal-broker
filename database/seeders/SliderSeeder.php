<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Subcategory;
use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::create([
            "link"     =>"https://www.amazon.eg/",
            "image_ar" => "default.png",
            "image_en" => "default.png",
            "cat_id"   => Category::first()->id,
            "subcat_id"=> Subcategory::first()->id
        ]);

        Slider::create([
            "link"     =>"https://www.google.com/",
            "image_ar" => "default.png",
            "image_en" => "default.png",
            "cat_id"   => Category::get()->last()->id,
            "subcat_id"=> Subcategory::first()->id
        ]);
    }
}
