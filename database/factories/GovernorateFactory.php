<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Country;
use App\Models\Governorate;
use Faker\Generator as Faker;


$factory->define(Governorate::class, function (Faker $faker) {
    return [
        'name_ar'           => $faker->name,
        'name_en'           => $faker->name,
        'country_id'      => function(){
            return Country::get()->random();
         },
    ];
});
