<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\District;
use App\Models\Governorate;
use Faker\Generator as Faker;

$factory->define(District::class, function (Faker $faker) {
    return [
        'name_ar'           => $faker->name,
        'name_en'           => $faker->name,
        'govern_id'         => function(){
            return Governorate::get()->random();
        },
    ];
});
