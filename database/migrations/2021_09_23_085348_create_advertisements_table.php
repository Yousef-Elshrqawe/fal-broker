<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->unsignedBigInteger('subcat_id')->nullable();
            $table->foreign('subcat_id')->references('id')->on('subcategories')->onDelete('cascade');
            $table->text('desc')->nullable();
            $table->string('payment_way')->nullable();
            $table->float('price')->nullable();
            $table->unsignedBigInteger('type')->nullable();
            $table->foreign('type')->references('id')->on('advertisement_types')->onDelete('cascade');

            $table->unsignedBigInteger('special_ad_id')->nullable();
            $table->foreign('special_ad_id')->references('id')->on('special_advertisements')->onUpdate('cascade');
            $table->timestamp('special_ad_start')->nullable();

            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->unsignedTinyInteger('finished')->default(0)->nullable();
            $table->unsignedTinyInteger('canceled')->default(0)->nullable();
            
            $table->unsignedBigInteger('reason_id')->nullable();
            $table->foreign('reason_id')->references('id')->on('reasons')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->unsignedBigInteger('govern_id')->nullable();
            $table->foreign('govern_id')->references('id')->on('governorates')->onDelete('cascade');
            $table->unsignedBigInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->boolean('is_subscribed')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
