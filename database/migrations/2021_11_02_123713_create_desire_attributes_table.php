<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesireAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('desire_attributes')) {
            Schema::create('desire_attributes', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('desire_id')->nullable();
                $table->foreign('desire_id')->references('id')->on('desires')->onDelete('cascade');
                $table->unsignedBigInteger('attr_id')->nullable();
                $table->foreign('attr_id')->references('id')->on('attributes')->onDelete('cascade');
                $table->unsignedBigInteger('subattr_id')->nullable();
                $table->foreign('subattr_id')->references('id')->on('subattributes')->onDelete('cascade');
                $table->unsignedTinyInteger('attr_type')->nullable()->comment('0: multichoice, 1: text or number value');
                $table->string('value')->nullable()->comment('value of number or text attribute');
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desire_attributes');
    }
}
