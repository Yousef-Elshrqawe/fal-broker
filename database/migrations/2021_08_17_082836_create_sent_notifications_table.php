<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSentNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sent_notifications')) {
            Schema::create('sent_notifications', function (Blueprint $table) {
                $table->id();

                //notifications
                $table->bigInteger('not_id')->unsigned()->nullable();
                $table->foreign('not_id')->references('id')->on('notifications')->onDelete('cascade')->onUpdate('cascade');

                //users
                $table->bigInteger('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

                $table->string('title')->nullable();
                $table->text('body')->nullable();

                $table->unsignedBigInteger('ad_id')->nullable();
                $table->foreign('ad_id')->references('id')->on('advertisements')->onDelete('cascade')->onUpdate('cascade');

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sent_notifications');
    }
}
