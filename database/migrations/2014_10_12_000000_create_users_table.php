<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('type')->comment('0=>person, 1=>company')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable()->unique();
            $table->string('country_code')->nullable()->default('+966');
            $table->string('password');
            $table->boolean('active')->default(0);
            $table->string('image')->nullable()->default('default.png');
            $table->unsignedBigInteger('governorate_id')->nullable();
            $table->foreign('governorate_id')->references('id')->on('governorates')->onDelete('cascade')->onUpdate('cascade');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('address')->nullable();
            $table->float('view_rate')->comment('1 => view_rate 0=>hide rate')->default(1);
            $table->float('avg_rate')->default(0);
            $table->unsignedTinyInteger('count_rate')->default(0);
            $table->unsignedTinyInteger('count_followes')->default(0);
            $table->string('number_identity')->nullable();
            $table->string('image_identity')->nullable();
            $table->string('commercial_register')->nullable();
            $table->boolean('mobile_id')->comment('0 for android, 1 for ios')->nullable();
            $table->string('fcm_token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('forgetcode')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
