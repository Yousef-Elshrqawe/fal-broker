<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedTinyInteger('fund_type')->nullable()->comment('0=>realestute , 1=>personal, 2=>cars');
            $table->string('birth_date')->nullable();
            $table->string('nationlity')->nullable();
            $table->string('sector')->nullable();
            $table->string('monthly_income')->nullable();
            $table->string('salary_type')->nullable();
            $table->string('financing_periad')->nullable();
            $table->string('monthly_commit')->nullable();
            $table->string('car_price')->nullable();
            $table->string('downpayment')->nullable();
            $table->string('property_value')->nullable();
            $table->string('account_type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funds');
    }
}
