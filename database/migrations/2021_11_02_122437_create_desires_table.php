<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('desires')) {
            Schema::create('desires', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id')->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->unsignedBigInteger('subcat_id')->nullable();
                $table->foreign('subcat_id')->references('id')->on('subcategories')->onDelete('cascade');
                $table->string('payment_way')->nullable();
                $table->float('from_price')->nullable();
                $table->float('to_price')->nullable();
                $table->unsignedBigInteger('type')->nullable();
                $table->foreign('type')->references('id')->on('advertisement_types')->onDelete('cascade');
                $table->unsignedBigInteger('country_id')->nullable();
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
                $table->unsignedBigInteger('govern_id')->nullable();
                $table->foreign('govern_id')->references('id')->on('governorates')->onDelete('cascade');
                $table->unsignedBigInteger('district_id')->nullable();
                $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desires');
    }
}
