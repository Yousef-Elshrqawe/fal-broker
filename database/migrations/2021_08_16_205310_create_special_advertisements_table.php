<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_advertisements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('expiration')->nullable();
            $table->unsignedFloat('price')->nullable();
            $table->BigInteger('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('Categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_advertisements');
    }
}
